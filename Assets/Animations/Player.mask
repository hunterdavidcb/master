%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Player
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: body
    m_Weight: 1
  - m_Path: gem
    m_Weight: 1
  - m_Path: head_ring
    m_Weight: 1
  - m_Path: Lance
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 1
  - m_Path: metarig/Placement
    m_Weight: 1
  - m_Path: metarig/Placement/foot_target.L
    m_Weight: 1
  - m_Path: metarig/Placement/foot_target.R
    m_Weight: 1
  - m_Path: metarig/Placement/knee_pole.L
    m_Weight: 1
  - m_Path: metarig/Placement/knee_pole.R
    m_Weight: 1
  - m_Path: metarig/Placement/Spear.1
    m_Weight: 1
  - m_Path: metarig/Placement/Spear.1/Spear.002
    m_Weight: 1
  - m_Path: metarig/Placement/Spear.1/Spear.002/Spear.003
    m_Weight: 1
  - m_Path: metarig/Placement/Spear.1/Spear.002/Spear.003/Spear.004
    m_Weight: 1
  - m_Path: metarig/Placement/Spear.1/Spear.002/Spear.003/Spear.004/Spear.005
    m_Weight: 1
  - m_Path: metarig/Placement/spine
    m_Weight: 1
  - m_Path: metarig/Placement/spine/pelvis.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/pelvis.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/breast.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/breast.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/spine.004
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/spine.004/spine.005
    m_Weight: 1
  - m_Path: metarig/Placement/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.L/shin.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.L/shin.L/foot.L
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.R/shin.R
    m_Weight: 1
  - m_Path: metarig/Placement/spine/thigh.R/shin.R/foot.R
    m_Weight: 1
