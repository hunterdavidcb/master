﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

//[CustomEditor(typeof(StatDatabase))]
public class WeaponDatabaseEditor: EditorWindow
{
	Vector2 scrollPos;

	WeaponDatabase weapons;
	[MenuItem("Window/Weapons")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(WeaponDatabaseEditor));
	}

	private void OnEnable()
	{
		weapons = Resources.Load<WeaponDatabase>("Weapons");//target as StatDatabase;
	}

	void OnGUI()
	{
		//ItemDatabase items = Resources.Load<ItemDatabase>("Items");
		Debug.Log(weapons.keys.Count);
		if (weapons != null)
		{
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			GUILayout.BeginVertical();
			{
				foreach (var item in weapons.keys)
				{
					GUILayout.BeginHorizontal();
					{
						GUILayout.BeginVertical();
						{
							EditorGUILayout.LabelField("ID", item);

							//changing the name
							EditorGUI.BeginChangeCheck();
							string n = EditorGUILayout.TextField("Name", weapons[item].Name);
							if (EditorGUI.EndChangeCheck())
							{
								weapons[item].ChangeName(n);
								EditorUtility.SetDirty(weapons);
								AssetDatabase.Refresh();
							}

							//changing the description
							EditorGUI.BeginChangeCheck();
							string d = EditorGUILayout.TextField("Description", weapons[item].Description);

							if (EditorGUI.EndChangeCheck())
							{
								weapons[item].ChangeDescription(d);
								EditorUtility.SetDirty(weapons);
								AssetDatabase.Refresh();
							}

							//changing the base attack
							EditorGUI.BeginChangeCheck();
							float a = EditorGUILayout.Slider("Base Attack", weapons[item].BaseAttack, 0f, 10);
							if (EditorGUI.EndChangeCheck())
							{
								weapons[item].ChangeAttack(a);
								EditorUtility.SetDirty(weapons);
								AssetDatabase.Refresh();
							}

							//changing the sprite
							EditorGUI.BeginChangeCheck();
							Sprite s = EditorGUILayout.ObjectField(weapons[item].Icon, typeof(Sprite), false) as Sprite;
							if (EditorGUI.EndChangeCheck())
							{
								weapons[item].ChangeIcon(s);
								EditorUtility.SetDirty(weapons);
								AssetDatabase.Refresh();
							}
						}
						GUILayout.EndVertical();

						if (GUILayout.Button("Remove"))
						{
							Debug.Log("removing");
							weapons.Remove(item);
							EditorUtility.SetDirty(weapons);
							AssetDatabase.SaveAssets();
							AssetDatabase.Refresh();
							return;
						}
					}

					GUILayout.EndHorizontal();

					EditorGUILayout.Space(10f);

				}

				if (GUILayout.Button("Add"))
				{
					Debug.Log("called");
					Weapon id = new Weapon("", "");
					weapons.Add(id.ID, id);
					EditorUtility.SetDirty(weapons);
					//ItemData id = new ItemData("", "");
					//items.Add(id.ID, id);
					//EditorUtility.SetDirty(items);
					//Context();
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}
			}
			GUILayout.EndVertical();

			EditorGUILayout.EndScrollView();
		}

		
	}	
}
