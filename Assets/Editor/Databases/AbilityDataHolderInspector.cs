﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AbilityDataHolder))]
public class AbilityDataHolderInspector : Editor
{
	AbilityDatabase abilities;
	string[] abilityNames;
	List<AbilityData> abilityList = new List<AbilityData>();

	SerializedProperty ability;
	private void OnEnable()
	{
		abilities = Resources.Load<AbilityDatabase>("Abilities");

		foreach (var item in abilities.keys)
		{
			//Debug.Log(skills[item].Name);
			//Debug.Log(skills[item].ID);
			abilityList.Add(abilities[item]);
		}

		abilityNames = new string[abilityList.Count];
		for (int i = 0; i < abilityList.Count; i++)
		{
			abilityNames[i] = abilityList[i].Name;
		}

		ability = serializedObject.FindProperty("abilityData");
	}

	public override void OnInspectorGUI()
	{
		//serializedObject.Update();
		EditorGUI.BeginChangeCheck();

		int sel = EditorGUILayout.Popup(abilityList.IndexOf((target as AbilityDataHolder).abilityData), abilityNames);

		//EditorGUILayout.PropertyField(ability);
		//serializedObject.ApplyModifiedProperties();

		if (EditorGUI.EndChangeCheck())
		{
			(target as AbilityDataHolder).abilityData = abilityList[sel];
			EditorUtility.SetDirty(target);
		//	serializedObject.ApplyModifiedProperties();
		}
	}
}
