﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(StatDatabase))]
public class SkillDatabaseEditor: EditorWindow
{
	Vector2 scrollPos;

	[MenuItem("Window/Skills")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(SkillDatabaseEditor));
	}

	void OnGUI()
	{
		SkillDatabase skills = Resources.Load<SkillDatabase>("Skills");//target as StatDatabase;

		//Debug.Log(skills.keys.Count);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		GUILayout.BeginVertical();
		foreach (var item in skills.keys)
		{


			GUILayout.BeginHorizontal();

			GUILayout.BeginVertical();

			EditorGUILayout.LabelField("ID", item);

			EditorGUI.BeginChangeCheck();
			string n = EditorGUILayout.TextField("Name",skills[item].Name);

			if (EditorGUI.EndChangeCheck())
			{
				skills[item].ChangeName(n);
				EditorUtility.SetDirty(skills);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();
			string d = EditorGUILayout.TextField("Description", skills[item].Description);

			if (EditorGUI.EndChangeCheck())
			{
				skills[item].ChangeDescription(d);
				EditorUtility.SetDirty(skills);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();
			float m = EditorGUILayout.Slider("Multiplier", skills[item].LevelMultiplier, 0f, 20);
			if (EditorGUI.EndChangeCheck())
			{
				skills[item].ChangeMultiplier(m);
				EditorUtility.SetDirty(skills);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();
			float e = EditorGUILayout.Slider("Exponent", skills[item].LevelExponent, 0f, 10);
			if (EditorGUI.EndChangeCheck())
			{
				skills[item].ChangeExponent(e);
				EditorUtility.SetDirty(skills);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();
			float c = EditorGUILayout.Slider("Const", skills[item].LevelConst, 0f, 500);
			if (EditorGUI.EndChangeCheck())
			{
				skills[item].ChangeConst(c);
				EditorUtility.SetDirty(skills);
				AssetDatabase.Refresh();
			}

			GUILayout.EndVertical();

			if (GUILayout.Button("Remove"))
			{
				//Debug.Log("removing");
				skills.Remove(item);
				EditorUtility.SetDirty(skills);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
				return;
			}

			

			GUILayout.EndHorizontal();

			EditorGUILayout.Space(10f);
		}

		

		GUILayout.EndVertical();

		if (GUILayout.Button("Add"))
		{
			SkillData s = new SkillData("", "");
			skills.Add(s.ID, s);
			EditorUtility.SetDirty(skills);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		EditorGUILayout.EndScrollView();
	}
}
