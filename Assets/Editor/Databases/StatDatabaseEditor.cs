﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(StatDatabase))]
public class StatDatabaseEditor: EditorWindow
{
	Vector2 scrollPos;

	[MenuItem("Window/Stats")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(StatDatabaseEditor));
	}

	void OnGUI()
	{
		StatDatabase std = Resources.Load<StatDatabase>("Stats");//target as StatDatabase;

		//Debug.Log(std.keys.Count);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		GUILayout.BeginVertical();
		foreach (var item in std.keys)
		{

			GUILayout.BeginHorizontal();

			GUILayout.BeginVertical();

			EditorGUILayout.LabelField("ID",item);

			EditorGUI.BeginChangeCheck();
			string n = EditorGUILayout.TextField("Name",std[item].Name);

			if (EditorGUI.EndChangeCheck())
			{
				std[item].ChangeName(n);
				EditorUtility.SetDirty(std);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();
			string d = EditorGUILayout.TextField("Description",std[item].Description);

			if (EditorGUI.EndChangeCheck())
			{
				std[item].ChangeDescription(d);
				EditorUtility.SetDirty(std);
				AssetDatabase.Refresh();
			}

			EditorGUI.BeginChangeCheck();

			float b = EditorGUILayout.Slider("Base Value", std[item].Base, 0f, 100);
			if (EditorGUI.EndChangeCheck())
			{
				std[item].SetBase(b);
				EditorUtility.SetDirty(std);
				AssetDatabase.Refresh();
			}

			GUILayout.EndVertical();

			if (GUILayout.Button("Remove"))
			{
				Debug.Log("removing");
				std.Remove(item);
				EditorUtility.SetDirty(std);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
				return;
			}

			

			GUILayout.EndHorizontal();

			EditorGUILayout.Space(10f);
		}

		GUILayout.EndVertical();

		if (GUILayout.Button("Add"))
		{
			StatData s = new StatData("", "", 0);
			std.Add(s.ID, s);
			EditorUtility.SetDirty(std);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
		EditorGUILayout.EndScrollView();
	}
}
