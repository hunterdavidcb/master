﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(StatDatabase))]
public class AbilityDatabaseEditor : EditorWindow
{
	SkillDatabase skills;
	List<SkillData> skillsList;
	string[] skillNames;
	AbilityDatabase abilities;
	Vector2 scrollPos;

	[MenuItem("Window/Abiltities")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(AbilityDatabaseEditor));
	}

	private void OnEnable()
	{
		skills = Resources.Load<SkillDatabase>("Skills");//target as StatDatabase;
		skillsList = new List<SkillData>();
		foreach (var item in skills.keys)
		{
			//Debug.Log(skills[item].Name);
			//Debug.Log(skills[item].ID);
			skillsList.Add(skills[item]);
		}

		skillNames = new string[skillsList.Count];
		for (int i = 0; i < skillsList.Count; i++)
		{
			skillNames[i] = skillsList[i].Name;
		}

		abilities = Resources.Load<AbilityDatabase>("Abilities");//target as StatDatabase;
	}

	private void OnDisable()
	{

	}

	void OnGUI()
	{

		if (abilities == null)
		{
			Debug.Log("loading, null");
			abilities = Resources.Load<AbilityDatabase>("Abilities");//target as StatDatabase;
		}

		//Debug.Log(skills.keys.Count);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		GUILayout.BeginVertical();
		{
			if (abilities != null)
			{
				foreach (var item in abilities.keys)
				{


					GUILayout.BeginHorizontal();
					{

						GUILayout.BeginVertical();
						{


							EditorGUILayout.LabelField("ID", item);

							EditorGUI.BeginChangeCheck();
							Sprite sprite = (Sprite)EditorGUILayout.ObjectField("Icon", abilities[item].Icon,typeof(Sprite),false);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeIcon(sprite);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							EditorGUI.BeginChangeCheck();
							string n = EditorGUILayout.TextField("Name", abilities[item].Name);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeName(n);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							EditorGUI.BeginChangeCheck();
							string d = EditorGUILayout.TextField("Description", abilities[item].Description);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeDescription(d);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}


							EditorGUI.BeginChangeCheck();
							//if you use a list and you want to check equality, you need to implement IEquatable
							int sel = EditorGUILayout.Popup(skillsList.IndexOf(abilities[item].SkillData), skillNames);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeSkill(skillsList[sel]);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							EditorGUI.BeginChangeCheck();
							float dur = EditorGUILayout.FloatField("Duration", abilities[item].Duration);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeDuration(dur);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							EditorGUI.BeginChangeCheck();
							float cd = EditorGUILayout.FloatField("Cooldown", abilities[item].CoolDown);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeCoolDown(cd);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							EditorGUI.BeginChangeCheck();
							uint rsl = (uint)EditorGUILayout.IntField("Required Skill Level", (int)abilities[item].RequiredSkillLevel);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeRequiredSkillLevel(rsl);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							//willpower cost
							EditorGUI.BeginChangeCheck();
							uint wr = (uint)EditorGUILayout.IntField("Cost", (int)abilities[item].WillpowerRequired);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeWillpowerRequired(wr);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}



							//animation
							EditorGUI.BeginChangeCheck();
							AnimatorOverrideController ani = (AnimatorOverrideController)EditorGUILayout.ObjectField(abilities[item].Anim, typeof(AnimatorOverrideController), false);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeAnimation(ani);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							//effect prefab
							EditorGUI.BeginChangeCheck();
							GameObject go = EditorGUILayout.ObjectField(abilities[item].Effect, typeof(GameObject), false) as GameObject;

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeEffectPrefab(go);
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							//target type
							EditorGUI.BeginChangeCheck();
							AbilityTarget at = (AbilityTarget)EditorGUILayout.EnumPopup("Target", abilities[item].Target);

							if (EditorGUI.EndChangeCheck())
							{
								abilities[item].ChangeTarget(at);
								if (at != AbilityTarget.Weapon)
								{
									abilities[item].ChangeBuffDamageType(DamageType.Neutral);
									abilities[item].ChangeBuffDamageAmount(0);
								}
								EditorUtility.SetDirty(abilities);
								AssetDatabase.Refresh();
							}

							if (at == AbilityTarget.Weapon)
							{
								//damage type
								EditorGUI.BeginChangeCheck();
								DamageType dt = (DamageType)EditorGUILayout.EnumPopup("Damage Type", abilities[item].DamageType);

								if (EditorGUI.EndChangeCheck())
								{
									abilities[item].ChangeBuffDamageType(dt);
									EditorUtility.SetDirty(abilities);
									AssetDatabase.Refresh();
								}


								//damage amount
								EditorGUI.BeginChangeCheck();
								uint damt = (uint)EditorGUILayout.IntField("Damage Amount", (int)abilities[item].DamageAmount);

								if (EditorGUI.EndChangeCheck())
								{
									abilities[item].ChangeBuffDamageAmount(damt);
									EditorUtility.SetDirty(abilities);
									AssetDatabase.Refresh();
								}
							}

							

						}
						GUILayout.EndVertical();

						if (GUILayout.Button("Remove"))
						{
							//Debug.Log("removing");
							abilities.Remove(item);
							EditorUtility.SetDirty(abilities);
							AssetDatabase.SaveAssets();
							AssetDatabase.Refresh();
							return;
						}


					}
					GUILayout.EndHorizontal();

					EditorGUILayout.Space(10f);
				}
			}
			


			if (GUILayout.Button("Add"))
			{
				AbilityData s = new AbilityData("", "", null);
				abilities.Add(s.ID, s);
				EditorUtility.SetDirty(abilities);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}

		}
		GUILayout.EndVertical();

		EditorGUILayout.EndScrollView();
	}
}
