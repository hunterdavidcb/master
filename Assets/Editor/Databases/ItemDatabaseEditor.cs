﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(StatDatabase))]
public class ItemDatabaseEditor: EditorWindow
{
	StatDatabase stats;
	List<StatData> statList;
	string[] statNames;

	ItemDatabase items;
	Vector2 scrollPos;

	[MenuItem("Window/Items")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(ItemDatabaseEditor));
	}

	private void OnEnable()
	{
		items = Resources.Load<ItemDatabase>("Items");//target as StatDatabase;


		stats = Resources.Load<StatDatabase>("Stats");//target as StatDatabase;
		statList = new List<StatData>();
		foreach (var item in stats.keys)
		{
			statList.Add(stats[item]);
		}

		statNames = new string[statList.Count];
		for (int i = 0; i < statList.Count; i++)
		{
			statNames[i] = statList[i].Name;
		}
	
	}

	void OnGUI()
	{
		//ItemDatabase items = Resources.Load<ItemDatabase>("Items");
		//Debug.Log(items.keys.Count);
		if (items != null)
		{
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			GUILayout.BeginVertical();
			for (int i = 0; i < items.keys.Count; i++)
			{
				GUILayout.BeginHorizontal();

				GUILayout.BeginVertical();
				EditorGUILayout.LabelField("ID", items.keys[i]);

				EditorGUI.BeginChangeCheck();
				string n = EditorGUILayout.TextField("Name", items[items.keys[i]].Name);

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeName(n);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				EditorGUI.BeginChangeCheck();
				string d = EditorGUILayout.TextField("Description", items[items.keys[i]].Description);

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeDescription(d);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				EditorGUI.BeginChangeCheck();
				Sprite s = EditorGUILayout.ObjectField(items[items.keys[i]].Icon, typeof(Sprite), false) as Sprite;

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeSprite(s);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				EditorGUI.BeginChangeCheck();
				GameObject go = EditorGUILayout.ObjectField(items[items.keys[i]].Prefab, typeof(GameObject), false) as GameObject;

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangePrefab(go);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				EditorGUI.BeginChangeCheck();
				DamageType dt = (DamageType)EditorGUILayout.EnumPopup("Weakness", items[items.keys[i]].WeakAgainst);

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeWeakness(dt);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				EditorGUI.BeginChangeCheck();
				uint hp = (uint)EditorGUILayout.IntField("HP", (int)items[items.keys[i]].HitPoints);

				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeHP(hp);
					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				float min = items[items.keys[i]].ProtectionAmountMin;
				float max = items[items.keys[i]].ProtectionAmountMax;
				
				EditorGUI.BeginChangeCheck();
				EditorGUILayout.MinMaxSlider(new GUIContent("Min and Max Protection","in a percentage between 0 (0%) and 1 (100%)")
					, ref min, ref max, 0f, 1f);
				if (EditorGUI.EndChangeCheck())
				{
					items[items.keys[i]].ChangeMin(min);
					items[items.keys[i]].ChangeMax(max);
					EditorUtility.SetDirty(items);
					//AssetDatabase.Refresh();
				}

				//for status effects
				for (int e = 0; e < items[items.keys[i]].Effects.Count; e++)
				{
					EditorGUI.BeginChangeCheck();
					int sel = EditorGUILayout.Popup(statList.IndexOf(items[items.keys[i]].Effects[e].StatData), statNames);

					if (EditorGUI.EndChangeCheck())
					{
						items[items.keys[i]].SetStatData(e,statList[sel]);
						//abilities[item].ChangeDescription(d);
						EditorUtility.SetDirty(items);
						AssetDatabase.Refresh();
					}
					//EditorGUILayout.LabelField(items[items.keys[i]].Effects[e].StatData.Name);
				}
				//foreach (var effect in items[items.keys[i]].Effects)
				//{
				//	EditorGUILayout.LabelField(effect.Key.Name);
				//	foreach (var se in effect.Value)
				//	{
				//		EditorGUILayout.LabelField(se.Amount.ToString());
				//	}
					
				//}
				EditorGUI.BeginChangeCheck();


				if (EditorGUI.EndChangeCheck())
				{

					EditorUtility.SetDirty(items);
					AssetDatabase.Refresh();
				}

				if (GUILayout.Button("Add Effect"))
				{
					StatusEffect se = new StatusEffect(statList[0], 0, 0, StatusEffectType.Flat);
					
					items[items.keys[i]].AddEffect( se);
					//items.Add(id.ID, id);
					EditorUtility.SetDirty(items);
					//ItemData id = new ItemData("", "");
					//items.Add(id.ID, id);
					//EditorUtility.SetDirty(items);
					//Context();
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
				}


				GUILayout.EndVertical();

				if (GUILayout.Button("Remove"))
				{
					Debug.Log("removing");
					items.Remove(items.keys[i]);
					EditorUtility.SetDirty(items);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
					return;
				}


				GUILayout.EndHorizontal();

				EditorGUILayout.Space(10f);

				//if (items[item] is Consumable)
				//{
				//	//Debug.Log("perserved");
				//}
			}
			GUILayout.EndVertical();
		}
			

		
			if (GUILayout.Button("Add"))
			{
				//Debug.Log("called");
				Consumable id = new Consumable("", "");
				items.Add(id.ID, id);
				EditorUtility.SetDirty(items);
				//ItemData id = new ItemData("", "");
				//items.Add(id.ID, id);
				//EditorUtility.SetDirty(items);
				//Context();
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}

		EditorGUILayout.EndScrollView();
	}

	//void Context()
	//{
	//	var menu = new GenericMenu();
	//	menu.AddItem(new GUIContent("Add Consumable"), false, AddConsumable);
	//	menu.AddItem(new GUIContent("Add Armor"), false, AddArmor);
	//	menu.AddItem(new GUIContent("Add Weapon"), false, AddWeapon);

	//	menu.ShowAsContext();

	//	//if (GUILayout.Button("Add Consumable"))
	//	//{

	//	//}

	//	//if (GUILayout.Button("Add Weapon"))
	//	//{

	//	//}

	//	//if (GUILayout.Button("Add Armor"))
	//	//{

	//	//}
	//}

	//void AddConsumable()
	//{
	//	Debug.Log("called");
	//	ItemData id = new Consumable("", "");
	//	items.Add(id.ID, id);
	//	EditorUtility.SetDirty(items);
	//}

	
}
