﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class GraphSaveUitility
{
	private DialogueView target;
	private DialogueContainer containerCache;

	private List<Edge> Edges => target.edges.ToList();
	private List<DialogueNode> Nodes => target.nodes.ToList().Cast<DialogueNode>().ToList();

	public static GraphSaveUitility GetInstance(DialogueView targetGraphView)
	{
		return new GraphSaveUitility { target = targetGraphView };
	}

	public void SaveGraph(string name)
	{
		name = name.Replace(Application.dataPath + "/Resources/", "");
		name = name.Remove(name.Length - 6);
		if (!Edges.Any())
		{
			return;
		}

		var dialogueContainer = ScriptableObject.CreateInstance<DialogueContainer>();

		var connectedPorts = Edges.Where(x => x.input.node != null).ToArray();
		for (int i = 0; i < connectedPorts.Length; i++)
		{
			var outputNode = connectedPorts[i].output.node as DialogueNode;
			var inputNode = connectedPorts[i].input.node as DialogueNode;

			dialogueContainer.nodeLinks.Add(new NodeLinkData
			{
				baseNodeID = outputNode.id,
				portName = connectedPorts[i].output.portName,
				targetNodeID = inputNode.id
			});
		}

		foreach (var dialogueNode in Nodes.Where(node =>!node.entryPoint))
		{
			dialogueContainer.dialogueNodeData.Add(new DialogueNodeData
			{
				id = dialogueNode.id,
				text = dialogueNode.dialogueText,
				position = dialogueNode.GetPosition().position
			});
		}

		if (!AssetDatabase.IsValidFolder("Assets/Resources"))
		{
			AssetDatabase.CreateFolder("Assets", "Resources");
		}

		AssetDatabase.CreateAsset(dialogueContainer, $"Assets/Resources/{name}.asset");
		AssetDatabase.SaveAssets();
	}

	public void LoadGraph(string name)
	{
		name = name.Replace(Application.dataPath + "/Resources/", "");
		name = name.Remove(name.Length - 6);
		Debug.Log(name);
		containerCache = Resources.Load<DialogueContainer>(name);

		if (containerCache == null)
		{
			Debug.Log("here");
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist", "OK");
			return;
		}

		ClearGraph();
		CreateNodes();
		ConnectNodes();
	}

	private void ConnectNodes()
	{
		for (int i = 0; i < Nodes.Count; i++)
		{
			var connections = containerCache.nodeLinks.Where(x =>
			x.baseNodeID == Nodes[i].id).ToList();
			for (int h = 0; h < connections.Count; h++)
			{
				var targetNodeID = connections[h].targetNodeID;
				var targetNode = Nodes.First(x => x.id == targetNodeID);
				LinkNodes(Nodes[i].outputContainer[h].Q<Port>(), (Port)targetNode.inputContainer[0]);

				targetNode.SetPosition(new Rect(containerCache.dialogueNodeData.First(x => x.id == targetNodeID).position, target.defaultNodeSize));
			}
		}
	}

	private void LinkNodes(Port output, Port input)
	{
		var tempEdge = new Edge
		{
			output = output,
			input = input
		};

		tempEdge.input.Connect(tempEdge);
		tempEdge.output.Connect(tempEdge);
		target.Add(tempEdge);
	}

	private void CreateNodes()
	{
		foreach (var node in containerCache.dialogueNodeData)
		{
			var temp = target.CreateDialogueNode(node.text);
			temp.id = node.id;
			target.AddElement(temp);
			//temp.SetPosition()

			var nodePorts = containerCache.nodeLinks.Where(x => x.baseNodeID == node.id).ToList();
			nodePorts.ForEach(x => target.AddChoicePort(temp,x.portName));
		}
	}

	private void ClearGraph()
	{
		//set entry point
		Nodes.Find(x => x.entryPoint).id = containerCache.nodeLinks[0].baseNodeID;

		foreach (var node in Nodes)
		{
			if (node.entryPoint)
			{
				continue;
			}

			//remove the edges
			Edges.Where(x => x.input.node == node).ToList().
				ForEach(edge => target.RemoveElement(edge));

			//remove the nodes
			target.RemoveElement(node);

		}
	}
}
