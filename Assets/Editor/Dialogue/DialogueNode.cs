﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;


public class DialogueNode : Node
{

	public string id;
	public string dialogueText;
	public bool entryPoint;

	public DialogueNode()
	{
		id = Guid.NewGuid().ToString();
	}
}
