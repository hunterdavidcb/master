﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class Dialogue : EditorWindow
{
	DialogueView dialogueGraph;
	private string fileName;

	[MenuItem("Graph/Dialogue")]
	public static void OpenGraphWindow()
	{
		var window = GetWindow<Dialogue>();
		window.titleContent = new GUIContent("Dialogue");
	}

	private void OnEnable()
	{
		ConstructGraphView();
		GenerateToolBar();
		GenerateMinimap();
	}

	private void GenerateMinimap()
	{
		var minimap = new MiniMap { anchored = true};
		minimap.SetPosition(new Rect(10, 30, 200, 140));
		dialogueGraph.Add(minimap);
	}

	private void GenerateToolBar()
	{
		var toolbar = new Toolbar();

		//var fileNameTextField = new TextField("File Name");
		//fileNameTextField.SetValueWithoutNotify("New Narrative");
		//fileNameTextField.MarkDirtyRepaint();
		//fileNameTextField.RegisterValueChangedCallback((evt) => fileName = evt.newValue);

		//toolbar.Add(fileNameTextField);

		toolbar.Add(new Button(() => SaveData()) { text = "Save Data" });
		toolbar.Add(new Button(() => LoadData()) { text = "Load Data" });


		var nodeCreateButton = new Button(() => { dialogueGraph.CreateNode("New"); });
		nodeCreateButton.text = "Create Node";
		toolbar.Add(nodeCreateButton);

		rootVisualElement.Add(toolbar);
	}

	private void LoadData()
	{
		fileName = EditorUtility.OpenFilePanel("Load Dialogue","Assets/Resources","asset" );
		Debug.Log(fileName);

		if (string.IsNullOrEmpty(fileName))
		{
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist","OK");
			return;
		}

		var load = GraphSaveUitility.GetInstance(dialogueGraph);
		load.LoadGraph(fileName);
	}

	private void SaveData()
	{
		fileName = EditorUtility.SaveFilePanel("Save Dialogue", "Assets/Resources","New Dialogue" ,"asset");
		if (string.IsNullOrEmpty(fileName))
		{
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist", "OK");
			return;
		}

		var save = GraphSaveUitility.GetInstance(dialogueGraph);
		save.SaveGraph(fileName);
	}

	private void ConstructGraphView()
	{
		dialogueGraph = new DialogueView();
		dialogueGraph.StretchToParentSize();
		rootVisualElement.Add(dialogueGraph);
	}

	private void OnDisable()
	{
		rootVisualElement.Remove(dialogueGraph);
	}
}
