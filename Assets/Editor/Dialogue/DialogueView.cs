﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class DialogueView : GraphView
{
	public readonly Vector2 defaultNodeSize = new Vector2(150, 200);

	public DialogueView()
	{
		styleSheets.Add(Resources.Load<StyleSheet>("DialogueGraph"));

		SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
		this.AddManipulator(new ContentDragger());
		this.AddManipulator(new SelectionDragger());
		this.AddManipulator(new RectangleSelector());


		var grid = new GridBackground();

		Insert(0, grid);

		grid.StretchToParentSize();

		AddElement(GenerateEntryPointNode());
	}

	private Port GeneratePort(DialogueNode sn, Direction portDirection, Port.Capacity capacity = Port.Capacity.Single)
	{
		return sn.InstantiatePort(Orientation.Horizontal, portDirection, capacity, typeof(float));
	}

	public void CreateNode(string name)
	{
		AddElement(CreateDialogueNode(name));
	}

	public DialogueNode CreateDialogueNode(string nodeName)
	{
		var dialogueNode = new DialogueNode();

		dialogueNode.title = nodeName;
		dialogueNode.dialogueText = nodeName;

		//for inputs
		var inputPort = GeneratePort(dialogueNode, Direction.Input, Port.Capacity.Multi);
		inputPort.portName = "Input";
		dialogueNode.inputContainer.Add(inputPort);

		//for loading the style
		dialogueNode.styleSheets.Add(Resources.Load<StyleSheet>("Node"));

		//for adding outputs
		var button = new Button(() => AddChoicePort(dialogueNode));
		button.text = "New Choice";
		dialogueNode.titleContainer.Add(button);


		//for showing and editing the dialogue text
		var textField = new TextField(string.Empty);
		textField.RegisterValueChangedCallback(evt =>
		{
			dialogueNode.dialogueText = evt.newValue;
			dialogueNode.title = evt.newValue;
		});

		textField.SetValueWithoutNotify(dialogueNode.title);
		dialogueNode.contentContainer.Add(textField);

		//refreshing
		dialogueNode.RefreshExpandedState();
		dialogueNode.RefreshPorts();
		dialogueNode.SetPosition(new Rect(Vector2.zero, defaultNodeSize));

		//dialogueNode.capabilities &= Capabilities.Movable;

		return dialogueNode;

	}

	public void AddChoicePort(DialogueNode dialogueNode, string name = null)
	{
		var generatedPort = GeneratePort(dialogueNode,Direction.Output);

		//generatedPort.

		var oldLabel = generatedPort.contentContainer.Q<Label>("type");
		generatedPort.contentContainer.Remove(oldLabel);

		var outputPortCount = dialogueNode.outputContainer.Query("connector").ToList().Count;
		var outputPortName = $"Choice {outputPortCount+1}";
		var choicePortName = string.IsNullOrEmpty(name) ? outputPortName : name;

		var textField = new TextField
		{
			name = string.Empty,
			value = choicePortName
		};

		textField.RegisterValueChangedCallback(evt => 
		generatedPort.portName = evt.newValue);

		generatedPort.contentContainer.Add(new Label("  "));
		generatedPort.contentContainer.Add(textField);

		var deleteButton = new Button(() => RemovePort(dialogueNode, generatedPort))
		{
			text = "X"
		};

		generatedPort.contentContainer.Add(deleteButton);


		generatedPort.portName = choicePortName;

		dialogueNode.outputContainer.Add(generatedPort);

		dialogueNode.RefreshExpandedState();
		dialogueNode.RefreshPorts();
	}

	private void RemovePort(DialogueNode dialogueNode, Port generatedPort)
	{
		var targetEdge = edges.ToList().Where(x => x.output.portName == generatedPort.portName
		&& x.output.node == generatedPort.node);

		if (!targetEdge.Any())
		{
			return;
		}

		var edge = targetEdge.First();
		edge.input.Disconnect(edge);
		RemoveElement(targetEdge.First()); //RemoveElement(edge);

		//remove the port
		dialogueNode.outputContainer.Remove(generatedPort);
		dialogueNode.RefreshPorts();
		dialogueNode.RefreshExpandedState();
		
	}

	public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
	{
		var compatiblePorts = new List<Port>();

		ports.ForEach((port) =>
		{
			if (startPort != port && startPort.node != port.node)
			{
				compatiblePorts.Add(port);
			}
		});

		return compatiblePorts;
	}

	private DialogueNode GenerateEntryPointNode()
	{
		var node = new DialogueNode();
		node.title = "Start";
		node.dialogueText = "Entry Point";
		node.entryPoint = true;

		node.SetPosition(new Rect(100, 200, 100, 150));

		
		var port = GeneratePort(node, Direction.Output);

		port.portName = "Next";

		node.outputContainer.Add(port);

		node.capabilities &= ~Capabilities.Movable;
		node.capabilities &= ~Capabilities.Deletable;

		node.RefreshExpandedState();
		node.RefreshPorts();

		return node;
	}
}
