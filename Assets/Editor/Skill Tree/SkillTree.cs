﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class SkillTree : EditorWindow
{
	SkillTreeView skillTree;
	private string fileName;

	[MenuItem("Graph/Skill Tree")]
	public static void OpenGraphWindow()
	{
		var window = GetWindow<SkillTree>();
		window.titleContent = new GUIContent("Skill Tree");
	}

	private void OnEnable()
	{
		ConstructGraphView();
		GenerateToolBar();
		GenerateMinimap();
	}

	private void GenerateMinimap()
	{
		var minimap = new MiniMap { anchored = true };
		minimap.SetPosition(new Rect(10, 30, 200, 140));
		skillTree.Add(minimap);
	}

	private void GenerateToolBar()
	{
		var toolbar = new Toolbar();

		//var fileNameTextField = new TextField("File Name");
		//fileNameTextField.SetValueWithoutNotify("New Narrative");
		//fileNameTextField.MarkDirtyRepaint();
		//fileNameTextField.RegisterValueChangedCallback((evt) => fileName = evt.newValue);

		//toolbar.Add(fileNameTextField);

		toolbar.Add(new Button(() => SaveData()) { text = "Save Data" });
		toolbar.Add(new Button(() => LoadData()) { text = "Load Data" });


		var nodeCreateButton = new Button(() => { skillTree.CreateNode("New"); });
		nodeCreateButton.text = "Create Node";
		toolbar.Add(nodeCreateButton);

		rootVisualElement.Add(toolbar);
	}

	private void LoadData()
	{
		fileName = EditorUtility.OpenFilePanel("Load Skill Tree", "Assets/Resources", "asset");
		Debug.Log(fileName);

		if (string.IsNullOrEmpty(fileName))
		{
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist", "OK");
			return;
		}

		var load = TreeSaveUitility.GetInstance(skillTree);
		load.LoadGraph(fileName);
	}

	private void SaveData()
	{
		fileName = EditorUtility.SaveFilePanel("Save Skill", "Assets/Resources", "New Skill Tree", "asset");
		if (string.IsNullOrEmpty(fileName))
		{
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist", "OK");
			return;
		}

		var save = TreeSaveUitility.GetInstance(skillTree);
		save.SaveGraph(fileName);
	}

	private void ConstructGraphView()
	{
		skillTree = new SkillTreeView();
		skillTree.StretchToParentSize();
		rootVisualElement.Add(skillTree);
	}

	private void OnDisable()
	{
		rootVisualElement.Remove(skillTree);
	}
}
