﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class TreeSaveUitility
{
	private SkillTreeView target;
	private SkillContainer containerCache;

	private List<Edge> Edges => target.edges.ToList();
	private List<SkillNode> Nodes => target.nodes.ToList().Cast<SkillNode>().ToList();

	public static TreeSaveUitility GetInstance(SkillTreeView targetGraphView)
	{
		return new TreeSaveUitility { target = targetGraphView };
	}

	public void SaveGraph(string name)
	{
		name = name.Replace(Application.dataPath + "/Resources/", "");
		name = name.Remove(name.Length - 6);
		if (!Edges.Any())
		{
			return;
		}

		var skillContainer = ScriptableObject.CreateInstance<SkillContainer>();

		var connectedPorts = Edges.Where(x => x.input.node != null).ToArray();
		for (int i = 0; i < connectedPorts.Length; i++)
		{
			var outputNode = connectedPorts[i].output.node as SkillNode;
			var inputNode = connectedPorts[i].input.node as SkillNode;

			skillContainer.nodeLinks.Add(new NodeLinkData
			{
				baseNodeID = outputNode.id,
				portName = connectedPorts[i].output.portName,
				targetNodeID = inputNode.id
			});
		}

		foreach (var skillNode in Nodes.Where(node =>!node.entryPoint))
		{
			skillContainer.skillNodeData.Add(new SkillNodeData
			{
				id = skillNode.id,
				skillName = skillNode.skillName,
				requiredSkillLevel = skillNode.requiredSkillLevel,
				position = skillNode.GetPosition().position
			});
		}

		if (!AssetDatabase.IsValidFolder("Assets/Resources"))
		{
			AssetDatabase.CreateFolder("Assets", "Resources");
		}

		AssetDatabase.CreateAsset(skillContainer, $"Assets/Resources/{name}.asset");
		AssetDatabase.SaveAssets();
	}

	public void LoadGraph(string name)
	{
		name = name.Replace(Application.dataPath + "/Resources/", "");
		name = name.Remove(name.Length - 6);
		Debug.Log(name);
		containerCache = Resources.Load<SkillContainer>(name);

		if (containerCache == null)
		{
			Debug.Log("here");
			EditorUtility.DisplayDialog("Invalid file name!", "That file does not exist", "OK");
			return;
		}

		ClearGraph();
		CreateNodes();
		ConnectNodes();
	}

	private void ConnectNodes()
	{
		for (int i = 0; i < Nodes.Count; i++)
		{
			var connections = containerCache.nodeLinks.Where(x =>
			x.baseNodeID == Nodes[i].id).ToList();
			for (int h = 0; h < connections.Count; h++)
			{
				var targetNodeID = connections[h].targetNodeID;
				var targetNode = Nodes.First(x => x.id == targetNodeID);
				LinkNodes(Nodes[i].outputContainer[h].Q<Port>(), (Port)targetNode.inputContainer[0]);

				targetNode.SetPosition(new Rect(containerCache.skillNodeData.First(x => x.id == targetNodeID).position, target.defaultNodeSize));
			}
		}
	}

	private void LinkNodes(Port output, Port input)
	{
		var tempEdge = new Edge
		{
			output = output,
			input = input
		};

		tempEdge.input.Connect(tempEdge);
		tempEdge.output.Connect(tempEdge);
		target.Add(tempEdge);
	}

	private void CreateNodes()
	{
		foreach (var node in containerCache.skillNodeData)
		{
			var temp = target.CreateSkillNode(node.skillName);
			temp.id = node.id;
			target.AddElement(temp);
			//temp.SetPosition()

			var nodePorts = containerCache.nodeLinks.Where(x => x.baseNodeID == node.id).ToList();
			nodePorts.ForEach(x => target.AddChoicePort(temp,x.portName));
		}
	}

	private void ClearGraph()
	{
		//set entry point
		Nodes.Find(x => x.entryPoint).id = containerCache.nodeLinks[0].baseNodeID;

		foreach (var node in Nodes)
		{
			if (node.entryPoint)
			{
				continue;
			}

			//remove the edges
			Edges.Where(x => x.input.node == node).ToList().
				ForEach(edge => target.RemoveElement(edge));

			//remove the nodes
			target.RemoveElement(node);

		}
	}
}
