﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class SkillTreeView : GraphView
{
	public readonly Vector2 defaultNodeSize = new Vector2(150, 200);
	public SkillTreeView()
	{
		styleSheets.Add(Resources.Load<StyleSheet>("DialogueGraph"));

		SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
		this.AddManipulator(new ContentDragger());
		this.AddManipulator(new SelectionDragger());
		this.AddManipulator(new RectangleSelector());

		var grid = new GridBackground();

		Insert(0, grid);

		grid.StretchToParentSize();

		AddElement(GenerateEntryPointNode());
	}

	private Port GeneratePort(SkillNode sn, Direction portDirection, Port.Capacity capacity = Port.Capacity.Single)
	{
		return sn.InstantiatePort(Orientation.Horizontal, portDirection, capacity, typeof(float));
	}

	public void CreateNode(string name)
	{
		AddElement(CreateSkillNode(name));
	}

	public SkillNode CreateSkillNode(string nodeName)
	{
		var skillNode = new SkillNode();

		skillNode.title = nodeName;
		skillNode.skillName = nodeName;

		//for inputs
		var inputPort = GeneratePort(skillNode, Direction.Input, Port.Capacity.Multi);
		inputPort.portName = "Input";
		skillNode.inputContainer.Add(inputPort);

		//for loading the style
		skillNode.styleSheets.Add(Resources.Load<StyleSheet>("Node"));

		//for adding outputs
		var button = new Button(() => AddChoicePort(skillNode));
		button.text = "New Choice";
		skillNode.titleContainer.Add(button);


		//for showing and editing the dialogue text
		var intField = new IntegerField();
		intField.RegisterValueChangedCallback(evt =>
		{
			skillNode.requiredSkillLevel = (uint)evt.newValue;
		});

		var textField = new TextField(string.Empty);
		textField.RegisterValueChangedCallback(evt =>
		{
			skillNode.skillName = evt.newValue;
			skillNode.title = evt.newValue;
		});

		textField.SetValueWithoutNotify(skillNode.title);
		skillNode.contentContainer.Add(textField);

		skillNode.contentContainer.Add(intField);

		//refreshing
		skillNode.RefreshExpandedState();
		skillNode.RefreshPorts();
		skillNode.SetPosition(new Rect(Vector2.zero, defaultNodeSize));

		//skillNode.capabilities &= Capabilities.Resizable;

		return skillNode;

	}

	public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
	{
		var compatiblePorts = new List<Port>();

		ports.ForEach((port) =>
		{
			if (startPort != port && startPort.node != port.node)
			{
				compatiblePorts.Add(port);
			}
		});

		return compatiblePorts;
	}

	public void AddChoicePort(SkillNode skillNode, string name = null)
	{
		var generatedPort = GeneratePort(skillNode, Direction.Output);

		var oldLabel = generatedPort.contentContainer.Q<Label>("type");
		generatedPort.contentContainer.Remove(oldLabel);

		var outputPortCount = skillNode.outputContainer.Query("connector").ToList().Count;
		var outputPortName = $"Choice {outputPortCount + 1}";
		var choicePortName = string.IsNullOrEmpty(name) ? outputPortName : name;

		var textField = new TextField
		{
			name = string.Empty,
			value = choicePortName
		};

		textField.RegisterValueChangedCallback(evt =>
		generatedPort.portName = evt.newValue);

		generatedPort.contentContainer.Add(new Label("  "));
		generatedPort.contentContainer.Add(textField);

		var deleteButton = new Button(() => RemovePort(skillNode, generatedPort))
		{
			text = "X"
		};

		generatedPort.contentContainer.Add(deleteButton);


		generatedPort.portName = choicePortName;

		skillNode.outputContainer.Add(generatedPort);

		skillNode.RefreshExpandedState();
		skillNode.RefreshPorts();
	}

	private SkillNode GenerateEntryPointNode()
	{
		var node = new SkillNode();

		node.entryPoint = true;

		node.SetPosition(new Rect(100, 200, 100, 150));

		var port = GeneratePort(node, Direction.Output);
		port.portName = "out";
		node.outputContainer.Add(port);

		node.capabilities &= ~Capabilities.Movable;
		node.capabilities &= ~Capabilities.Deletable;

		node.RefreshExpandedState();
		node.RefreshPorts();

		return node;
	}

	private void RemovePort(SkillNode skillNode, Port generatedPort)
	{
		var targetEdge = edges.ToList().Where(x => x.output.portName == generatedPort.portName
		&& x.output.node == generatedPort.node);

		if (!targetEdge.Any())
		{
			return;
		}

		var edge = targetEdge.First();
		edge.input.Disconnect(edge);
		RemoveElement(targetEdge.First()); //RemoveElement(edge);

		//remove the port
		skillNode.outputContainer.Remove(generatedPort);
		skillNode.RefreshPorts();
		skillNode.RefreshExpandedState();

	}
}
