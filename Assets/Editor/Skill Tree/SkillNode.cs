﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;


public class SkillNode : Node
{
	public string id;
	public string skillName;
	public uint requiredSkillLevel;
	public bool entryPoint;

	public SkillNode()
	{
		id = Guid.NewGuid().ToString();
	}
}
