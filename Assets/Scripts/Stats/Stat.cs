﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

[Serializable]
public class Stat : IStat
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;

	[SerializeField]
	float current;
	[SerializeField]
	float baseValue;
	[SerializeField]
	List<Skill> skills;

	[SerializeField]
	private readonly List<StatusEffect> statusEffects;

	public readonly ReadOnlyCollection<StatusEffect> StatusEffects;

	bool isDirty = true;


	public delegate void CurrentValueHandler(float cv);
	//the field part is necessary because.....REASONS
	//FU!!!
	[field: NonSerialized]
	public event CurrentValueHandler CurrentValueChanged;

	public Stat(string name, string description, string id, float b)
	{
		this.name = name;
		this.description = description;
		this.baseValue = b;
		this.id = id;

		statusEffects = new List<StatusEffect>();
		StatusEffects = statusEffects.AsReadOnly();

		current = CalculateValue();
	}

	public Stat(StatData sd)
	{
		name = sd.Name;
		description = sd.Description;
		this.baseValue = sd.Base;
		id = sd.ID;
		//add status effects???
		statusEffects = new List<StatusEffect>();
		StatusEffects = statusEffects.AsReadOnly();
	}

	public float Current
	{
		get 
		{
			if (isDirty)
			{
				current = CalculateValue();
				isDirty = false;
			}
			return current;
		}
	}


	public float Base
	{
		get { return baseValue; }
	}

	public List<SkillData> Skills
	{
		get { return null; }
		//get { return skills; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}


	public string ID
	{
		get { return id; }
	}

	public void AddStatusEffect(StatusEffect se)
	{
		statusEffects.Add(se);
		isDirty = true;
		statusEffects.Sort(CompareModifierOrder);
	}

	int CompareModifierOrder(StatusEffect a, StatusEffect b)
	{
		if (a.Order < b.Order)
		{
			return -1;
		}
		else if (a.Order > b.Order)
		{
			return 1;
		}
		return 0;
	}

	public bool RemoveStatusEffect(StatusEffect se)
	{
		
		bool b = statusEffects.Remove(se);
		isDirty = b;
		statusEffects.Sort(CompareModifierOrder);
		return b;
	}

	public bool RemoveAllModifiersFromSource(object source)
	{
		bool r = false;
		for (int i = statusEffects.Count - 1 ; i > -1; i--)
		{
			if (statusEffects[i].Source == source)
			{
				isDirty = true;
				statusEffects.RemoveAt(i);
				r = true;
			}
		}

		statusEffects.Sort(CompareModifierOrder);
		return r;
	}

	public void AddSkill(SkillData skill)
	{
		//throw new NotImplementedException();
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void RemoveSkill(SkillData skill)
	{
		//throw new NotImplementedException();
	}

	public void SetBase(float b)
	{
		if (isDirty || baseValue != b)
		{
			baseValue = b;
			current = CalculateValue();
			isDirty = false;
		}
		
	}

	//modified from Kryzarel
	float CalculateValue()
	{
		float f = baseValue;
		float sumPercentAdd = 0;

		for (int i = 0; i < statusEffects.Count; i++)
		{
			StatusEffect se = statusEffects[i];
			switch (se.Type)
			{
				case StatusEffectType.Flat:
					f += statusEffects[i].Amount;
					break;
				case StatusEffectType.PercentAdd:
					sumPercentAdd += se.Amount;
					
					if (i + 1 >= statusEffects.Count || statusEffects[i+1].Type != StatusEffectType.PercentAdd)
					{
						f *= 1 + sumPercentAdd;
						sumPercentAdd = 0;
					}
					break;
				case StatusEffectType.PercentMult:
					f *= 1 + se.Amount;
					break;
				default:
					break;
			}
			
		}

		return (float)Math.Round(f, 4);
	}

	public void SetCurrentValue(float c)
	{
		if (c > baseValue)
		{
			if (current != baseValue)
			{
				current = baseValue;
				if (CurrentValueChanged != null)
				{
					CurrentValueChanged(current);
				}
			}
			return;
		}

		if (c < 0)
		{
			if (current != 0)
			{
				current = 0;
				if (CurrentValueChanged != null)
				{
					CurrentValueChanged(current);
				}
			}
			return;
		}

		if (current != c)
		{
			current = c;
			if (CurrentValueChanged != null)
			{
				CurrentValueChanged(current);
			}
		}
	}
}
