﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//modified from Kryzarel
[Serializable]
public class StatusEffect
{
    [SerializeField]
    StatData statData;

    public readonly float Duration;
    public readonly StatusEffectType Type;
    public readonly float Amount;
    public readonly int Order;
    public readonly object Source;

    public StatData StatData
	{
        get { return statData; }
	}

    public StatusEffect(StatData sd, float a, float d, StatusEffectType t, int o, object s)
	{
        statData = sd;
        Amount = a;
        Duration = d;
        Type = t;
        Order = o;
        Source = s;
	}

    public StatusEffect(StatData sd, float a, float d, StatusEffectType t) : this (sd, a,d,t,(int)t, null) {   }
    public StatusEffect(StatData sd, float a, float d, StatusEffectType t, int o ) : this(sd, a, d, t, o, null) { }

    public StatusEffect(StatData sd, float a, float d, StatusEffectType t, object o) : this(sd, a, d, t, (int)t, o) { }

    public void SetStatData(StatData sd)
	{
        statData = sd;
	}
}

public enum StatusEffectType
{
    Flat,
    PercentAdd,
    PercentMult
}
