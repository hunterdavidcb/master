﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatData : IStat, IEquatable<StatData>, IEqualityComparer<StatData>
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	float baseValue;
	[SerializeField]
	List<SkillData> skills;

	public StatData(string name, string description, float b)
	{
		this.name = name;
		this.description = description;
		this.baseValue = b;
		id = Guid.NewGuid().ToString();
	}

	public float Base
	{
		get { return baseValue; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}

	public string ID
	{
		get { return id; }
	}

	public List<SkillData> Skills
	{
		get { return skills; }
	}

	public void AddSkill(SkillData skill)
	{
		if (skills == null)
		{
			skills = new List<SkillData>();
		}

		if (!skills.Contains(skill))
		{
			skills.Add(skill);
		}
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public bool Equals(StatData x, StatData y)
	{
		return x.ID.Equals(y.ID);
	}

	public bool Equals(StatData other)
	{
		return id.Equals(other.ID);
	}

	public int GetHashCode(StatData obj)
	{
		return obj.ID.GetHashCode();
	}

	public void RemoveSkill(SkillData skill)
	{
		skills.Remove(skill);
	}

	public void SetBase(float b)
	{
		baseValue = b;
	}
}
