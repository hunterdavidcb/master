﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SkillContainer : ScriptableObject
{
	public List<SkillNodeData> skillNodeData = new List<SkillNodeData>();
	public List<NodeLinkData> nodeLinks = new List<NodeLinkData>();
}