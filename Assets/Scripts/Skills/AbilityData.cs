﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AbilityData : ISkill, IEquatable<AbilityData>
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	SkillData skillData;
	[SerializeField]
	float duration;
	[SerializeField]
	float coolDown;
	[SerializeField]
	uint requiredSkillLevel;
	[SerializeField]
	uint willpowerRequired;
	[SerializeField]
	AnimatorOverrideController animatorOverride;
	[SerializeField]
	AbilityTarget target;
	//for use with weapons
	[SerializeField]
	private DamageType buffDamageType;
	[SerializeField]
	private uint buffDamageAmt;
	[SerializeField]
	GameObject effectPrefab;
	[SerializeField]
	Sprite icon;




	public AbilityData(string n, string d, SkillData sd)
	{
		name = n;
		description = d;
		skillData = sd;
		id = Guid.NewGuid().ToString();
	}

	public float LevelMultiplier
	{
		get { return -1f; }
	}

	public float LevelConst
	{
		get { return -1f; }
	}

	public float LevelExponent
	{
		get { return -1f; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}


	public string ID
	{
		get { return id; }
	}

	public SkillData SkillData
	{
		get { return skillData; }
	}

	public float Duration
	{
		get { return duration; }
	}

	public float CoolDown
	{
		get { return coolDown; }
	}

	public uint RequiredSkillLevel
	{
		get { return requiredSkillLevel; }
	}

	public uint WillpowerRequired
	{
		get { return willpowerRequired; }
	}

	public AbilityTarget Target
	{
		get { return target; }
	}

	public AnimatorOverrideController Anim
	{
		get { return animatorOverride; }
	}

	public GameObject Effect
	{
		get { return effectPrefab; }
	}

	public DamageType DamageType
	{
		get { return buffDamageType; }
	}

	public uint DamageAmount
	{
		get { return buffDamageAmt; }
	}

	public void AddXP(uint amt)
	{
		//throw new NotImplementedException();
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeSkill(SkillData sd)
	{
		skillData = sd;
	}

	public void ChangeDuration(float d)
	{
		duration = d;
	}

	public void ChangeCoolDown(float cd)
	{
		coolDown = cd;
	}

	public void ChangeRequiredSkillLevel(uint rsl)
	{
		requiredSkillLevel = rsl;
	}

	public void ChangeTarget(AbilityTarget at)
	{
		target = at;
	}

	public void ChangeBuffDamageType(DamageType dt)
	{
		buffDamageType = dt;
	}

	public void ChangeBuffDamageAmount(uint amt)
	{
		buffDamageAmt = amt;
	}

	public void ChangeIcon(Sprite i)
	{
		icon = i;
	}

	public void ChangeAnimation(AnimatorOverrideController ani)
	{
		animatorOverride = ani;
	}

	public void ChangeEffectPrefab(GameObject effect)
	{
		effectPrefab = effect;
	}

	public void ChangeWillpowerRequired(uint rw)
	{
		willpowerRequired = rw;
	}

	public bool Equals(AbilityData x, AbilityData y)
	{
		return x.ID.Equals(y.ID);
	}

	public int GetHashCode(AbilityData obj)
	{
		return obj.ID.GetHashCode();
	}

	public bool Equals(AbilityData other)
	{
		return ID.Equals(other.ID);
	}
}

public enum AbilityTarget
{
	Self,
	Weapon,
	Enemy,
	Area
}
