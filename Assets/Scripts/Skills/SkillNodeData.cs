﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SkillNodeData
{
	public string id;
	public string skillName;
	public uint requiredSkillLevel;
	public Vector2 position;
}