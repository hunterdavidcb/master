﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Skill : ISkill, IEqualityComparer<Skill>
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	float levelMultiplier;
	[SerializeField]
	float levelExponent;
	[SerializeField]
	float levelConst;
	[SerializeField]
	float currentXP;
	[SerializeField]
	uint currentLevel;
	[SerializeField]
	uint abilitiesPerTier;

	public delegate void SkillChangeHandler(Skill skill);
	//the field part is necessary because.....REASONS
	//FU!
	[field:NonSerialized]
	public event SkillChangeHandler LeveledUp;
	//the field part is necessary because.....REASONS
	//FU!
	[field: NonSerialized]
	public event SkillChangeHandler XPChanged;

	public float CurrentXP
	{
		get { return currentXP; }
	}

	
	public float XPToNextLevel
	{
		get
		{
			return Mathf.Ceil((levelMultiplier * Mathf.Pow(currentLevel, levelExponent)) + levelConst) ;
		}
	}

	public uint CurrentLevel
	{
		get { return currentLevel; }
	}

	public uint AbilitiesPerTier
	{
		get { return abilitiesPerTier; }
	}

	bool CanLevelUp
	{
		get { return currentXP >= XPToNextLevel; }
	}

	public float LevelMultiplier
	{
		get { return levelMultiplier; }
	}

	public float LevelConst
	{
		get { return levelConst; }
	}

	public float LevelExponent
	{
		get { return levelExponent; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}


	public string ID
	{
		get { return id; }
	}

	public Skill(string name, string description, string id, float lm, float lc, float le)
	{
		this.name = name;
		this.description = description;
		levelMultiplier = lm;
		levelConst = lc;
		levelExponent = le;
		this.id = id;
		currentLevel = 1;
	}

	public Skill(SkillData sd)
	{
		name = sd.Name;
		description = sd.Description;
		levelMultiplier = sd.LevelMultiplier;
		levelConst = sd.LevelConst;
		levelExponent = sd.LevelExponent;
		id = sd.ID;
		currentLevel = 1;
		abilitiesPerTier = sd.AbilitiesPerTier;
	}

	void LevelUp()
	{
		Debug.Log("leveling up");
		
		while (currentXP >= XPToNextLevel)
		{
			//we need to to this first, because XPToNextLevel is a property
			//dependent on currentLevel
			currentXP -= XPToNextLevel;

			//now we increment the level
			currentLevel++;
			

			//Debug.Log(currentXP);

			if (XPChanged != null)
			{
				XPChanged(this);
			}

			if (LeveledUp != null)
			{
				LeveledUp(this);
			}

			//Debug.Log(XPToNextLevel);
		}

		//Debug.Log(currentLevel);
	}

	public void AddXP(uint amt)
	{
		
		currentXP += amt;

		if (XPChanged != null)
		{
			XPChanged(this);
		}

		if (CanLevelUp)
		{
			LevelUp();
		}

		//Debug.Log("added " + amt);
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeAbilitiesPerTier(uint apt)
	{
		abilitiesPerTier = apt;
	}

	public bool Equals(Skill x, Skill y)
	{
		return x.ID.Equals(y.ID);
	}

	public int GetHashCode(Skill obj)
	{
		return obj.ID.GetHashCode();
	}
}
