﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCommand : ISkillCommand
{
	ISkill skill;

	public ISkill Skill
	{
		get { return skill; }
	}

	public SkillCommand(Skill sk)
	{
		skill = sk;
	}
	
	public void Execute()
	{
		skill.AddXP(10);
	}
}
