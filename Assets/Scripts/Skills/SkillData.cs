﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SkillData : ISkill, IEqualityComparer<SkillData>, IEquatable<SkillData>
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	float levelMultiplier;
	[SerializeField]
	float levelExponent;
	[SerializeField]
	float levelConst;
	[SerializeField]
	uint abilitiesPerTier;

	public SkillData(string n, string d)
	{
		name = n;
		description = d;
		id = Guid.NewGuid().ToString();
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}


	public string ID
	{
		get { return id; }
	}

	public float LevelMultiplier
	{
		get { return levelMultiplier; }
	}

	public float LevelConst
	{
		get { return levelConst; }
	}

	public float LevelExponent
	{
		get { return levelExponent; }
	}

	public uint AbilitiesPerTier
	{
		get { return abilitiesPerTier; }
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeMultiplier(float f)
	{
		levelMultiplier = f;
	}

	public void ChangeConst(float f)
	{
		levelConst = f;
	}

	public void ChangeExponent(float f)
	{
		levelExponent = f;
	}

	public void ChangeAbilitiesPerTier(uint apt)
	{
		abilitiesPerTier = apt;
	}

	public void AddXP(uint amt)
	{
		//throw new NotImplementedException();
	}

	public bool Equals(SkillData x, SkillData y)
	{
		return x.ID.Equals(y.ID);
	}

	public int GetHashCode(SkillData obj)
	{
		return obj.ID.GetHashCode();
	}

	public bool Equals(SkillData other)
	{
		return id.Equals(other.ID);
	}
}
