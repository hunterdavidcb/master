﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Abilities")]
public class AbilityDatabase : Database<AbilityData>
{

}
