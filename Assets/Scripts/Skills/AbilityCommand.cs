﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AbilityCommand : ISkillCommand
{
	Ability ability;
	PlayerAttack pa;

	public Ability Ability
	{
		get { return ability; }
	}

	public AbilityCommand(Ability ab, PlayerAttack pa)
	{
		ability = ab;
		this.pa = pa;
	}


	public void Execute()
	{
		ability.Skill.AddXP(10);
		//Debug.Log(ability.Name);
		//Debug.Log(ability.Skill.Name);
		switch (ability.Target)
		{
			case AbilityTarget.Self:
				break;
			case AbilityTarget.Weapon:
				//Debug.Log("correct place");
				pa.CurrentWeaponCollider.AddBuff(ability.DamageType, ability.DamageAmount, ability.Duration,ability.Skill);
				break;
			case AbilityTarget.Enemy:
				break;
			case AbilityTarget.Area:
				break;
			default:
				break;
		}
	}
}
