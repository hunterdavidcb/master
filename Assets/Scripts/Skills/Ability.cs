﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Ability : ISkill, IEqualityComparer<Ability>
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	Skill skill;
	[SerializeField]
	float duration;
	[SerializeField]
	float coolDown;
	[SerializeField]
	uint requiredSkillLevel;
	[SerializeField]
	uint willpowerRequired;
	[SerializeField]
	bool unlocked;
	//[SerializeField]
	[NonSerialized]
	AnimatorOverrideController anim;
	[SerializeField]
	AbilityTarget target;
	//for use with weapons
	[SerializeField]
	private DamageType buffDamageType;
	[SerializeField]
	private uint buffDamageAmt;
	//[SerializeField]
	[NonSerialized]
	GameObject effectPrefab;
	[NonSerialized]
	Sprite icon;

	[SerializeField]
	private bool learned;


	public delegate void AbilityChangeHandler(Ability abilty);
	//the field part is necessary because.....REASONS
	//FU!
	[field: NonSerialized]
	public event AbilityChangeHandler AbilityUnlocked;
	//the field part is necessary because.....REASONS
	//FU!
	[field: NonSerialized]
	public event AbilityChangeHandler AbilityLearned;

	public float Duration
	{
		get { return duration; }
	}

	public float CoolDown
	{
		get { return coolDown; }
	}

	public uint RequiredSkillLevel
	{
		get { return requiredSkillLevel; }
	}

	public uint WillpowerRequired
	{
		get { return willpowerRequired; }
	}

	public bool Unlocked
	{
		get { return unlocked; }
	}

	public bool Learned
	{
		get { return learned; }
	}

	public float CurrentXP
	{
		get { return skill.CurrentXP; }
	}

	public float XPToNextLevel
	{
		get
		{
			return skill.XPToNextLevel;
		}
	}

	public uint CurrentLevel
	{
		get { return skill.CurrentLevel; }
	}

	bool CanLevelUp
	{
		get { return skill.CurrentXP >= skill.XPToNextLevel; }
	}

	public float LevelMultiplier
	{
		get { return skill.LevelMultiplier; }
	}

	public float LevelConst
	{
		get { return skill.LevelConst; }
	}

	public float LevelExponent
	{
		get { return skill.LevelExponent; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}


	public string ID
	{
		get { return id; }
	}

	public Skill Skill
	{
		get { return skill; }
	}

	public AbilityTarget Target
	{
		get { return target; }
	}

	public AnimatorOverrideController Anim
	{
		get { return anim; }
	}

	public GameObject Effect
	{
		get { return effectPrefab; }
	}

	public DamageType DamageType
	{
		get { return buffDamageType; }
	}

	public uint DamageAmount
	{
		get { return buffDamageAmt; }
	}

	public Ability(string name, string description, string id, Skill skill, float dur, float cd, uint rsl, bool ul)
	{
		this.name = name;
		this.description = description;
		this.id = id;
		this.skill = skill;

		this.skill.LeveledUp += OnSkillLeveledUp;

		duration = dur;
		coolDown = cd;
		requiredSkillLevel = rsl;
		unlocked = ul;
	}

	private void OnSkillLeveledUp(Skill skill)
	{
		if (requiredSkillLevel <= skill.CurrentLevel)
		{
			unlocked = true;

			if (AbilityUnlocked != null)
			{
				AbilityUnlocked(this);
			}
		}
	}

	public Ability(AbilityData ad)
	{
		name = ad.Name;
		description = ad.Description;
		id = ad.ID;
		skill = new Skill(ad.SkillData);
		duration = ad.Duration;
		coolDown = ad.CoolDown;
		requiredSkillLevel = ad.RequiredSkillLevel;
		willpowerRequired = ad.WillpowerRequired;

		this.skill.LeveledUp += OnSkillLeveledUp;

		unlocked = requiredSkillLevel <= skill.CurrentLevel;

		if (skill.CurrentLevel == 1)
		{
			learned = true;

			if (AbilityLearned != null)
			{
				AbilityLearned(this);
			}
		}

		anim = ad.Anim;
		effectPrefab = ad.Effect;

		target = ad.Target;
		switch (ad.Target)
		{
			case AbilityTarget.Self:
				break;
			case AbilityTarget.Weapon:
				buffDamageType = ad.DamageType;
				buffDamageAmt = ad.DamageAmount;
				break;
			case AbilityTarget.Enemy:
				break;
			case AbilityTarget.Area:
				break;
			default:
				break;
		}

		duration = ad.Duration;
	}

	public Ability(AbilityData ad, Skill skill)
	{
		name = ad.Name;
		description = ad.Description;
		id = ad.ID;
		this.skill = skill;
		duration = ad.Duration;
		coolDown = ad.CoolDown;
		requiredSkillLevel = ad.RequiredSkillLevel;
		willpowerRequired = ad.WillpowerRequired;

		unlocked =  requiredSkillLevel <= skill.CurrentLevel;

		this.skill.LeveledUp += OnSkillLeveledUp;

		if (skill.CurrentLevel == 1)
		{
			learned = true;

			if (AbilityLearned != null)
			{
				AbilityLearned(this);
			}
		}

		anim = ad.Anim;
		effectPrefab = ad.Effect;

		target = ad.Target;
		switch (ad.Target)
		{
			case AbilityTarget.Self:
				break;
			case AbilityTarget.Weapon:
				buffDamageType = ad.DamageType;
				buffDamageAmt = ad.DamageAmount;
				break;
			case AbilityTarget.Enemy:
				break;
			case AbilityTarget.Area:
				break;
			default:
				break;
		}

		duration = ad.Duration;
	}

	public void LearnAbility()
	{
		if (unlocked)
		{
			learned = true;

			if (AbilityLearned != null)
			{
				AbilityLearned(this);
			}
		}
	}


	public void AddXP(uint amt)
	{
		skill.AddXP(amt);
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeTarget(AbilityTarget at)
	{
		target = at;
	}

	public void ChangeBuffDamageType(DamageType dt)
	{
		buffDamageType = dt;
	}

	public void ChangeBuffDamageAmount(uint amt)
	{
		buffDamageAmt = amt;
	}

	public void ChangeAnimation(AnimatorOverrideController ani)
	{
		anim = ani;
	}

	public void ChangeEffectPrefab(GameObject effect)
	{
		effectPrefab = effect;
	}

	public bool Equals(Ability x, Ability y)
	{
		return x.ID.Equals(y.ID);
	}

	public int GetHashCode(Ability obj)
	{
		return obj.ID.GetHashCode();
	}
}
