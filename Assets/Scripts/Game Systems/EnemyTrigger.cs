﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTrigger : MonoBehaviour
{

    public delegate void SpawnEnemyHandler(Transform t);
    public event SpawnEnemyHandler SpawnEnemy;
	public event SpawnEnemyHandler StopSpawn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			if (SpawnEnemy != null)
			{
                SpawnEnemy(transform);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			if (SpawnEnemy != null)
			{
				StopSpawn(transform);
			}
		}
	}
}
