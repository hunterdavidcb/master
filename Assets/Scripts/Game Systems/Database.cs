﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Database<T> : ScriptableObject, ISerializationCallbackReceiver where T : IIdentity
{
	Dictionary<string, T> pairs = new Dictionary<string, T>();

	[HideInInspector]
	public List<string> keys = new List<string>();
	[HideInInspector]
	public List<T> values = new List<T>();

	public void OnBeforeSerialize()
	{
		keys.Clear();
		values.Clear();
		foreach (KeyValuePair<string, T> pair in pairs)
		{
			keys.Add(pair.Key);
			values.Add(pair.Value);
		}
	}

	public void OnAfterDeserialize()
	{
		pairs.Clear();

		if (keys.Count != values.Count)
			throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable."));

		for (int i = 0; i < keys.Count; i++)
			pairs.Add(keys[i], values[i]);
	}


	public T TryGetValue(string s)
	{
		Debug.Assert(pairs.ContainsKey(s));

		if (pairs.ContainsKey(s))
		{
			return pairs[s];
		}

		return default;
	}

	public void Add(string s, T t)
	{
		pairs.Add(s, t);
	}

	public T this[string s]
	{
		get { return pairs[s]; }
		set { pairs[s] = value; }
	}

	public void Remove(string s)
	{
		keys.Remove(s);
		values.Remove(pairs[s]);
		pairs.Remove(s);
	}

	public bool ContainsKey(string s)
	{
		return pairs.ContainsKey(s);
	}

	public bool ContainsValue(T t)
	{
		return pairs.ContainsValue(t);
	}

	
}
