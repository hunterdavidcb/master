﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class SpawnEnemies : MonoBehaviour
{
	public GameObject[] enemyPrefab;
	ItemDatabase items;
	StatDatabase statDatabase;
	Stat willpower;

	[SerializeField, Tooltip("the maximum number of enemies on screen")]
	int maxNumEnemies;
	[SerializeField, Tooltip("The time between spawn attempts")]
	float spawnInterval;

	List<GameObject> currentEnemies = new List<GameObject>();

	List<Transform> enemyTriggers = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
		items = Resources.Load<ItemDatabase>("Items");
		statDatabase = Resources.Load<StatDatabase>("Stats");
		foreach (var item in statDatabase.keys)
		{

			if (statDatabase[item].Name.Equals("Willpower"))
			{
				willpower = new Stat(statDatabase[item]);
			}


		}

		GameObject[] spawners = GameObject.FindGameObjectsWithTag("EnemySpawner");
		foreach (var spawner in spawners)
		{
			enemyTriggers.Add(spawner.transform);
			spawner.GetComponent<EnemyTrigger>().SpawnEnemy += StartSpawn;
			spawner.GetComponent<EnemyTrigger>().StopSpawn += StopSpawn;
		}

		//InvokeRepeating("Spawn", 0f, spawnInterval);
    }

	private void StopSpawn(Transform t)
	{
		StopCoroutine("Spawn");
	}

	void RemoveEnemy(NPC npc)
	{
		//take off from active list
		currentEnemies.Remove(npc.gameObject);
	}

	IEnumerator Spawn(Transform t)
	{
		int spawnedEnemies = 0;
		while (spawnedEnemies < maxNumEnemies)
		{
			NavMeshHit hit;
			Vector3 pos = Random.insideUnitSphere * 30f + t.position;
			pos.y = 0f;
			if (NavMesh.SamplePosition(pos, out hit, 10f, 1))
			{
				int e = Random.Range(0, enemyPrefab.Length);
				GameObject enemy = Instantiate(enemyPrefab[e], pos, Quaternion.identity);
				currentEnemies.Add(enemy);

				enemy.GetComponent<NPC>().NPCRecovering += RemoveEnemy;

				enemy.GetComponent<NPC>().willpower = willpower;

				for (int ar = 0; ar < 3; ar++)
				{
					Transform place;
					//Debug.Log(ar);
					if (ar == 0)
					{
						place = enemy.GetComponent<NPC>().chestHolder;
					}
					else if (ar == 1)
					{
						place = enemy.GetComponent<NPC>().headHolder;
					}
					else
					{
						place = enemy.GetComponent<NPC>().leftHolder;
					}

					//Debug.Log(place.name);

					int r = UnityEngine.Random.Range(0, 3);
					//Debug.Log(r);
					GameObject go = null;
					string key = "";
					if (r == 0)
					{
						//light
						foreach (var item in items.keys)
						{
							if (items[item].WeakAgainst != DamageType.Shadow)
							{
								continue;
							}
							else
							{
								//Debug.Log("instanciating light");
								go = Instantiate(items[item].Prefab, place);
								key = item;
								break;
							}
						}
					}

					if (r == 1)
					{
						//shadow
						foreach (var item in items.keys)
						{
							if (items[item].WeakAgainst != DamageType.Dark)
							{
								continue;
							}
							else
							{
								//Debug.Log("instanciating shadow");
								go = Instantiate(items[item].Prefab, place);
								key = item;
								break;
							}
						}
					}

					if (r == 2)
					{
						//dark
						foreach (var item in items.keys)
						{
							if (items[item].WeakAgainst != DamageType.Light)
							{
								continue;
							}
							else
							{
								//Debug.Log("instanciating dark");
								go = Instantiate(items[item].Prefab, place);
								key = item;
								break;
							}
						}
					}

					ArmorHolder ah = go.GetComponent<ArmorHolder>();
					ah.SetProtectionAmount(items[key].ProtectionAmount);
					ah.SetStartHP(items[key].HitPoints);
					ah.SetWeakness(items[key].WeakAgainst);

					ah.Damaged += enemy.GetComponent<NPC>().OnReceivedDamage;
				}
			}
			spawnedEnemies++;
			yield return new WaitForSeconds(spawnInterval);
		}
	}

	void StartSpawn(Transform t)
	{
		if (currentEnemies.Count < maxNumEnemies)
		{
			//Debug.Log("getting pos");
			StartCoroutine(Spawn(t));
		}
	}
}
