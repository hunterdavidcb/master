﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveAndLoad
{
	public static void Save(PlayerData pd)
	{
		string path = Application.persistentDataPath + "/Player.dat";

		BinaryFormatter formatter = new BinaryFormatter();
		FileStream stream = new FileStream(path, FileMode.Create);

		formatter.Serialize(stream, pd);

		stream.Close();

		for (int i = 0; i < pd.skills.Count; i++)
		{
			Debug.Log(pd.skills[i].Name);
			Debug.Log(pd.skills[i].CurrentLevel);
			Debug.Log(pd.skills[i].CurrentXP);
			Debug.Log(pd.skills[i].XPToNextLevel);
		}
	}

	public static PlayerData Load()
	{
		string path = Application.persistentDataPath + "/Player.dat";
		if (File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();

			FileStream stream = new FileStream(path, FileMode.Open);

			PlayerData pd = formatter.Deserialize(stream) as PlayerData;

			stream.Close();

			return pd;
		}

		//Debug.Log("null");
		return null;
	}
		
}
