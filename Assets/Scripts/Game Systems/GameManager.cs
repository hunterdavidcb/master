﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[System.Flags]
	public enum GameState
	{
		// Decimal				// Binary
		None = 0,				// 00000000
		MainMenu = 1,			// 00000001
		CharacterCreation = 2,  // 00000010
		Explore = 4,			// 00000100
		Dialogue = 8,		    // 00001000
		Combat = 16,			// 00010000
		Recovery = 32,			// 00100000
		All = 0xFFFFFFF			// 11111111111111111111111111111111
	}

	static private GameManager _gm;
	static private GameState _GAME_STATE = GameState.MainMenu;
	static private bool _PAUSED = false;

	[SerializeField]
	[Tooltip("This private field shows the game state in the Inspector and is set by the "
		+ "GAME_STATE_CHANGE_DELEGATE whenever GAME_STATE changes.")]
	protected GameState _gameState;

	public delegate void CallbackDelegate(); // Set up a generic delegate type.
	static public CallbackDelegate GAME_STATE_CHANGE_DELEGATE;
	static public event CallbackDelegate PAUSE_CHANGE_DELEGATE;

	public int mainMenuScene;
	public int characterCreationScene;
	public int mainLevelScene;
	public int creditsScene;

	[FMODUnity.EventRef]
	public string combat;
	[FMODUnity.EventRef]
	public string explore;
	[FMODUnity.EventRef]
	public string title;
	[FMODUnity.EventRef]
	public string ambience;

	FMOD.Studio.EventInstance ambienceEvent;
	FMOD.Studio.EventInstance combatEvent;
	FMOD.Studio.EventInstance exploreEvent;
	FMOD.Studio.EventInstance titleEvent;

	//these get set by the options
	[HideInInspector]
	public PlayerMovementStyle movementStyle;
	[HideInInspector]
	public PlayerAttackStyle attackStyle;
	[HideInInspector]
	public bool rotateAroundPlayer;
	[HideInInspector]
	public bool canZoom;
	[HideInInspector]
	public bool canControlVerticalAngle;
	[HideInInspector]
	public bool zoomControlsVerticalAngle;
	[HideInInspector]
	public float horizontalRotationSpeed = 90f;
	[HideInInspector]
	public float verticalRotationSpeed = 90f;
	[HideInInspector]
	public float zoomSpeed = 10f;

	PlayerData pd;
	public GameObject playerPrefab;

	[SerializeField,Tooltip("Time in seconds between saves")]
	private float timeBetweenSaves;

	static public GameState GAME_STATE
	{
		get
		{
			return _GAME_STATE;
		}
		set
		{
			if (value != _GAME_STATE)
			{
				_GAME_STATE = value;
				if (GAME_STATE_CHANGE_DELEGATE != null)
				{
					GAME_STATE_CHANGE_DELEGATE();
				}
			}
		}
	}

	protected bool _paused;
	
	internal bool newStart;

	static public bool PAUSED
	{
		get { return _PAUSED; }
		private set
		{
			if (value != _PAUSED)
			{
				_PAUSED = value;
				if (PAUSE_CHANGE_DELEGATE != null)
				{
					PAUSE_CHANGE_DELEGATE();
				}
			}
		}
	}

	static public GameManager Instance
	{
		get
		{
			if (_gm == null)
			{
				Debug.LogError("Anamnesis Sojourn: Game Manager getter - Attempt to get value of S before it has been set.");
				return null;
			}
			return _gm;
		}
	}

	private void Awake()
	{
		_gm = this;
		GAME_STATE_CHANGE_DELEGATE += GameStateChanged;
		PAUSE_CHANGE_DELEGATE += PauseChanged;
		// This strange use of _gameState as an intermediary in the following lines 
		//  is solely to stop the Warning from popping up in the Console telling you 
		//  that _gameState was assigned but not used.
		_gameState = GameState.MainMenu;
		GAME_STATE = _gameState;
		_paused = false;
		PauseGame(_paused);
	}

	public void PauseGame(bool toPaused)
	{
		PAUSED = toPaused;
		if (PAUSED)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	private void GameStateChanged()
	{
		_gameState = GAME_STATE;
		switch (_gameState)
		{
			case GameState.Combat:
				exploreEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
				combatEvent = FMODUnity.RuntimeManager.CreateInstance(combat);
				combatEvent.start();
				break;
			case GameState.Explore:
				combatEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
				exploreEvent = FMODUnity.RuntimeManager.CreateInstance(explore);
				exploreEvent.start();
				titleEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
				break;
		}
	}

	private void PauseChanged()
	{
		this._paused = GameManager.PAUSED;
	}

	public void SetGameOptions(PlayerMovementStyle ms, PlayerAttackStyle pas, bool canRotate,
		bool canZ, bool canPitch, bool canControlPitch, float hRotationSpeed, float vRotationSpeed, float zSpeed)
	{
		movementStyle = ms;
		attackStyle = pas;
		rotateAroundPlayer = canRotate;
		canZoom = canZ;
		canControlVerticalAngle = canPitch;
		zoomControlsVerticalAngle = canControlPitch;
		horizontalRotationSpeed = hRotationSpeed;
		verticalRotationSpeed = vRotationSpeed;
		zoomSpeed = zSpeed;
	}

	private void OnDestroy()
	{
		GAME_STATE = GameManager.GameState.None;
	}

	// Start is called before the first frame update
	void Start()
    {
		SceneManager.sceneLoaded += OnSceneLoaded;
		SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Additive);

		pd = SaveAndLoad.Load();
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		//we are in the main menu
		if (scene.buildIndex == mainMenuScene)
		{
			HandleMainMenu();
		}

		//we are creating a character
		if (scene.buildIndex == characterCreationScene)
		{
			HandleCharacterCreation();
		}

		//we are in the main level
		if (scene.buildIndex == mainLevelScene)
		{
			HandleMainLevel();
		}

		if (scene.buildIndex == creditsScene)
		{
			HandleCredits();
		}
	}

	private void HandleCredits()
	{
		//we just returned from the main level, so unload it
		if (SceneManager.GetSceneByBuildIndex(mainLevelScene).isLoaded)
		{
			//unload the main level
			SceneManager.UnloadSceneAsync(mainLevelScene);
		}
		titleEvent = FMODUnity.RuntimeManager.CreateInstance(title);
		titleEvent.start();
	}

	void HandleMainMenu()
	{
		titleEvent = FMODUnity.RuntimeManager.CreateInstance(title);
		titleEvent.start();

		//we just returned from the main level, so unload it
		if (SceneManager.GetSceneByBuildIndex(mainLevelScene).isLoaded)
		{
			//unload the main level
			SceneManager.UnloadSceneAsync(mainLevelScene);
		}

		//we just returned from character creation, so unload it
		if (SceneManager.GetSceneByBuildIndex(characterCreationScene).isLoaded)
		{
			//unload the character creation scene
			SceneManager.UnloadSceneAsync(characterCreationScene);
		}


		//we just returned from credits, so unload it
		if (SceneManager.GetSceneByBuildIndex(creditsScene).isLoaded)
		{
			//unload the credits
			SceneManager.UnloadSceneAsync(creditsScene);
		}

		//make the main menu the active scene
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(mainMenuScene));

		GAME_STATE = GameState.MainMenu;

		//make sure to unpause everything
		PauseGame(false);

		MainMenu mm = FindObjectOfType<MainMenu>();
		mm.sceneToLoadOnStartNew = mainLevelScene; //change this to characterCreationScene when it is ready
		mm.sceneToLoadOnContinue = mainLevelScene;

		//if player data is not null, we want to allow the player to continue from their game
		mm.EnableContinue(pd != null);


	}

	void HandleCharacterCreation()
	{
		if (SceneManager.GetSceneByBuildIndex(mainMenuScene).isLoaded)
		{
			//unload the main menu
			SceneManager.UnloadSceneAsync(mainMenuScene);
		}

		if (newStart)
		{
			pd = null;
		}

		//if the player data is null, create a new one and set it on the playerobject
		if (pd == null)
		{
			pd = new PlayerData();
		}
	}

	void HandleMainLevel()
	{
		if (SceneManager.GetSceneByBuildIndex(characterCreationScene).isLoaded)
		{
			//unload the character creation
			SceneManager.UnloadSceneAsync(characterCreationScene);
		}

		if (SceneManager.GetSceneByBuildIndex(mainMenuScene).isLoaded)
		{
			//unload the main menu
			SceneManager.UnloadSceneAsync(mainMenuScene);
		}

		GAME_STATE = GameState.Explore;

		if (newStart)
		{
			pd = null;
		}
		else
		{
			//load the settings from the previous time and override the defaults
			canControlVerticalAngle = pd.canPitchCamera;
			canZoom = pd.canZoomCamera;
			rotateAroundPlayer = pd.canRotateCamera;
			zoomControlsVerticalAngle = pd.zoomControlsCameraAngle;
			movementStyle = pd.pms;
			attackStyle = pd.pas;
			zoomSpeed = pd.zoomSpeed;
			verticalRotationSpeed = pd.verticalRotationSpeed;
			horizontalRotationSpeed = pd.horizontalRotationSpeed;
		}

		//spawn the player
		Vector3 location = pd == null ? Vector3.zero : new Vector3(pd.position[0], pd.position[1], pd.position[2]);

		//make the player part of the level scene
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(mainLevelScene));

		GameObject playerObject = Instantiate(playerPrefab, location, Quaternion.identity);
		Player player = playerObject.GetComponent<Player>();

		//if the player data is null, create a new one and set it on the playerobject
		if (pd == null)
		{
			pd = new PlayerData();
		}

		//set the attack style
		PlayerAttack pa = playerObject.GetComponent<PlayerAttack>();
		pa.SetPlayerAttackStyle(attackStyle);
		//set the movement style
		playerObject.GetComponent<PlayerMovement>().SetMovementStyle(movementStyle);

		player.playerData = pd;

		//add the weapons
		//we do this first because the skills are dependent on the weapons being set up
		Debug.Log("Adding weapons");
		WeaponDatabase weaponDatabase = Resources.Load<WeaponDatabase>("Weapons");
		foreach (var item in weaponDatabase.keys)
		{
			Debug.Log(weaponDatabase[item].Name);
			pa.AddWeapon(weaponDatabase[item]);
		}

		//set up skill callbacks
		SkillDatabase skillDatabase = Resources.Load<SkillDatabase>("Skills");

		Dictionary<string, Skill> skills = new Dictionary<string, Skill>();

		if (pd.skills == null || pd.skills.Count == 0)
		{
		//	playerData = new PlayerData();
			foreach (var item in skillDatabase.keys)
			{

		//		//SkillInvoker.AddSkillCommand(isk);
				Skill skill = new Skill(skillDatabase[item]);
				skills.Add(skill.ID, skill);
				ISkillCommand isk = new SkillCommand(skill);
				pa.AddSkillCommand(isk);
				pd.skills.Add(skill);
				//skill.XPIncreased
				//skill.LeveledUp
				
				//		Debug.Log(skill.Name);
				//		Debug.Log(skill.CurrentXP);
				//		Debug.Log(skill.CurrentLevel);
				//		Debug.Log("xp to next level " + skill.XPToNextLevel);
			}
		}
		else
		{
		//	Debug.Log("not null");
			for (int i = 0; i < pd.skills.Count; i++)
			{
				skills.Add(pd.skills[i].ID, pd.skills[i]);
				ISkillCommand isk = new SkillCommand(pd.skills[i]);
				pa.AddSkillCommand(isk);
				//pd.skills[i].XPIncreased
				//pd.skills[i].LeveledUp
				
				//		Debug.Log(playerData.skills[i].Name);
				//		Debug.Log(playerData.skills[i].CurrentXP);
				//		Debug.Log(playerData.skills[i].CurrentLevel);
				//		Debug.Log("xp to next level " + playerData.skills[i].XPToNextLevel);
			}
		}

		//set up ability callbacks
		AbilityDatabase abilityDatabase = Resources.Load<AbilityDatabase>("Abilities");
		
		AbilitySwitch abilitySwitch = FindObjectOfType<AbilitySwitch>();
		LevelUpPanel lup = FindObjectOfType<LevelUpPanel>();

		pa.SetAbilitySwitch(abilitySwitch);
		pa.SetPlayerData(pd);

		if (pd.abilities == null || pd.abilities.Count == 0)
		{
			//	playerData = new PlayerData();
			foreach (var item in abilityDatabase.keys)
			{

				//		//SkillInvoker.AddSkillCommand(isk);
				//we do it this way to make sure we reference the same skill in both cases
				Ability ability = new Ability(abilityDatabase[item],skills[abilityDatabase[item].SkillData.ID]);
				ISkillCommand isk = new AbilityCommand(ability,pa);
				pa.AddAbilityCommand(isk);
				pd.abilities.Add(ability);
				abilitySwitch.AddAbility(ability);
				lup.AddAbility(ability);
				//skill.XPIncreased
				//skill.LeveledUp

				//		Debug.Log(skill.Name);
				//		Debug.Log(skill.CurrentXP);
				//		Debug.Log(skill.CurrentLevel);
				//		Debug.Log("xp to next level " + skill.XPToNextLevel);
			}
		}
		else
		{
			//	Debug.Log("not null");
			for (int i = 0; i < pd.abilities.Count; i++)
			{
				//set the animation from the database, since this is not a system.serializable object
				pd.abilities[i].ChangeAnimation(abilityDatabase[pd.abilities[i].ID].Anim);

				ISkillCommand isk = new AbilityCommand(pd.abilities[i],pa);
				pa.AddAbilityCommand(isk);
				abilitySwitch.AddAbility(pd.abilities[i]);
				lup.AddAbility(pd.abilities[i]);
				//pd.skills[i].XPIncreased
				//pd.skills[i].LeveledUp

				//		Debug.Log(playerData.skills[i].Name);
				//		Debug.Log(playerData.skills[i].CurrentXP);
				//		Debug.Log(playerData.skills[i].CurrentLevel);
				//		Debug.Log("xp to next level " + playerData.skills[i].XPToNextLevel);
			}
		}


		SkillBarHolder sbh = FindObjectOfType<SkillBarHolder>();
		foreach (var skill in pd.skills)
		{
			sbh.AddSkill(skill);
		}

		





		//set up stat callbacks
		Stat willpower = null;
		StatDatabase statDatabase = Resources.Load<StatDatabase>("Stats");
		if (pd.stats == null || pd.stats.Count == 0)
		{
			foreach (var item in statDatabase.keys)
			{

				//		//SkillInvoker.AddSkillCommand(isk);
				Stat stat = new Stat(statDatabase[item]);
				if (stat.Name.Equals("Willpower"))
				{
					willpower = new Stat(statDatabase[item]);
					player.willpower = willpower;
				}

				pd.stats.Add(stat);

			}
		}
		else
		{
			//	Debug.Log("not null");
			for (int i = 0; i < pd.stats.Count; i++)
			{
				if (pd.stats[i].Name.Equals("Willpower"))
				{
					willpower = pd.stats[i];
					player.willpower = willpower;
					break;
				}
			}
		}

		

		//set up camera follow
		FollowPlayer fp = FindObjectOfType<FollowPlayer>();
		fp.SetPlayer(playerObject.transform);
		fp.SetCameraStyle(rotateAroundPlayer, canZoom, canControlVerticalAngle, zoomControlsVerticalAngle,
			horizontalRotationSpeed, verticalRotationSpeed, zoomSpeed);

		//this will prevent some cheesing of triggering attacks while in slow motion
		AbilitySwitch.AbilitySwitchActivated += pa.OnAbilitySwitchActivated;
		AbilitySwitch.AbilitySwitchDeactivated += pa.OnAbilitySwitchDeactivated;

		//set up dialogue interaction
		FindObjectOfType<DialoguePanel>().RegisterPlayer(playerObject.GetComponent<PlayerInteraction>());

		//trigger the tutorial
		//check if this is a new game, or if the player has completed stuff
		if (newStart)
		{

		}


		//for testing only
		// in final version, move this to the enemy spawner
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		ItemDatabase items = Resources.Load<ItemDatabase>("Items");

		Debug.Log(enemies.Length);
		for (int i = 0; i < enemies.Length; i++)
		{
			//setting up the enemy hp
			enemies[i].GetComponent<NPC>().willpower = willpower;

			for (int ar = 0; ar < 3; ar++)
			{
				Transform place;
				//Debug.Log(ar);
				if (ar == 0)
				{
					place = enemies[i].GetComponent<NPC>().chestHolder;
				}
				else if (ar == 1)
				{
					place = enemies[i].GetComponent<NPC>().headHolder;
				}
				else
				{
					place = enemies[i].GetComponent<NPC>().leftHolder;
				}

				Debug.Log(place.name);
				
				int r = UnityEngine.Random.Range(0, 3);
				Debug.Log(r);
				GameObject go = null;
				string key = "";
				if (r == 0)
				{
					//light
					foreach (var item in items.keys)
					{
						if (items[item].WeakAgainst != DamageType.Shadow)
						{
							continue;
						}
						else
						{
							Debug.Log("instanciating light");
							go = Instantiate(items[item].Prefab, place);
							key = item;
							break;
						}
					}
				}

				if (r == 1)
				{
					//shadow
					foreach (var item in items.keys)
					{
						if (items[item].WeakAgainst != DamageType.Dark)
						{
							continue;
						}
						else
						{
							Debug.Log("instanciating shadow");
							go = Instantiate(items[item].Prefab, place);
							key = item;
							break;
						}
					}
				}

				if (r == 2)
				{
					//dark
					foreach (var item in items.keys)
					{
						if (items[item].WeakAgainst != DamageType.Light)
						{
							continue;
						}
						else
						{
							Debug.Log("instanciating dark");
							go = Instantiate(items[item].Prefab, place);
							key = item;
							break;
						}
					}
				}

				ArmorHolder ah = go.GetComponent<ArmorHolder>();
				ah.SetProtectionAmount(items[key].ProtectionAmount);
				ah.SetStartHP(items[key].HitPoints);
				ah.SetWeakness(items[key].WeakAgainst);
			}
			
		}


		//begin saving automatically
		StartCoroutine(SaveOnTimer());
	}

	public void EnterCombat()
	{
		GAME_STATE = GameState.Combat;
	}

	public void ExitCombat()
	{
		GAME_STATE = GameState.Explore;
	}

	public void Save()
	{
		SaveAndLoad.Save(pd);
	}

	IEnumerator SaveOnTimer()
	{
		while (true)
		{
			
			yield return new WaitForSeconds(timeBetweenSaves);
			Debug.Log("saving");

			Debug.Log(pd.position);
			for (int i = 0; i < pd.skills.Count; i++)
			{
				Debug.Log("Skill Name: " + pd.skills[i].Name);
				Debug.Log("Skill Level: " + pd.skills[i].CurrentLevel);
				Debug.Log("Skill XP: " + pd.skills[i].CurrentXP);
				
			}
			SaveAndLoad.Save(pd);
		}
	}

}
