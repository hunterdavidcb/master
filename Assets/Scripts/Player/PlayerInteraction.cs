﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
	GameObject currentSelectedObject;
	[SerializeField]
	KeyCode interactionKeyCode;

	public delegate void DialogueHandler(DialogueContainer dc);
	public event DialogueHandler DialogueTriggered;

	public void RegisterNPC(NPC npc)
	{
		DialogueTriggered += npc.OnDialogueTriggered;
	}

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("here");

		foreach (var mb in other.transform.root.GetComponents<MonoBehaviour>())
		{
			if (mb is IInteractable)
			{
				//Debug.Log("We can interact!");
				currentSelectedObject = mb.gameObject;
			}
		}
		
	}

	private void OnTriggerExit(Collider other)
	{
		foreach (var mb in other.transform.root.GetComponents<MonoBehaviour>())
		{
			if (mb is IInteractable)
			{
				//Debug.Log("Interaction Nazi: No interaction for you!");
				currentSelectedObject = null;
			}
		}
	}

	public void OnDialogueFinished()
	{
		//throw new NotImplementedException();
	}

	private void Update()
	{
		if (Input.GetKeyDown(interactionKeyCode) && currentSelectedObject != null)
		{
			//Debug.Log("Triggering interaction");
			if (DialogueTriggered != null)
			{
				DialogueTriggered(currentSelectedObject.GetComponent<NPC>().container);
			}
			
		}
	}
}
