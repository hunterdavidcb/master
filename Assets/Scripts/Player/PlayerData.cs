﻿using System;
using System.Collections;
using System.Collections.Generic;


[Serializable]
public class PlayerData
{
	public float[] position;
	public List<Skill> skills;
	public List<Stat> stats;
	public List<Ability> abilities;
	public List<Consumable> inventory;
	public bool completedTutorial;
	//add variables for story progress
	
	public bool canRotateCamera;
	public bool canZoomCamera;
	public bool zoomControlsCameraAngle;
	public bool canPitchCamera;
	public float horizontalRotationSpeed;
	public float verticalRotationSpeed;
	public float zoomSpeed;
	public PlayerMovementStyle pms;
	public PlayerAttackStyle pas;

	public PlayerData()
	{
		skills = new List<Skill>();
		stats = new List<Stat>();
		abilities = new List<Ability>();
		inventory = new List<Consumable>();
	}
}
