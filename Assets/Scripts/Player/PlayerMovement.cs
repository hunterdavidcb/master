﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField]
	float moveSpeed;
	[SerializeField]
	float runSpeed;
	[SerializeField]
	float rotationSpeed;
	[SerializeField]
	float interpolationSpeed;
	[SerializeField]
	float dodgeCoolDown;
	[SerializeField]
	float dodgeForce;

	[FMODUnity.EventRef]
	public string footstep;
	[FMODUnity.EventRef]
	public string dodge;


	Vector3 cv;
	Vector3 moveDir;
	CharacterController controller;

	Vector3 forward;
	Vector3 right;


	bool canDodge = true;
	float lastDodgeTime;

	float heldTime = 0f;
	float stopTime = 0f;

	[SerializeField]
	PlayerMovementStyle movementStyle;

	#region Modified from Unity's standard third person controler
	[SerializeField]
	private float groundCheckDistance = 0.1f;
	//[Range(1f, 4f)] [SerializeField] float gravityMultiplier = 2f;

	private bool isGrounded;
	private bool isRunning;
	private Vector3 groundNormal;

	private float origGroundCheckDistance;

	#endregion
	private Animator anim;
	private float turnSmoothTime;
	private Vector3 gravity = Vector3.zero;

	private void Awake()
	{
		//bank.Load();
		//rb = GetComponent<Rigidbody>();
		anim = GetComponentInChildren<Animator>();
		GetComponent<PlayerInteraction>().DialogueTriggered += OnDialogueTriggered;
		origGroundCheckDistance = groundCheckDistance;
		controller = GetComponent<CharacterController>();
	}

	private void OnDialogueTriggered(DialogueContainer dc)
	{
		//throw new NotImplementedException();
	}

	public void SetMovementStyle(PlayerMovementStyle pms)
	{
		movementStyle = pms;
		//Debug.Log(pms);
	}

	// Start is called before the first frame update
	void Start()
	{

	}

	private void Update()
	{
		//putting the dodge code in fixed update will register multiple button down presses per frame!!!
		//WTF Unity
		if (Time.time > lastDodgeTime + dodgeCoolDown && !canDodge)
		{
			//Debug.Log("setting dodge to true");
			canDodge = true;
		}

		if (canDodge)
		{
			//Debug.Log("checking");
			CheckDodge();
		}

		//we keep this here because fixed update does not always detect single button presses per frame
		if (Input.GetButtonDown("Run"))
		{
			isRunning = !isRunning;
		}

		CheckGroundStatus();
	}

	void CheckDodge()
	{
		if (Input.GetButton("Dodge") && Input.GetKey(KeyCode.A))
		{
			Dodge(DodgeDir.Left);
		}
		else if (Input.GetButton("Dodge") && Input.GetKey(KeyCode.W))
		{
			Dodge(DodgeDir.Forward);
		}
		else if (Input.GetButton("Dodge") && Input.GetKey(KeyCode.S))
		{
			Dodge(DodgeDir.Back);
		}
		else if (Input.GetButton("Dodge") && Input.GetKey(KeyCode.D))
		{
			Dodge(DodgeDir.Right);
		}
	}

	void Dodge(DodgeDir dd)
	{
		canDodge = false;
		lastDodgeTime = Time.time;
		forward = Camera.main.transform.forward;
		right = Camera.main.transform.right;

		forward.y = 0f;
		right.y = 0f;

		forward.Normalize();
		right.Normalize();

		FMODUnity.RuntimeManager.PlayOneShot(dodge);

		switch (dd)
		{
			case DodgeDir.Forward:
				//rb.AddForce(forward * dodgeForce);
				StartCoroutine(DodgeTime(forward * dodgeForce));
				break;
			case DodgeDir.Back:
				//rb.AddForce(-forward * dodgeForce);
				StartCoroutine(DodgeTime(-forward * dodgeForce));
				break;
			case DodgeDir.Left:
				//rb.AddForce(-right * dodgeForce);
				StartCoroutine(DodgeTime(-right * dodgeForce));
				break;
			case DodgeDir.Right:
				//Debug.Log("right");
				//rb.AddForce(right * dodgeForce);
				StartCoroutine(DodgeTime(right * dodgeForce));
				break;
			default:
				break;
		}

	}

	IEnumerator DodgeTime(Vector3 dir)
	{
		float d = 0f;
		while (d < 0.5f)
		{
			d += Time.deltaTime;
			controller.Move(dir * Time.deltaTime);
			//transform.position = Vector3.Lerp(orig, dir, d / 0.5f);
			yield return null;
		}
	}

	//keep this in fixed update
	//moving it to update or late update causes camera jittering
	void FixedUpdate()
	{
		HandleCameraRelative();
	}

	
	float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
	{
		return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
	}

	void HandleCameraRelative()
	{

		CheckGroundStatus();


		if (Time.time - lastDodgeTime < dodgeCoolDown)
		{
			//Debug.Log("can't move");
			return;
		}

		float v = Input.GetAxis("Vertical");
		float h = Input.GetAxis("Horizontal");

		Vector3 direction = new Vector3(h, 0f, v).normalized;

		

		if (HandleAnimator(h, v) && !Input.GetButton("Dodge"))
		{
			

			float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

			float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref rotationSpeed, turnSmoothTime);

			transform.rotation = Quaternion.Euler(0f, angle, 0f);

			Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

			if (!isGrounded)
			{
				gravity.y = -2f * 9.81f * Time.deltaTime;
				controller.Move(gravity);
			}

			if (isRunning)
			{
				controller.Move(moveDir.normalized * runSpeed * Time.deltaTime);
			}
			else
			{
				controller.Move(moveDir.normalized * moveSpeed * Time.deltaTime);
			}
			

		}

		//if (HandleAnimator(h, v))
		//{
		//	heldTime += Time.smoothDeltaTime;
		//	stopTime = 0f;
		//	if (heldTime > interpolationSpeed)
		//	{
		//		heldTime = interpolationSpeed;
		//	}

		//	forward = Camera.main.transform.forward;
		//	right = Camera.main.transform.right;

		//	forward.y = 0f;
		//	right.y = 0f;

		//	forward.Normalize();
		//	right.Normalize();

		//	//CheckGroundStatus();

		//	moveDir = h * right + v * forward;

		//	moveDir = Vector3.ProjectOnPlane(moveDir, groundNormal);

		//	//fixes weird angles
		//	//dir.x = 0f;

		//	if (moveDir.sqrMagnitude > 0 && Time.time - lastSPress > doubleTapMaxValue / 2f
		//		&& Time.time - lastAPress > doubleTapMaxValue / 2f
		//		&& Time.time - lastWPress > doubleTapMaxValue / 2f
		//		&& Time.time - lastDPress > doubleTapMaxValue / 2f)
		//	{
		//		Quaternion rot = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, moveDir, rotationSpeed * Mathf.Deg2Rad, 10f));
		//		Vector3 euler = rot.eulerAngles;
		//		euler.x = 0f;
		//		transform.rotation = Quaternion.Euler(euler);
		//	}


		//	if (isRunning)
		//	{
		//		moveDir = moveDir * CosineInterpolate(moveSpeed, runSpeed, heldTime / interpolationSpeed) * Time.smoothDeltaTime;
		//	}
		//	else
		//	{
		//		float speed = CosineInterpolate(0, moveSpeed, heldTime / interpolationSpeed);
		//		anim.SetFloat("Speed", speed / moveSpeed);
		//		moveDir = moveDir * speed * Time.smoothDeltaTime;
		//	}

			

		//	moveDir += transform.position;
		//	rb.MovePosition(moveDir);
		//}
		//else
		//{
		//	heldTime = 0;
		//	stopTime += Time.smoothDeltaTime;
		//	if (stopTime > interpolationSpeed)
		//	{
		//		stopTime = interpolationSpeed;
		//	}
		//	rb.velocity = Vector3.Lerp(rb.velocity,
		//		new Vector3(0f,rb.velocity.y,0f),stopTime/interpolationSpeed);
		//	//rb.velocity = Vector3.Lerp(new Vector3((moveDir.x-transform.position.x)/Time.smoothDeltaTime,
		//	//	rb.velocity.y, (moveDir.z - transform.position.z) / Time.smoothDeltaTime),
		//	//	new Vector3(0f,rb.velocity.y,0f),stopTime/interpolationSpeed);
		//}


		//this gives a slow transition
		//rb.velocity = Vector3.Lerp(rb.velocity, vel, Time.deltaTime * interpolationSpeed);

		//rb.velocity = Vector3.Lerp(rb.velocity, vel, interpolationSpeed);

		//OR
		//transform.position += vel * Time.deltaTime;
	}

	private bool HandleAnimator(float h, float v)
	{

		anim.SetBool("IsMoving", Mathf.Abs(h) > 0f || Mathf.Abs(v) > 0f);
		anim.SetBool("IsRunning", isRunning);
		return Mathf.Abs(h) > 0f || Mathf.Abs(v) > 0f;
	}

	public void Footstep()
	{
		FMODUnity.RuntimeManager.PlayOneShot(footstep);
	}

	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
#if UNITY_EDITOR
		// helper to visualise the ground check ray in the scene view
		Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
#endif
		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
		{
			//Debug.Log("on ground");
			groundNormal = hitInfo.normal;
			isGrounded = true;
			//m_Animator.applyRootMotion = true;
		}
		else
		{
			//Debug.Log("in air");
			isGrounded = false;
			groundNormal = Vector3.up;
			//m_Animator.applyRootMotion = false;
		}

		//rb.useGravity = !isGrounded;
		if (!isGrounded)
		{
			//rb.AddForce(Vector3.down * 30f);
		}
	}

	float CosineInterpolate(float y1, float y2, float mu)
	{
		float mu2;

		mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
		return (y1 * (1 - mu2) + y2 * mu2);
	}

	public enum DodgeDir
	{
		Forward,
		Back,
		Left,
		Right
	}
}
