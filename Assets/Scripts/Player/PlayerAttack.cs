﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerAttack : MonoBehaviour
{
	public Animator weaponAnim;

	public GameObject spear;
	public GameObject mace;
	//public GameObject bow;

	Dictionary<string,ISkillCommand> skillCommands = new Dictionary<string,ISkillCommand>();
	Dictionary<string, Weapon> weapons = new Dictionary<string, Weapon>();
	Dictionary<string, WeaponCollider> weaponColliderDictionary = new Dictionary<string, WeaponCollider>();

	Collider weaponCollider;

	public GameObject arrowPrefab;

	bool isDown;
	bool isDragging;
	bool isAttacking = false;
	bool canAttack = true;
	//float

	Vector2 dragDirection = Vector2.zero;
	Vector2 mouseDownPosition = Vector2.zero;

	int currentWeaponIndex = 0; //spear
	//TODO make sure to set these
	Weapon currentWeapon;
	string currentWeaponName;


	Dictionary<string, ISkillCommand> abilityCommands = new Dictionary<string, ISkillCommand>();
	Ability currentAbility;

	HashSet<Ability> abilityCooldowns = new HashSet<Ability>();

	PlayerAttackStyle attackStyle;
	[SerializeField]
	private float arrowSpeed = 50f;

	[SerializeField] AnimatorOverrideController special;

	[FMODUnity.EventRef]
	public string swing;

	PlayerData playerData;
	Stat willpower;

	public WeaponCollider CurrentWeaponCollider
	{
		get { return weaponColliderDictionary[currentWeaponName]; }
	}
	// Start is called before the first frame update
	void Start()
    {
		currentWeaponName = "Spear";
		weaponCollider = spear.GetComponentInChildren<Collider>();
		mace.SetActive(false);
		//bow.SetActive(false);
		weaponCollider.enabled = false;
		willpower = GetComponent<Player>().willpower;
	}

	public void AddSkillCommand(ISkillCommand isk)
	{
		skillCommands.Add((isk as SkillCommand).Skill.Name, isk);

		switch ((isk as SkillCommand).Skill.Name)
		{
			case "Spear":
				Collider sc = spear.GetComponentInChildren<Collider>();
				

				WeaponCollider wc = sc.GetComponent<WeaponCollider>();

				weaponColliderDictionary.Add("Spear", wc);

				wc.SetSkillCommand(isk);
				wc.SetDamageType(DamageType.Neutral);

				uint dmg = ((isk as SkillCommand).Skill as Skill).CurrentLevel * (uint)weapons["Spear"].BaseAttack;
				((isk as SkillCommand).Skill as Skill).LeveledUp += OnLeveledUp;
				wc.SetDamageAmount(dmg);
				sc.enabled = false;

				break;
			case "Mace":
				Collider mc = mace.GetComponentInChildren<Collider>();

				wc = mc.GetComponent<WeaponCollider>();

				weaponColliderDictionary.Add("Mace", wc);

				wc.SetSkillCommand(isk);
				wc.SetDamageType(DamageType.Neutral);

				dmg = ((isk as SkillCommand).Skill as Skill).CurrentLevel * (uint)weapons["Mace"].BaseAttack;
				((isk as SkillCommand).Skill as Skill).LeveledUp += OnLeveledUp;
				wc.SetDamageAmount(dmg);
				mc.enabled = false;

				break;
			case "Bow":
				Collider bc = mace.GetComponentInChildren<Collider>();

				wc = bc.GetComponent<WeaponCollider>();

				weaponColliderDictionary.Add("Bow", wc);

				wc.SetSkillCommand(isk);
				wc.SetDamageType(DamageType.Neutral);

				dmg = ((isk as SkillCommand).Skill as Skill).CurrentLevel * (uint)weapons["Bow"].BaseAttack;
				((isk as SkillCommand).Skill as Skill).LeveledUp += OnLeveledUp;
				wc.SetDamageAmount(dmg);
				bc.enabled = false;
				break;
			default:
				break;
		}
	}

	public void AddAbilityCommand(ISkillCommand isk)
	{
		AbilityCommand ac = isk as AbilityCommand;
		if (ac != null)
		{
			Debug.Log("adding ability");
			abilityCommands.Add(ac.Ability.Name, isk);
		}
	}

	private void OnLeveledUp(Skill skill)
	{
		uint dmg = skill.CurrentLevel * (uint)weapons[skill.Name].BaseAttack;
		weaponColliderDictionary[skill.Name].SetDamageAmount(dmg);
	}

	public void AddWeapon(Weapon w)
	{
		Debug.Log(w.Name);
		weapons.Add(w.Name, w);
	}

	public void SetPlayerData(PlayerData pd)
	{
		playerData = pd;
		for (int i = 0; i < playerData.stats.Count; i++)
		{
			if (playerData.stats[i].Name.Equals("Willpower"))
			{
				willpower = playerData.stats[i];
				break;
			}
		}
	}

	public void SetPlayerAttackStyle(PlayerAttackStyle pas)
	{
		attackStyle = pas;
		//Debug.Log(attackStyle);
	}

	public void SetAbilitySwitch(AbilitySwitch abilitySwitch)
	{
		abilitySwitch.AbilitySelected += OnAbilitySelected;
	}

	private void OnAbilitySelected(string abilityName)
	{
		currentAbility = (abilityCommands[abilityName] as AbilityCommand).Ability;
		Debug.Log(abilityName);
		Debug.Log(currentAbility);
	}

	// Update is called once per frame
	void Update()
    {
		//Debug.Log(Input.mouseScrollDelta.y);
		if (Mathf.Abs(Input.mouseScrollDelta.y) > 0f)
		{
			//Debug.Log("here");
			SwitchWeapons(Input.mouseScrollDelta.y);
		}

		if (canAttack)
		{
			switch (attackStyle)
			{
				case PlayerAttackStyle.Swipes:
					HandleSwipes();
					break;
				case PlayerAttackStyle.Clicks:
					HandleClicks();
					break;
				default:
					Debug.LogError("We are pacifists!");
					break;
			}
		}
		
		
    }

	private void HandleClicks()
	{
		if (Input.GetAxis("Attack") > 0 && !isAttacking
			&& !EventSystem.current.IsPointerOverGameObject())
		{
			int ran = UnityEngine.Random.Range(0, 3);
			switch (ran)
			{
				case 0:
					weaponAnim.SetBool("Attacking", true);
					weaponAnim.SetTrigger("HorizontalAttack");
					//Debug.Log("setting horizontal");
					isAttacking = true;
					if (weaponCollider != null)
					{
						weaponCollider.enabled = true;
					}
					break;
				case 1:
					weaponAnim.SetBool("Attacking", true);
					weaponAnim.SetTrigger("VerticalAttack");
					//Debug.Log("setting horizontal");
					isAttacking = true;
					if (weaponCollider != null)
					{
						weaponCollider.enabled = true;
					}
					break;
				case 2:
					weaponAnim.SetBool("Attacking", true);
					weaponAnim.SetTrigger("ThrustAttack");
					//Debug.Log("setting horizontal");
					isAttacking = true;
					if (weaponCollider != null)
					{
						weaponCollider.enabled = true;
					}
					break;
				default:
					break;
			}

		}
	}

	void HandleSwipes()
	{
		//we check this first to prevent calculating the drag if it is just a click
		if (Input.GetAxis("Attack") > 0 && isDown)
		{
			isDragging = true;
			return;
		}

		if (Input.GetAxis("Attack") > 0 && !EventSystem.current.IsPointerOverGameObject())
		{
			isDown = true;
			mouseDownPosition = Input.mousePosition;
			//Debug.Log("down");
		}

		if (Input.GetAxis("Attack") == 0 && isDragging && !isAttacking)
		{
			//Debug.Log("up");
			//reset the trackers
			isDown = isDragging = false;

			//we also need to set up code to handle the right analog stick here
			//uncomment this once we have an animation
			dragDirection = mouseDownPosition - (Vector2)Input.mousePosition;
			if (dragDirection.magnitude == 0)
			{
				//we are not using the mouse
				//check the camera axes instead
				//Debug.Log("using the controller");
				dragDirection = new Vector2(Input.GetAxis("Camera Horizontal"), Input.GetAxis("Camera Vertical"));
				//Debug.Log(dragDirection);
			}

			FMODUnity.RuntimeManager.PlayOneShot(swing);

			if (Mathf.Abs(dragDirection.x) > Mathf.Abs(dragDirection.y))
			{
				weaponAnim.SetBool("Attacking", true);
				weaponAnim.SetTrigger("HorizontalAttack");
				//Debug.Log("setting horizontal");
				isAttacking = true;
				if (weaponCollider != null)
				{
					weaponCollider.enabled = true;
				}
			}
			else if (dragDirection.y > 0)
			{
				weaponAnim.SetBool("Attacking", true);
				weaponAnim.SetTrigger("VerticalAttack");
				//Debug.Log("setting vertical");
				isAttacking = true;
				if (weaponCollider != null)
				{
					weaponCollider.enabled = true;
				}
			}
			else
			{
				weaponAnim.SetBool("Attacking", true);
				weaponAnim.SetTrigger("ThrustAttack");
				//Debug.Log("setting thrust");
				if (weaponCollider != null)
				{
					weaponCollider.enabled = true;
				}
			}
			
		}


		if (currentAbility != null)
		{
			if (Input.GetButtonDown("Special Ability") && !abilityCooldowns.Contains(currentAbility)
			&& currentAbility.WillpowerRequired < willpower.Current)
			{
				//Debug.Log("special ability");
				abilityCommands[currentAbility.Name].Execute();
				weaponAnim.runtimeAnimatorController = special;
				weaponAnim.SetTrigger("Special");

				abilityCooldowns.Add(currentAbility);

				StartCoroutine(RemoveCooldown(currentAbility));
			}
		}
		
	}

	private void HandleBow()
	{
		Debug.Log("yo");
		//GameObject arrow = Instantiate(arrowPrefab, bow.transform.position + Vector3.up, transform.rotation);
		//arrow.GetComponent<Rigidbody>().velocity = transform.forward * arrowSpeed;
	}


	private IEnumerator RemoveCooldown(Ability ability)
	{
		yield return new WaitForSeconds(ability.Duration);

		abilityCooldowns.Remove(ability);
	}

	private void SwitchWeapons(float y)
	{
		currentWeaponIndex = y < 0 ? currentWeaponIndex + 1 : currentWeaponIndex - 1;

		if (currentWeaponIndex > 2)
		{
			currentWeaponIndex = 0;
		}

		if (currentWeaponIndex < 0)
		{
			currentWeaponIndex = 2;
		}

		ActivateWeapon();
	}

	void ActivateWeapon()
	{
		//Debug.Log("switching weapons");
		Debug.Log(currentWeaponIndex);
		switch (currentWeaponIndex)
		{
			case 0:
				mace.SetActive(false);
				//bow.SetActive(false);
				spear.SetActive(true);
				weaponCollider = spear.GetComponentInChildren<Collider>();
				break;
			case 1:
				mace.SetActive(false);
				//bow.SetActive(true);
				weaponCollider = null; //there is no collider for the bow
				spear.SetActive(false);
				break;
			case 2:
				mace.SetActive(true);
				weaponCollider = mace.GetComponentInChildren<Collider>();
				//bow.SetActive(false);
				spear.SetActive(false);
				break;
			default:
				break;
		}

		weaponAnim.SetInteger("Weapon", currentWeaponIndex);
	}

	//this is used by the animation events in the attack animations.
	//We can use it to give each weapon a different timing without worrying about the different times in code.
	public void FinishAttack()
	{
		//Debug.Log("Finished");
		isAttacking = false;
		weaponAnim.SetBool("Attacking", false);
		if (weaponCollider != null)
		{
			weaponCollider.enabled = false;
		}
	}

	internal void OnAbilitySwitchActivated()
	{
		canAttack = false;
	}

	internal void OnAbilitySwitchDeactivated()
	{
		canAttack = true;
	}
}
