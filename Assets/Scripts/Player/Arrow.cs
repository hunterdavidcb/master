﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField]
    private float arrowLifeTime = 5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        arrowLifeTime -= Time.deltaTime;
		if (arrowLifeTime <= 0f)
		{
            Destroy(gameObject);
		}
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("NPC") || other.CompareTag("ArmorHolder"))
		{
            //Debug.Log("hit " + other.gameObject.name);
            Destroy(gameObject);
		}
	}
}
