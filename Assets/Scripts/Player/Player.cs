﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerMovement)), RequireComponent(typeof(PlayerInteraction))]
public class Player : MonoBehaviour, IDamagable
{
	public PlayerData playerData;// = null;

	//int currentSkill = -1;

	//List<ISkillCommand> commands = new List<ISkillCommand>();

	public GameObject cape;

	Cloth capeCloth;

	public Stat willpower;

	float max = 222.3f;
	float min = 0f;

	public DamageType WeakTo
	{
		get { return DamageType.Neutral; }
	}

	public void ReceiveDamage(float amt, DamageType dt, bool knockBack, Vector3 dir)
	{

		willpower.SetCurrentValue(willpower.Current - amt);

		//Debug.Log(willpower.Current);

		float ratio = willpower.Current / willpower.Base;
		//Debug.Log(ratio);
		ratio *= max;

		//Debug.Log(ratio);
		capeCloth.enabled = false;
		cape.transform.localScale = new Vector3(cape.transform.localScale.x, cape.transform.localScale.y, ratio);
		capeCloth.enabled = true;

	}
	
	void Awake()
	{
		capeCloth = cape.GetComponent<Cloth>();
	}


    void Update()
    {
		if (transform.hasChanged)
		{
			//Debug.Log("updating position");
			playerData.position = new float[3] { transform.position.x, transform.position.y, transform.position.z };

			//float ratio = willpower.Current / willpower.Base;
			////Debug.Log(ratio);
			//ratio *= max;
			//ratio = max - ratio;
			//Debug.Log(ratio);

			//ratio = ratio + transform.root.position.y;
			//capeMaterial.SetFloat("_DissolveY", ratio);
			//capeMaterial.SetFloat("_DissolveStartY", transform.root.position.y);
		}
		
		//if (Input.GetKeyDown(KeyCode.Alpha1))
		//{
		//	currentSkill = 0;
		//	Debug.Log(playerData.skills[currentSkill].Name);
		//}

		//if (Input.GetKeyDown(KeyCode.Alpha2))
		//{
		//	currentSkill = 1;
		//	Debug.Log(playerData.skills[currentSkill].Name);
		//}

		//if (Input.GetKeyDown(KeyCode.Alpha3))
		//{
		//	currentSkill = 3;
		//	Debug.Log(playerData.skills[currentSkill].Name);
		//}

		
		//if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
		//{
		//	//playerData.skills[currentSkill].
		//	//commands[currentSkill].Execute();
		//}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			ReceiveDamage(10, DamageType.Neutral, false, Vector3.zero);
		}	
	}


}
