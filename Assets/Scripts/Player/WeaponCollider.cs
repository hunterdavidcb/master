﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollider : MonoBehaviour
{
	ISkillCommand isk;
	private DamageType baseDamageType;
	private uint baseDamageAmt;

	private DamageType buffDamageType;
	private uint buffDamageAmt;
	private float buffDuration;
	private Skill skill;

	public float stopTime;
	public float slowTime;
	public float slowTimeRatio;
	private bool stopping = false;

	[FMODUnity.EventRef]
	public string armorHit;
	[FMODUnity.EventRef]
	public string astralHit;


	public void SetSkillCommand(ISkillCommand skill)
	{
		isk = skill;
	}

	public void SetDamageType(DamageType dt)
	{
		baseDamageType = dt;
		Debug.Log("damage type is " + dt.ToString());
	}

	public void SetDamageAmount(uint amt)
	{
		baseDamageAmt = amt;
		Debug.Log("damage amount is " + amt.ToString());
	}

	public void AddBuff(DamageType dt, uint amt, float duration, Skill s)
	{
		Debug.Log("adding buff");
		buffDamageType = dt;
		buffDamageAmt = amt;
		skill = s;
		StartCoroutine(RemoveBuff(duration));
	}

	IEnumerator RemoveBuff(float dur)
	{
		yield return new WaitForSeconds(dur);

		buffDamageAmt = 0;
		buffDamageType = DamageType.Neutral;
		Debug.Log("removing buff");
	}

	private void OnTriggerEnter(Collider collider)
	{
		if (collider.CompareTag("ArmorHolder") || collider.CompareTag("Body"))
		{
			//Debug.Log("from weapon");
			isk.Execute();

			if (skill != null)
			{
				skill.AddXP(10);
			}
			

			if (collider.CompareTag("ArmorHolder"))
			{
				FMODUnity.RuntimeManager.PlayOneShot(armorHit);
				collider.GetComponent<ArmorHolder>().ReceiveDamage(baseDamageAmt, baseDamageType, false, Vector3.zero);
				GetComponent<Collider>().enabled = false;
			}
			else
			{
				//Debug.Log(collider.CompareTag("Body"));
				//Debug.Log(collider.name);
				FMODUnity.RuntimeManager.PlayOneShot(astralHit);
				collider.GetComponentInParent<NPC>().ReceiveDamage(baseDamageAmt, baseDamageType, false, Vector3.zero);
				GetComponent<Collider>().enabled = false;
			}
			//try to get the root
			//Vector3 dir = transform.root.position - collider.transform.root.position;
			//dir.Normalize();
			//transform.root.GetComponent<Rigidbody>().AddForce(dir * 200f);

			if (!stopping)
			{
				Time.timeScale = 0f;
				stopping = true;
				StartCoroutine(HitStop());
			}
			
		}
	}

	// adapted from https://www.youtube.com/watch?v=ChOtkGLIGyU
	private IEnumerator HitStop()
	{
		yield return new WaitForSecondsRealtime(stopTime);

		Time.timeScale = slowTimeRatio;
		yield return new WaitForSecondsRealtime(slowTime);

		Time.timeScale = 1f;
		stopping = false;
	}
}
