﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItem : IIdentity
{
   // float Weight { get; }
	List<StatusEffect> Effects { get; }
	Sprite Icon { get; }
	GameObject Prefab { get; }
	//void ChangeWeight(float w);
}

public interface IEquipable : IItem
{
	EquipmentSlot Slot { get; }
	bool Equipped { get; }
	void Equip();
}

public enum EquipmentSlot
{
	Head,
	Chest,
	Legs,
	WeaponHand,
	Accessory
}
