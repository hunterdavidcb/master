﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable 
{
	void ReceiveDamage(float amt, DamageType dt, bool knockBack, Vector3 dir);
	DamageType WeakTo { get; }
}

public enum DamageType
{
	Neutral,
	Light,
	Dark,
	Shadow
}
