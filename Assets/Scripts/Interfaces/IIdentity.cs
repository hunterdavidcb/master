﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IIdentity
{
    string Name { get; }
	string Description { get; }
	string ID { get; }
	void ChangeName(string n);
	void ChangeDescription(string d);
}
