﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkill : IIdentity
{
    
	float LevelMultiplier { get; }
	float LevelConst { get; }
	float LevelExponent { get; }
	void AddXP(uint amt);

}
