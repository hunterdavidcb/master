﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStat : IIdentity
{
    float Base { get; }
	List<SkillData> Skills { get; }
	void AddSkill(SkillData skill);
	void RemoveSkill(SkillData skill);
	void SetBase(float b);
}
