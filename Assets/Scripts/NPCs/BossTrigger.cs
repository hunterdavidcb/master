﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    public Animator bossAnim;
    public BossAnimHook bossAnimHook;

    new Transform camera;

    Vector3 start = new Vector3(21.93f, -8.7f, 2326.53f);
    Vector3 startRotation = new Vector3(-18f, -15.7f, 0f);
	
    [SerializeField]
    private float transitionTime;

	// Start is called before the first frame update
	void Start()
    {
        camera = Camera.main.transform;
        bossAnimHook.Landed += OnLand;
    }

	private void OnLand()
	{
        bossAnim.SetTrigger("Walk");
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
            //disable player movement

            //disable player attack

            //disable ability switch

            //disable inventory/level up

            StartCoroutine(MoveCameraToStart());
		}
	}


    IEnumerator MoveCameraToStart()
	{
        Vector3 originalPos = camera.position;
        Quaternion originalRot = camera.rotation;
        Quaternion targetRot = Quaternion.Euler(startRotation);

        float t = 0f;
		while (t < transitionTime)
		{
            t += Time.deltaTime;
            camera.position = Vector3.Lerp(originalPos, start, t / transitionTime);
            camera.rotation = Quaternion.Slerp(originalRot,targetRot, t);
            yield return null;
        }

        bossAnim.SetTrigger("Fly");
        
	}

}
