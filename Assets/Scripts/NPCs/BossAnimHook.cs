﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimHook : MonoBehaviour
{
	public delegate void LandHandler();
	public event LandHandler Landed;
    public void Land()
	{
		if (Landed != null)
		{
			Landed();
		}
	}

	public void Credits()
	{

	}
}
