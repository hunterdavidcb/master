﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour, IInteractable, IDamagable, IIdentity
{
	public DialogueContainer container;
	public Transform headHolder;
	public Transform leftHolder;
	public Transform chestHolder;
	public Animator anim;

	public Stat willpower;

	private string id;
	private string description;

	[SerializeField]
	float timeBetweenAttacks;

	public DamageType WeakTo
	{
		get { return DamageType.Neutral; }
	}

	public float Speed
	{
		get { return agent.speed; }
		set { agent.speed = value; }
	}

	public float OriginalSpeed
	{
		get { return originalSpeed; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}

	public string ID
	{
		get { return id; }
	}

	float originalSpeed;

	[SerializeField]
	float wanderDisance;

	NavMeshAgent agent;
	EnemyState enemyState;
	bool attacking;
	Transform targetObject;
	Vector3 targetPosition;
	Rigidbody rb;

	public delegate void NPCStateHandler(NPC npc);
	public event NPCStateHandler NPCRecovering;

	public void ReceiveDamage(float amt, DamageType dt, bool knockBack, Vector3 dir)
	{
		
		willpower.SetCurrentValue(willpower.Current -(int)amt);


		Debug.Log("receiving damage");
		//pass amt on to the body
		//if (Damaged != null)
		//{
		//	Damaged((uint)amt);
		//}


		//Debug.Log(willpower.Current);

		if (willpower.Current <= 0)
		{
			//rb.isKinematic = false;
			//rb.useGravity = true;
			//rb.AddForce((transform.position - transform.parent.parent.position).normalized * 20f);
			//GetComponent<Collider>().isTrigger = false;
			//rb.isKinematic = false;
			//rb.AddForce(Vector3.up * 100);
			transform.position = new Vector3(transform.position.x, transform.position.y + 10 , transform.position.z);
			agent.enabled = false;
			enemyState = EnemyState.Recover;
			if (NPCRecovering != null)
			{
				Debug.Log("now recovering");
				NPCRecovering(this);
			}
		}
	}

	public void OnReceivedDamage(uint amt)
	{
		ReceiveDamage(amt, DamageType.Neutral, false, Vector3.zero);
	}

	void Awake()
	{
		agent = GetComponent<NavMeshAgent>();

		enemyState = EnemyState.Wander;

		rb = GetComponent<Rigidbody>();

		originalSpeed = agent.speed;

		//uncomment this after testing is done
		StartWander();

		id = Guid.NewGuid().ToString();

	}

	public void OnDialogueTriggered(DialogueContainer dc)
	{

	}

	public void OnDialogueFinished()
	{
		//throw new NotImplementedException();
	}


	// Update is called once per frame
	void Update()
    {
		//we always check this because agents might be enabled or disabled by game events, for example
		// entering recover
		if (agent !=null && agent.isActiveAndEnabled && agent.hasPath)
		{
			if (agent.remainingDistance <= agent.stoppingDistance + 0.5f)
			{
				if (enemyState == EnemyState.Attack && !attacking)
				{
					attacking = true;
					StartCoroutine(Attack());
				}
				else if (enemyState == EnemyState.Wander)
				{
					StopAllCoroutines();
					Invoke("StartWander", 5f);
				}
			}
			
		}
    }

	IEnumerator Attack()
	{

		while (true)
		{
			float timer = UnityEngine.Random.Range(timeBetweenAttacks - 2f, timeBetweenAttacks + 2f);
			yield return new WaitForSeconds(timer);

			bool isSlap = UnityEngine.Random.Range(0f, 1f) > 0.5f;

			if (isSlap)
			{
				anim.SetTrigger("Slap");
			}
			else
			{
				anim.SetTrigger("Flick");
			}
			


		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.CompareTag("Player"))
		{
			enemyState = EnemyState.Attack;
			targetObject = collider.transform;
			agent.SetDestination(collider.transform.position);
			GameManager.GAME_STATE = GameManager.GameState.Combat;
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.CompareTag("Player"))
		{
			enemyState = EnemyState.Wander;
			targetObject = null;
			GameManager.GAME_STATE = GameManager.GameState.Explore;
			StartWander();
		}
	}

	void StartWander()
	{
		if (agent != null && agent.enabled)
		{
			targetPosition = transform.position + UnityEngine.Random.insideUnitSphere * wanderDisance;
			//targetPosition.y = 0f;

			NavMeshHit hit;
			while (!NavMesh.SamplePosition(targetPosition, out hit, 10f, 1))
			{
				targetPosition = transform.position + UnityEngine.Random.insideUnitSphere * wanderDisance;
				targetPosition.y = 0f;
			}

			agent.SetDestination(targetPosition);
		}
		
	}

	public void Interact()
	{
		//throw new System.NotImplementedException();
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}
}

public enum EnemyState
{
	Wander,
	Talk,
	Attack,
	Recover
}
