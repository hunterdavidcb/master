﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpiritComponent : MonoBehaviour
{
    // Start is called before the first frame update
    public abstract void ProcessNPC(NPC npc);
    public abstract void UnprocessNPC(NPC npc);
}
