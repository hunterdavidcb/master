﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowComponent : SpiritComponent
{

	public float speed;


    public override void ProcessNPC(NPC npc)
	{
		npc.Speed = npc.OriginalSpeed * speed;
	}

	public override void UnprocessNPC(NPC npc)
	{
		npc.Speed = npc.OriginalSpeed;
	}
}
