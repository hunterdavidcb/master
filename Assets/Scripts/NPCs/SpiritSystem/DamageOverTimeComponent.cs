﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTimeComponent : SpiritComponent
{

	public uint damage;
	public DamageType damageType;
	public float timeBetween;

	Dictionary<string, NPC> npcDictionary = new Dictionary<string, NPC>();
	Dictionary<string, Coroutine> coroutineDictionary = new Dictionary<string, Coroutine>();

    public override void ProcessNPC(NPC npc)
	{
		Coroutine cr = StartCoroutine(DealDamage(npc));
		npcDictionary.Add(npc.ID, npc);
		coroutineDictionary.Add(npc.ID, cr);

		npc.NPCRecovering += OnNPCRecovering;
	}

	private void OnNPCRecovering(NPC npc)
	{
		if (npcDictionary.ContainsKey(npc.ID))
		{
			npcDictionary.Remove(npc.ID);
		}
		
		if (coroutineDictionary.ContainsKey(npc.ID))
		{
			StopCoroutine(coroutineDictionary[npc.ID]);
			coroutineDictionary.Remove(npc.ID);
		}
		
	}

	public override void UnprocessNPC(NPC npc)
	{
		OnNPCRecovering(npc);
	}

	IEnumerator DealDamage(NPC npc)
	{
		while (true)
		{
			npc.ReceiveDamage(damage, damageType, false, Vector3.zero);
			yield return new WaitForSeconds(timeBetween);
		}
		
	}


}
