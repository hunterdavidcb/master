﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Spirit : MonoBehaviour
{
	public uint damage;
	public DamageType damageType;

	public float timeBetweenAttacks;

	public float currentLifetime; //in seconds

	NPC currentTarget;

	Dictionary<string, NPC> npcs = new Dictionary<string, NPC>();

	List<SpiritComponent> spiritComponents = new List<SpiritComponent>();

	NavMeshAgent agent;
	bool startedAttack = false;

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("entered");
		NPC npc = other.GetComponentInParent<NPC>();

		if (npc != null)
		{
			
			if (!npcs.ContainsKey(npc.ID))
			{
				Debug.Log("npc entered");
				npcs.Add(npc.ID, npc);
				npc.NPCRecovering += OnNPCRecovery;

				for (int i = 0; i < spiritComponents.Count; i++)
				{
					spiritComponents[i].ProcessNPC(npc);
				}

				if (currentTarget == null)
				{
					currentTarget = npc;
					agent.SetDestination(npc.transform.position);
				}
			}
		}
		
	}

	private void OnNPCRecovery(NPC npc)
	{
		npcs.Remove(npc.ID);
		for (int i = 0; i < spiritComponents.Count; i++)
		{
			spiritComponents[i].UnprocessNPC(npc);
		}

		if (npc == currentTarget)
		{
			StopAllCoroutines();
			if (npcs.Count > 0)
			{
				int t = Random.Range(0, npcs.Keys.Count);
				string key = npcs.Keys.ToList()[t];
				currentTarget = npcs[key];
				agent.SetDestination(npc.transform.position);
			}
			else
			{
				currentTarget = null;
				
			}
			
		}
	}

	private void OnTriggerExit(Collider other)
	{
		//Debug.Log("exited");
		NPC npc = other.GetComponentInParent<NPC>();

		if (npc != null)
		{
			Debug.Log("npc exited");
			npcs.Remove(npc.ID);
			npc.NPCRecovering -= OnNPCRecovery;
			for (int i = 0; i < spiritComponents.Count; i++)
			{
				spiritComponents[i].UnprocessNPC(npc);
			}
		}
	}

	private void Awake()
	{
		GetComponents<SpiritComponent>(spiritComponents);
		agent = GetComponent<NavMeshAgent>();
	}

	private void Update()
	{
		currentLifetime -= Time.deltaTime;

		if (currentLifetime <= 0f)
		{
			//we need to return the npcs to their normal state
			foreach (var npc in npcs.Values)
			{
				for (int i = 0; i < spiritComponents.Count; i++)
				{
					spiritComponents[i].UnprocessNPC(npc);
				}

				//we also don't care about their events any more
				npc.NPCRecovering -= OnNPCRecovery;
			}

			//free up some memory
			npcs.Clear();
			Destroy(gameObject);
		}


		if (agent.hasPath && agent.remainingDistance < 0.5f && !startedAttack)
		{
			StartCoroutine(DamageTarget());
		}

		if (currentTarget != null)
		{
			agent.SetDestination(currentTarget.transform.position);
		}
		
	}

	IEnumerator DamageTarget()
	{
		startedAttack = true;

		while (true)
		{
			if (currentTarget != null)
			{
				currentTarget.ReceiveDamage(damage, damageType, false, Vector3.zero);
				yield return new WaitForSeconds(timeBetweenAttacks);
			}
			else if (npcs.Count > 0)
			{
				List<string> list = npcs.Keys.ToList();
				string key = list[Random.Range(0, list.Count)];
				currentTarget = npcs[key];
			}
			else
			{
				StopAllCoroutines();
			}

		}
		
	}
}
