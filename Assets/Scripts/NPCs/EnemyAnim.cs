﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnim : MonoBehaviour
{
	[FMODUnity.EventRef]
	public string footstep;
    public void Footstep()
	{
		FMODUnity.RuntimeManager.PlayOneShot(footstep);
	}

	public void ActivateTriggers()
	{

	}

	public void DeactivateTriggers()
	{

	}
}
