﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NodeLinkData
{
	public string baseNodeID;
	public string portName;
	public string targetNodeID;
}
