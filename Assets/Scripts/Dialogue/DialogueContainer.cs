﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DialogueContainer : ScriptableObject
{
	public List<DialogueNodeData> dialogueNodeData = new List<DialogueNodeData>();
	public List<NodeLinkData> nodeLinks = new List<NodeLinkData>();
}
