﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPanel : MonoBehaviour
{
    public GameObject levelUpPanel;
    Dictionary<string, Ability> abilities = new Dictionary<string, Ability>();

    Dictionary<string, Button> abilityButtons = new Dictionary<string, Button>();

    public List<Button> buttons;

	public Button switchButton;

	private void Awake()
	{
        levelUpPanel.SetActive(false);
		foreach (var button in buttons)
		{
            //register the buttons with the tooltip
            AbilityDataHolder adh = button.GetComponent<AbilityDataHolder>();
            ShowTooltip.Register(adh);

            abilityButtons.Add(adh.abilityData.ID, button);
            //activate/deactivate the buttons based on unlocked or not
		}
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.I))
		{
            levelUpPanel.SetActive(!levelUpPanel.activeSelf);
			if (levelUpPanel.activeSelf)
			{
				TakeCareOfAbilityActivation();
			}
		}
    }

    void TakeCareOfAbilityActivation()
	{
		foreach (var button in abilityButtons)
		{
			bool maxedOut = false;
			int count = 0;
			foreach (var ability in abilities)
			{
				if (ability.Value.Learned && ability.Value.RequiredSkillLevel == abilities[button.Key].RequiredSkillLevel
					&& ability.Value.Skill.Equals(abilities[button.Key].Skill))
				{
					count++;
				}
			}

			maxedOut = count == abilities[button.Key].Skill.AbilitiesPerTier;

			if (abilities[button.Key].Unlocked && abilities[button.Key].Learned)
			{
				button.Value.enabled = false;
			}
			else if (abilities[button.Key].Unlocked && !abilities[button.Key].Learned && !maxedOut)
			{
				button.Value.interactable = true;
			}
			else
			{
				button.Value.interactable = false;
			}
		}
	}

    public void AddAbility(Ability ability)
	{
        abilities.Add(ability.ID, ability);
	}

	public void LearnAbility(string id)
	{

	}
}
