﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DissolveEffect : MonoBehaviour
{
	Material mat;
	Shader shader;
    // Start is called before the first frame update
    void Start()
    {
		mat = GetComponent<Image>().material;
		//shader = mat.shader;
		StartCoroutine(DissolveIn());
    }

	IEnumerator DissolveIn()
	{
		float t = 1f;
		while (t > 0)
		{
			t -= Time.deltaTime;
			mat.SetFloat("_Level", t);
			yield return null;
		}
		//while (shader.)
		//{

		//}
	}
}
