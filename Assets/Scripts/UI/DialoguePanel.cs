﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour
{
	public GameObject dialoguePanel;
	public Text dialogueDisplayText;
	public Transform choiceHolder;
	public GameObject choicePrefab;

	List<GameObject> currentChoices = new List<GameObject>();

	DialogueContainer currentDialogue;

	public delegate void DialogueFinishedHandler();
	public event DialogueFinishedHandler DialogueFinished;

	private void Awake()
	{
		dialoguePanel.SetActive(false);
	}

	public void RegisterPlayer(PlayerInteraction pi)
	{
		DialogueFinished += pi.OnDialogueFinished;
		pi.DialogueTriggered += OnDialogueTriggered;
	}

	public void RegisterNPC(NPC npc)
	{
		//npc.DialogueTriggered += OnDialogueTriggered;
		DialogueFinished += npc.OnDialogueFinished;
	}

	private void OnDialogueTriggered(DialogueContainer dc)
	{
		GameManager.GAME_STATE = GameManager.GameState.Dialogue;
		dialoguePanel.SetActive(true);
		currentDialogue = dc;
		ClearChoices();

		dialogueDisplayText.text = "";

		dialogueDisplayText.text = dc.dialogueNodeData[0].text;

		CreateChoices(0);
		//dc.nodeLinks.First(x => x.baseNodeID == dc.)
		//throw new NotImplementedException();
	}

	private void CreateChoices(int v)
	{
		var choiceList = currentDialogue.nodeLinks.Where(x => x.baseNodeID == currentDialogue.dialogueNodeData[v].id);
		if (choiceList.Any())
		{
			foreach (var choice in choiceList)
			{
				GameObject c = Instantiate(choicePrefab, choiceHolder);
				c.GetComponentInChildren<Text>().text = choice.portName;
				currentChoices.Add(c);
				//set up callback
				//separate variable just to be safe
				string id = choice.targetNodeID;
				c.GetComponent<Button>().onClick.AddListener(() => ButtonPressed(id));

			}
		}
		else
		{
			//end option
			//close
			GameObject c = Instantiate(choicePrefab, choiceHolder);
			c.GetComponentInChildren<Text>().text = "Exit";
			c.GetComponent<Button>().onClick.AddListener(() => ButtonPressed("Exit"));
			currentChoices.Add(c);

		}
	
	}

	void ButtonPressed(string id)
	{
		ClearChoices();

		if (id != "Exit")
		{
			var next = currentDialogue.dialogueNodeData.First(x => x.id == id);

			if (next != null)
			{
				dialogueDisplayText.text += "\n" + next.text;
				CreateChoices(currentDialogue.dialogueNodeData.IndexOf(next));
			}
		}
		else
		{
			dialoguePanel.SetActive(false);
			
			//send messages to enemies
			if (DialogueFinished != null)
			{
				DialogueFinished();
			}
			
			GameManager.GAME_STATE = GameManager.GameState.Explore;
		}

		
	}

	private void ClearChoices()
	{
		//going backwards to be careful and not skip anything
		//or cause errors
		for (int i = currentChoices.Count-1; i > -1; i--)
		{
			Destroy(currentChoices[i]);
		}

		currentChoices.Clear();
	}

}
