﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillBar : MonoBehaviour
{
    public Text skillName;
    public Text skillLevel;
    public Text xpToNextLevel;
    public Image currentAmount;
}
