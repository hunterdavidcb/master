﻿using UnityEngine;
using UnityEngine.UI;

public class ShowTooltip : MonoBehaviour
{
	static GameObject tooltip;
	// Start is called before the first frame update
	void Start()
	{
		tooltip = transform.GetChild(0).gameObject;
		tooltip.SetActive(false);

		//sign up for events: interation, shopping items

		//diabled for testing
		//FindObjectOfType<Interaction>().Showtooltip += OnShowTooltip;
		//FindObjectOfType<Interaction>().HideTooltip += OnHideTooltip;
	}

	// Update is called once per frame
	void Update()
	{

	}

	public static void Register(AbilityDataHolder adh)
	{
		adh.Entered += OnShowTooltip;
		adh.Exited += OnHideTooltip;
	}

	private static void OnHideTooltip(Vector2 pos, string message)
	{
		tooltip.SetActive(false);
	}

	private static void OnShowTooltip(Vector2 pos, string message)
	{
		tooltip.SetActive(true);
		tooltip.GetComponent<RectTransform>().anchoredPosition = pos;
		tooltip.GetComponentInChildren<Text>().text = message;
	}
}