﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
	[SerializeField]
	int mainMenuScene;
	public GameObject pausePanel;

	public List<Button> buttons;

	int currentButton;
	private void Awake()
	{
		pausePanel.SetActive(GameManager.PAUSED);
	}

	private void Update()
	{
		if (Input.GetButtonDown("Cancel"))
		{
			SwitchPause();
			//Debug.Log(GameManager.PAUSED);
		}

		if (Input.GetAxisRaw("Horizontal") > 0)
		{

		}

		if (Input.GetAxisRaw("Horizontal") < 0)
		{

		}
	}
	
	public void SwitchPause()
	{
		GameManager.Instance.PauseGame(!GameManager.PAUSED);
		pausePanel.SetActive(GameManager.PAUSED);
	}

	public void Quit()
	{
		//do saving first
		GameManager.Instance.Save();

		Application.Quit();
	}

	public void ReturnToMenu()
	{
		//do saving first
		GameManager.Instance.Save();

		SceneManager.LoadSceneAsync(mainMenuScene, LoadSceneMode.Additive);
	}
}
