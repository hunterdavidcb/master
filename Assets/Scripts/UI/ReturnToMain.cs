﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetButtonDown("Cancel"))
		{
            SceneManager.LoadSceneAsync(1);
		}
    }

    public void Return()
	{
        SceneManager.LoadSceneAsync(1);
    }
}
