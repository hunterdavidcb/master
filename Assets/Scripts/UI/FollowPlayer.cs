﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
	[SerializeField]
	Transform player;// = default;

	[SerializeField]
	float horizontalRotationSpeed = 90f;
	[SerializeField]
	float verticalRotationSpeed = 90f;

	[SerializeField, Tooltip("The farthest distance the camera may get from the player"), Range(7f, 35f)]
	float maxDistance;
	[SerializeField, Tooltip("The closest the camera may get to the player"), Range(7f, 35f)]
	float minDistance;
	[SerializeField, Tooltip("The speed of the camera zooming in and out"), Range(1f, 100f)]
	float zoomSpeed;

	[SerializeField,Tooltip("The lowest angle the camera can take, 0 is looking straight ahead"),Range(-20f,90f)]
	float minAngle;
	[SerializeField,Tooltip("The highest angle the camera can take, 90 is looking straight down"), Range(0f, 90f)]
	float maxAngle;

	[SerializeField, Min(0f)]
	float focusRadius = 5f;

	[SerializeField, Range(0f, 1f)]
	float focusCentering = 0.5f;



	Vector3 cv;

	private Vector3 offset;
	//[SerializeField, Range(0f, 1f)]
	//private float smoothFactor = 0.5f;

	public bool lookAtPlayer;
	public bool rotateAroundPlayer;
	public bool canZoom;
	public bool canControlVerticalAngle;
	public bool zoomControlsVerticalAngle;

	Vector2 orbitAngles = new Vector2(45f, 0f);
	private float distance;

	Vector3 focusPoint;

	private void OnDialogueTriggered(DialogueContainer dc)
	{
		//throw new NotImplementedException();
	}

	// Start is called before the first frame update
	void Start()
    {
		//this is only used for levels where we don't spawn in the player
		if (player != null)
		{
			offset = new Vector3(player.position.x, player.position.y + 14, player.position.z - 14) - (player.position + Vector3.up);
			distance = offset.magnitude;
			focusPoint = player.position + Vector3.up;
			//offset = transform.position - player.position;
		}

	}

	void OnValidate()
	{
		if (maxAngle < minAngle)
		{
			maxAngle = minAngle;
		}

		if (maxDistance < minDistance)
		{
			maxDistance = minDistance;
		}
	}

	private void Update()
	{
		//if (Input.GetAxis("Attack") > 0.1f)
		//{
		//	Debug.Log("attack");
		//}

		//if (Input.GetAxis("Camera Trigger") > .1f)
		//{
		//	Debug.Log("camera");
		//}
	}

	//keep this on late update, it gives smoother motion
	//my original version
	void LateUpdate()
	{
		if (player != null)
		{
			UpdateFocusPoint();
			//SetDistance();
			Quaternion lookRotation = transform.localRotation;
			if (rotateAroundPlayer)
			{
				
				if (Input.GetAxis("Camera Trigger") > .1f)//Input.GetMouseButton(2))
				{
					//modified from Cat-like coding
					//https://catlikecoding.com/unity/tutorials/movement/orbit-camera/
					if (rotateAroundPlayer && ManualRotation())
					{
						ConstrainAngles();
						if (!canControlVerticalAngle && !canZoom)
						{
							orbitAngles.x = 45f;
						}

						
					}
					else
					{
						lookRotation = transform.localRotation;
					}


					if (canZoom && !canControlVerticalAngle && zoomControlsVerticalAngle)
					{
						float delta = Input.GetAxis("Camera Vertical") == 0f ? Input.GetAxis("Mouse Y") : Input.GetAxis("Camera Vertical");
						float d = -delta * zoomSpeed * Time.deltaTime;
						distance += d;
						distance -= distance * -d * zoomSpeed * Time.deltaTime;
						distance = Mathf.Clamp(distance, minDistance, maxDistance);
						orbitAngles.x = Mathf.Lerp(minAngle, maxAngle, distance / maxDistance);
					}
					else if (canZoom && ((Input.GetButton("Zoom") || Input.GetAxis("Attack") > 0.1f)))
					{
						float delta = Input.GetAxis("Camera Vertical") == 0f ? Input.GetAxis("Mouse Y") : Input.GetAxis("Camera Vertical");
						float d = -delta * zoomSpeed * Time.deltaTime;
						distance += d;
						distance -= distance * -d * zoomSpeed * Time.deltaTime;
						distance = Mathf.Clamp(distance, minDistance, maxDistance);
					}

					lookRotation = Quaternion.Euler(orbitAngles);

				}
				else
				{
					lookRotation = transform.localRotation;
				}

				//TODO: change this to use a different input
				//float d = Input.GetAxis("Mouse ScrollWheel");
				//offset += offset * d;
			}


			//modified from Cat-like coding
			//https://catlikecoding.com/unity/tutorials/movement/orbit-camera/
			Vector3 lookDirection = lookRotation * Vector3.forward;
			Vector3 lookPosition = focusPoint - lookDirection * distance;
			transform.SetPositionAndRotation(lookPosition, lookRotation);
		}
	}

	internal void SetPlayer(Transform t)
	{
		player = t;
		player.GetComponent<PlayerInteraction>().DialogueTriggered += OnDialogueTriggered;
		offset = new Vector3(player.position.x, player.position.y + 14, player.position.z - 14) - (player.position + Vector3.up);
		distance = offset.magnitude;
		UpdateFocusPoint();
	}

	public void SetCameraStyle(bool canR, bool canZ, bool canPitch, bool zoomVertical, float hrs, float vrs, float zs)
	{
		rotateAroundPlayer = canR;
		canZoom = canZ;
		canControlVerticalAngle = canPitch;
		zoomControlsVerticalAngle = zoomVertical;
		horizontalRotationSpeed = hrs;
		verticalRotationSpeed = vrs;
		zoomSpeed = zs;
	}

	//public static Vector3 ClampMagnitude(Vector3 v, float max, float min)
	//{
	//	double sm = v.sqrMagnitude;
	//	if (sm > (double)max * (double)max) return v.normalized * max;
	//	else if (sm < (double)min * (double)min) return v.normalized * min;
	//	return v;
	//}

	#region Modified from cat-like coding
	//https://catlikecoding.com/unity/tutorials/movement/orbit-camera/
	bool ManualRotation()
	{
		Vector2 input = new Vector2(
			Input.GetAxis("Camera Vertical") == 0f? Input.GetAxis("Mouse Y"): Input.GetAxis("Camera Vertical"),
			Input.GetAxis("Camera Horizontal") == 0f ? Input.GetAxis("Mouse X") : Input.GetAxis("Camera Horizontal"));

		const float e = 0.1f;
		if (input.x < -e || input.x > e || input.y < -e || input.y > e)
		{
			orbitAngles += new Vector2(canControlVerticalAngle? verticalRotationSpeed * input.x:
				0f, horizontalRotationSpeed * input.y) * Time.unscaledDeltaTime;
			return true;
		}

		return false;
	}

	void UpdateFocusPoint()
	{
		Vector3 targetPoint = player.position;
		targetPoint += Vector3.up;
		if (focusRadius > 0f)
		{
			float distance = Vector3.Distance(targetPoint, focusPoint);
			float t = 1f;
			if (distance > 0.01f && focusCentering > 0f)
			{
				t = Mathf.Pow(1f - focusCentering, Time.unscaledDeltaTime);
			}
			if (distance > focusRadius)
			{
				//focusPoint = Vector3.Lerp(
				//	targetPoint, focusPoint, focusRadius / distance
				//);
				t = Mathf.Min(t, focusRadius / distance);
			}
			focusPoint = Vector3.Lerp(targetPoint, focusPoint, t);
			
		}
		else
		{
			focusPoint = targetPoint;
		}
	}

	void ConstrainAngles()
	{
		orbitAngles.x =
			Mathf.Clamp(orbitAngles.x, minAngle, maxAngle);

		if (orbitAngles.y < 0f)
		{
			orbitAngles.y += 360f;
		}
		else if (orbitAngles.y >= 360f)
		{
			orbitAngles.y -= 360f;
		}
	}

	#endregion

}
