﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public GameObject optionsPanel;
    [SerializeField]
    Toggle canRotate;
    [SerializeField]
    Toggle canZoom;
    [SerializeField]
    Toggle canControlCameraAngle;
    [SerializeField]
    Toggle zoomControlsCameraAngle;
    [SerializeField]
    Dropdown playerMovementStyleDropdown;

    [SerializeField]
    Dropdown playerAttackStyleDropdown;

    [SerializeField]
    Slider horizontalRotationSpeedSlider;
    [SerializeField]
    Slider verticalRotationSpeedSlider;
    [SerializeField]
    Slider zoomSpeedSlider;

    private void Awake()
	{
        //just in case
        playerMovementStyleDropdown.ClearOptions();

        List<string> options = new List<string>();
        //get all the enumerated vales, no matter how many
		for (int i = 0; i < System.Enum.GetNames(typeof(PlayerMovementStyle)).Length; i++)
		{
            options.Add(((PlayerMovementStyle)i).ToString());
		}

        playerMovementStyleDropdown.AddOptions(options);


        //just in case
        playerAttackStyleDropdown.ClearOptions();

        options = new List<string>();
        //get all the enumerated vales, no matter how many
        for (int i = 0; i < System.Enum.GetNames(typeof(PlayerAttackStyle)).Length; i++)
        {
            options.Add(((PlayerAttackStyle)i).ToString());
        }

        playerAttackStyleDropdown.AddOptions(options);
    }

    public void ClosePanel()
	{
        optionsPanel.SetActive(false);
        // we need to get this info to the game manager
        GameManager.Instance.SetGameOptions((PlayerMovementStyle)playerMovementStyleDropdown.value,
            (PlayerAttackStyle)playerAttackStyleDropdown.value, canRotate.isOn,
            canZoom.isOn, canControlCameraAngle.isOn, zoomControlsCameraAngle.isOn,horizontalRotationSpeedSlider.value,
            verticalRotationSpeedSlider.value, zoomSpeedSlider.value);
	}

    public void SwitchZoomControl()
	{
		if (canZoom.isOn)
		{
            canControlCameraAngle.gameObject.SetActive(true);
            zoomControlsCameraAngle.gameObject.SetActive(true);
        }
        else
		{
            canControlCameraAngle.gameObject.SetActive(false);
            zoomControlsCameraAngle.gameObject.SetActive(false);
        }
	}

    public void CanControl()
	{
        zoomControlsCameraAngle.isOn = false;
    }

    public void CantControl()
    {
        canControlCameraAngle.isOn = false;
    }

    public void OpenPanel()
    {
        optionsPanel.SetActive(true);
    }
}

public enum PlayerMovementStyle
{
    CameraRelative,
    PlayerRelativeMouseLook,
    AbsoluteMouseLook
}

public enum PlayerAttackStyle
{
    Swipes,
    Clicks
}
