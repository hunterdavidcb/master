﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class DollyZome : MonoBehaviour
{
	Transform target;
	float dollySpeed = 5f;

	new Camera camera;
	float initialFrustrumHeight;

	private void Awake()
	{
		camera = GetComponent<Camera>();
		float d = Vector3.Distance(transform.position, target.position);
		initialFrustrumHeight = ComputeFrustrumHeight(d);
	}

	float ComputeFrustrumHeight(float distance)
	{
		return (2f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad));
	}

	float ComputeFieldOfView(float height, float distance)
	{
		return (2f *  Mathf.Atan(height * 0.5f / distance) * Mathf.Rad2Deg);
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		transform.Translate(Input.GetAxis("Vertical") * Vector3.forward * Time.deltaTime * dollySpeed);
		float c = Vector3.Distance(transform.position, target.position);
		camera.fieldOfView = ComputeFieldOfView(initialFrustrumHeight, c);
	}
}
