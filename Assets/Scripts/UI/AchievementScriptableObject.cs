﻿using UnityEngine;

[CreateAssetMenu(fileName ="AchievementList.asset")]
public class AchievementScriptableObject : ScriptableObject 
{
	public Achievement[] achievements;
}
