﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AchievementManager : MonoBehaviour 
{
	static private AchievementManager _S; // A private singleton for AchievementManager

	[Header("Set in Inspector")]
	public AchievementPopUp popUp;
	public AchievementScriptableObject achievementList;
	private static Achievement[] ach;

	//private variables
	Dictionary<int, int> jumpsPerLevelUsed; // key is level, value is number of jumps
	int luckyShots = 0;
	int asteroidsShot = 0;
	int currentScore = 0;

	//callbacks to let other scripts know an achievement has been unlocked
	public delegate void AchievementStatusHandler(Achievement ach);
	public static event AchievementStatusHandler AchievementUnlocked;

	private void Awake()
	{
		S = this;
		jumpsPerLevelUsed = new Dictionary<int, int>();
	}


	void Start () 
	{
		// achievement code stays inside the achievement manager, so we don't have random calls to AchievementManager
		//scattered throughout the code base.
		//AsteraX.AsteroidShot += OnAsteroidShot;
		//AsteraX.BulletFired += OnBulletFired;
		//AsteraX.ScoreUpdated += OnScoreUpdated;
		//AsteraX.ShipJumped += OnShipJump;
		//AsteraX.LevelAdvanced += OnLevelAdvanced;

		SetAchievements(achievementList.achievements);
		UpdateAchievementStatus();
		
	}

	private void SetAchievements(Achievement[] achievements)
	{
		ach = achievements;
	}

	//updates which achievements are locked and unlocked based on gamedata
	public void UpdateAchievementStatus()
	{
		//if (asterax.gamedata != null)
		//{
		//	for (int j = 0; j < achievementlist.achievements.length; j++)
		//	{
		//		if (asterax.gamedata.completedachievements.contains(achievementlist.achievements[j].acheivementname))
		//		{
		//			if (!achievementlist.achievements[j].unlocked)
		//			{
		//				achievementlist.achievements[j].unlocked = true;
		//			}
		//		}
		//		else
		//		{
		//			achievementlist.achievements[j].unlocked = false;
		//		}
		//	}
		//}
	}

	//we've just completed a level, update stuff
	protected void OnLevelAdvanced(int level)
	{
		//if (AsteraX.GameData.HighScore < currentScore)
		//{
		//	AsteraX.GameData.SetHighScore(currentScore);
		//	AnnounceHighScore(currentScore);
		//}

		
		//for (int i = 0; i < achievementList.achievements.Length; i++)
		//{
		//	if (!achievementList.achievements[i].unlocked)
		//	{
		//		//check whether there is an achievement associated with this level
		//		if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.Level)
		//		{
		//			if (!achievementList.achievements[i].unlocked &&
		//				level >= achievementList.achievements[i].count)
		//			{
		//				AnnounceAchievementCompletion(achievementList.achievements[i]);
		//			}

		//		}
		//		//check whether any jumps were used
		//		//This is not necessary for currently implemented achievements, but this could be processed
		//		//as below
		//		else if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.UsedJumpInLevel)
		//		{
		//			//make sure the dictionary has the correct number
		//			if (!jumpsPerLevelUsed.ContainsKey(level))
		//			{
		//				jumpsPerLevelUsed.Add(level, 0);
		//			}

		//			if (!achievementList.achievements[i].unlocked &&
		//				jumpsPerLevelUsed[level] == achievementList.achievements[i].count)
		//			{
		//				AnnounceAchievementCompletion(achievementList.achievements[i]);
		//			}
		//		}
		//	}

		//}
	}

	//we just jumped
	protected void OnShipJump()
	{
		//Debug.Log("final location");
		//if (jumpsPerLevelUsed.ContainsKey(AsteraX.GAME_LEVEL))
		//{
		//	jumpsPerLevelUsed[AsteraX.GAME_LEVEL] = jumpsPerLevelUsed[AsteraX.GAME_LEVEL] + 1;
		//}
		//else
		//{
		//	jumpsPerLevelUsed.Add(AsteraX.GAME_LEVEL, 1);
		//}
	}

	//asteroid was shot, check whether this was a lucky shot or what
	protected void OnAsteroidShot(bool wrappedBullet)
	{
		asteroidsShot++;
		//AsteraX.GameData.SetShotAsteroids(asteroidsShot);
		//if (wrappedBullet)
		//{
		//	luckyShots++;
		//	AsteraX.GameData.SetLuckyShots(luckyShots);
		//}


		//ugly copy-pasted code from before
		for (int i = 0; i < achievementList.achievements.Length; i++)
		{
			if (!achievementList.achievements[i].unlocked)
			{
				if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.Shot)
				{
					if (!achievementList.achievements[i].unlocked &&
						asteroidsShot >= achievementList.achievements[i].count)
					{
						AnnounceAchievementCompletion(achievementList.achievements[i]);
					}

				}
				else if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.WrappedShot)
				{
					if (!achievementList.achievements[i].unlocked &&
						asteroidsShot >= achievementList.achievements[i].count)
					{
						AnnounceAchievementCompletion(achievementList.achievements[i]);
					}
				}
			}

		}

	}

	//the score has been updated
	protected void OnScoreUpdated(int score)
	{
		currentScore = score;
		//AsteraX.GameData.SetHighScore(score);

		for (int i = 0; i < achievementList.achievements.Length; i++)
		{
			if (!achievementList.achievements[i].unlocked)
			{
				if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.Score)
				{
					if (!achievementList.achievements[i].unlocked &&
						score >= achievementList.achievements[i].count)
					{
						AnnounceAchievementCompletion(achievementList.achievements[i]);
					}

				}
			}

		}
	}

	//we fired a bullet
	protected void OnBulletFired(int count)
	{
		//AsteraX.GameData.SetBulletsFired(count);

		for (int i = 0; i < achievementList.achievements.Length; i++)
		{
			if (!achievementList.achievements[i].unlocked)
			{
				if (achievementList.achievements[i].typeOfAcheivement == AcheivementType.Fired)
				{
					if (!achievementList.achievements[i].unlocked &&
						count >= achievementList.achievements[i].count)
					{
						AnnounceAchievementCompletion(achievementList.achievements[i]);
					}

				}
			}

		}
	}

	/// <summary>
	/// This allows us to trigger a dropdown to let the player know how awesome they are
	/// </summary>
	/// <param name="achievementName">the name of the achievement</param>
	/// <param name="achievementDescription">the description of the achievement</param>
	void TriggerPopUp(string achievementName, string achievementDescription = "")
	{
		popUp.PopUp(achievementName, achievementDescription);
	}


	/// <summary>
	/// This function updates the achievement list, triggers saving data, and sends out a
	/// notification to Unity Analytics
	/// </summary>
	/// <param name="ach"></param>
	void AnnounceAchievementCompletion(Achievement ach)
	{
		ach.unlocked = true;
		//save the data
		//AsteraX.GameData.AddAchievement(ach.AcheivementName);
		//send the event to Unity
		//AnalyticsEvent.AchievementUnlocked(ach.AcheivementName, new Dictionary<string, object>
	//	{
	//		{"time",DateTime.Now }
	//	});

		//trigger callback
		if (AchievementUnlocked != null)
		{
			AchievementUnlocked(ach);
		}
		string desc = ach.description.Replace("#", ach.count.ToString("N0"));
		S.TriggerPopUp(ach.AcheivementName, desc);
	}

	void AnnounceHighScore(int score)
	{
		string desc = "You've achieved a new high score: " + score.ToString("N0");
		S.TriggerPopUp("High Score!", desc);
	}

	// ———————————————— Statics ———————————————— //

	/// <summary>
	/// <para>This static private property provides some protection for the Singleton _S.</para>
	/// <para>get {} does return null, but throws an error first.</para>
	/// <para>set {} allows overwrite of _S by a 2nd instance, but throws an error first.</para>
	/// <para>Another advantage of using a property here is that it allows you to place
	/// a breakpoint in the set clause and then look at the call stack if you fear that 
	/// something random is setting your _S value.</para>
	/// </summary>
	static private AchievementManager S
	{
		get
		{
			if (_S == null)
			{
				Debug.LogError("AchievementManager:S getter - Attempt to get "
							   + "value of S before it has been set.");
				return null;
			}
			return _S;
		}
		set
		{
			if (_S != null)
			{
				Debug.LogError("AchievementManager:S setter - Attempt to set S "
							   + "when it has already been set.");
			}
			_S = value;
		}
	}

	#region Public Getters

	public int LuckyShots
	{
		get { return luckyShots; }
	}

	public Dictionary<int,int> JumpsPerLevel
	{
		get { return jumpsPerLevelUsed; }
	}

	public int AsteroidsShot
	{
		get { return asteroidsShot; }
	}

	public List<string> CompletedAchievements
	{
		get
		{
			List<string> ca = new List<string>();
			for (int i = 0; i < achievementList.achievements.Length; i++)
			{
				if (achievementList.achievements[i].unlocked)
				{
					ca.Add(achievementList.achievements[i].AcheivementName);
				}
			}
			return ca;
		}
	}

	public static Achievement[] Achievements
	{
		get { return ach; }
	}

	#endregion

	//unsubscribe to the static events in AsteraX
	private void OnDestroy()
	{
		//AsteraX.AsteroidShot -= OnAsteroidShot;
		//AsteraX.BulletFired -= OnBulletFired;
		//AsteraX.ScoreUpdated -= OnScoreUpdated;
		//AsteraX.ShipJumped -= OnShipJump;
		//AsteraX.LevelAdvanced -= OnLevelAdvanced;
	}
}
