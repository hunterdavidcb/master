﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	[SerializeField,Tooltip("How long to wait before the title fade in begins")]
	float titleDelay;
	[SerializeField, Tooltip("How long to wait before the button fade in begins")]
	float buttonsDelay;

	[SerializeField]
	List<Button> buttons;
	[SerializeField]
	Image title;

	[SerializeField]
	Button continueButton;

	[SerializeField]
	Button optionsButton;
	[SerializeField]
	GameObject optionsPanel;
	[SerializeField]
	new RectTransform particleSystem;

	[FMODUnity.EventRef]
	public string buttonPress;

	Color originalButton;
	Color originalTitle;
	Color originalText;

	[SerializeField, Tooltip("How long title fade in takes")]
	float titleFadeTime;
	[SerializeField, Tooltip("How long button fade in takes")]
	float buttonFadeTime;

	[SerializeField]
	public int sceneToLoadOnStartNew;
	[SerializeField]
	public int sceneToLoadOnContinue;

	int currentButton = 0;
	bool isMoving = false;

	private void Start()
	{
		optionsPanel.SetActive(false);
		EnableButtons(false);
		originalButton = buttons[0].image.color;
		originalText = buttons[0].GetComponentInChildren<Text>().color;

		originalTitle = title.color;
		HideButtons();

		HideTitle();

		Invoke("ShowTitle", titleDelay);
		Invoke("ShowButtons",buttonsDelay);
	}

	private void HideTitle()
	{
		originalTitle.a = 0f;
		title.color = originalTitle;
	}

	private void ShowButtons()
	{
		StartCoroutine(DisplayButtons(true));
	}

	private void ShowTitle()
	{
		StartCoroutine(DisplayTitle(true));
	}

	private void HideButtons()
	{
		particleSystem.gameObject.SetActive(false);
		originalButton.a = 0f;
		originalText.a = 0f;
		SetButtonsColor(originalButton);
	}

	private IEnumerator DisplayTitle(bool b)
	{
		if (b)
		{
			float a = 0;
			while (a < 1f)
			{
				a += Time.deltaTime / titleFadeTime;
				originalTitle.a = a;
				title.color = originalTitle;
				yield return null;
			}
			originalTitle.a = 1f;
			title.color = originalTitle;
		}
		else
		{
			float a = 1;
			while (a > 0)
			{
				a -= Time.deltaTime / titleFadeTime;
				originalTitle.a = a;
				title.color = originalTitle;
				yield return null;
			}

			originalTitle.a = 0f;
			title.color = originalTitle;
		}


	}

	private IEnumerator DisplayButtons(bool b)
	{
		if (b)
		{
			float a = 0;
			while (a < 1f)
			{
				a += Time.deltaTime / buttonFadeTime;
				//originalButton.a = a;
				originalText.a = a;
				SetButtonsColor(originalButton);
				yield return null;
			}
			//originalButton.a = 1f;
			originalText.a = 1f;
			SetButtonsColor(originalButton);
			EnableButtons(true);
		}
		else
		{
			float a = 1;
			particleSystem.gameObject.SetActive(false);
			while (a > 0)
			{
				a -= Time.deltaTime / buttonFadeTime;
				//originalButton.a = a;
				originalText.a = a;
				SetButtonsColor(originalButton);
				yield return null;
			}

			//originalButton.a = 0f;
			originalText.a = 0;
			SetButtonsColor(originalButton);
			EnableButtons(false);
		}


	}

	private void SetButtonsColor(Color c)
	{
		foreach (var button in buttons)
		{
			button.GetComponentInChildren<Text>().color = originalText;
			button.image.color = c;
		}
	}


	private void EnableButtons(bool b)
	{
		foreach (var button in buttons)
		{
			button.interactable = b;
			if (b)
			{
				button.GetComponent<Image>().enabled = true;
				particleSystem.gameObject.SetActive(true);
			}
		}

		if (b)
		{
			SwitchActiveButton();
		}
		
	}

	private void Update()
	{
		if (Input.GetAxisRaw("Vertical") < 0f && !isMoving)
		{
			//Debug.Log("less");
			isMoving = true;
			currentButton++;
			if (currentButton >= buttons.Count)
			{
				//Debug.Log("here");
				currentButton = 0;
			}
			SwitchActiveButton();
			return;
		}

		if (Input.GetAxisRaw("Vertical") > 0f && !isMoving)
		{
			//Debug.Log("more");
			isMoving = true;
			currentButton--;
			if (currentButton < 0)
			{
				currentButton = buttons.Count-1;
			}
			SwitchActiveButton();
			return;
		}

		if (Input.GetAxisRaw("Vertical") == 0f)
		{
			isMoving = false;
			return;
		}
	}

	void SwitchActiveButton()
	{
		//Debug.Log("switch");
		//Debug.Log(currentButton);
		buttons[currentButton].Select();
		particleSystem.anchoredPosition = buttons[currentButton].GetComponent<RectTransform>().anchoredPosition 
			- new Vector2(145f,0f);
		//Debug.Log(buttons[currentButton].name);
	}

	public void OnStartNewPressed()
	{
		GameManager.Instance.newStart = true;
		SceneManager.LoadScene(sceneToLoadOnStartNew,LoadSceneMode.Additive);
		ButtonPressed();
	}

	public void EnableContinue(bool b)
	{
		continueButton.gameObject.SetActive(b);
	}

	public void OnContinuePressed()
	{
		GameManager.Instance.newStart = false;
		SceneManager.LoadScene(sceneToLoadOnContinue, LoadSceneMode.Additive);
		ButtonPressed();
	}

	public void ButtonPressed()
	{
		FMODUnity.RuntimeManager.PlayOneShot(buttonPress);
	}

	public void OnQuitPressed()
	{
		ButtonPressed();
		Application.Quit();
	}
}
