﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AbilitySwitch : MonoBehaviour
{
	public GameObject abilitySwitchPanel;

	public GameObject mainAbilityHolder;
	public GameObject secondaryAbilityHolder;

	[SerializeField,Tooltip("The maximum amount of time we want to allow slow motion")]
	float maxLength;
	[SerializeField, Tooltip("The amount of time to take when changing time scale"), Range(0f, 1f)]
	float fadeTime;
	[SerializeField, Tooltip("How slow things will get, 0 is frozen, 1 is normal speed"), Range(0f, 1f)]
	float timeScale;

	public delegate void AbilitySwitchHandler();
	public static event AbilitySwitchHandler AbilitySwitchActivated;
	public static event AbilitySwitchHandler AbilitySwitchDeactivated;

	List<Button> currentButtons = new List<Button>();

	Vector3 previousMouse = Vector3.zero;

	public delegate void AbilityHandler(string abilityName);
	public event AbilityHandler AbilitySelected;

	Dictionary<string, Ability> abilities = new Dictionary<string, Ability>();

	[FMODUnity.EventRef]
	public string buttonPress;

	// Start is called before the first frame update
	void Start()
    {
		ActivatePanel(false);
    }

	public void Called(string s)
	{
		//Debug.Log(s);
		FMODUnity.RuntimeManager.PlayOneShot(buttonPress);
	}

	public void AddAbility(Ability a)
	{
		abilities.Add(a.Name, a);
		a.AbilityLearned += OnAbilityLearned;
	}

	private void OnAbilityLearned(Ability abilty)
	{
		//unlock the button here
	}

	public void Clicked(string name)
	{
		Debug.Log(name);
		if (abilities.ContainsKey(name))
		{
			//Debug.Log("a real ability");
			if (AbilitySelected != null)
			{
				AbilitySelected(name);
			}
		}
		else
		{
			currentButtons.Clear();
			mainAbilityHolder.SetActive(false);
			secondaryAbilityHolder.SetActive(true);

			int cur = 0;
			foreach (var item in abilities)
			{
				if (item.Value.Skill.Name.Equals(name))
				{
					Button button = secondaryAbilityHolder.transform.GetChild(cur).GetComponent<Button>();
					currentButtons.Add(button);

					//to avoid any potential issues
					string s = item.Key;

					button.onClick.RemoveAllListeners();

					if (item.Value.Learned)
					{
						button.onClick.AddListener(() => Clicked(s));
					}
					else
					{
						button.interactable = false;
					}
					

					button.GetComponentInChildren<Text>().text = s;

					cur++;
				}
			}

		}
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetButtonDown("AbilityTrigger"))
		{
			//first we stop any count down, or fading
			StopAllCoroutines();


			ActivatePanel(true);
			StartCoroutine(Fade(true));

			if (AbilitySwitchActivated != null)
			{
				AbilitySwitchActivated();
			}
		}

		if (Input.GetButtonUp("AbilityTrigger"))
		{
			//first we stop any count down, or fading
			StopAllCoroutines();

			StartCoroutine(Fade(false));
			ActivatePanel(false);
			if (AbilitySwitchDeactivated != null)
			{
				AbilitySwitchDeactivated();
			}
		}

		if (abilitySwitchPanel.activeSelf)
		{
			HandleInput();
		}

	}

	void HandleInput()
	{

		if (previousMouse != Input.mousePosition)
		{
			previousMouse = Input.mousePosition;
			return;
		}

		Vector2 pos = new Vector2(Input.GetAxis("Camera Horizontal"), Input.GetAxis("Camera Vertical"));

		if (pos.magnitude < 0.1f && currentButtons.Count != 7)
		{
			return;
		}

		//Debug.Log(pos);
		float angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;

		angle -= 90f;

		if (angle < 0)
		{
			angle += 360;
		}


		//Debug.Log(angle);
		//Debug.Log(currentButtons.Count);
		if (angle == 0)
		{
			if (currentButtons.Count == 7)
			{
				if (EventSystem.current.currentSelectedGameObject != currentButtons[6].gameObject)
				{
					currentButtons[6].Select();
				}
			}
		}
		else if (angle > 0 && angle <= 60)
		{
			
			if (EventSystem.current.currentSelectedGameObject != currentButtons[0].gameObject)
			{
				Debug.Log(0);
				currentButtons[0].Select();
			}
			
		}

		else if (angle > 60 && angle <= 120)
		{
			//Debug.Log(1);
			if (EventSystem.current.currentSelectedGameObject != currentButtons[1].gameObject)
			{
				Debug.Log(1);
				currentButtons[1].Select();
			}
			
		}
		else if (angle > 120 && angle <= 180)
		{
			//Debug.Log(2);
			if (EventSystem.current.currentSelectedGameObject != currentButtons[2].gameObject)
			{
				Debug.Log(2);
				currentButtons[2].Select();
			}
		}

		else if (angle <= 360 && angle > 300)
		{
			if (EventSystem.current.currentSelectedGameObject != currentButtons[3].gameObject)
			{
				//Debug.Log(3);
				currentButtons[3].Select();
			}
		}
		else if (angle <= 300 && angle > 240)
		{
			if (EventSystem.current.currentSelectedGameObject != currentButtons[4].gameObject)
			{
				//Debug.Log(4);
				currentButtons[4].Select();
			}
		}
		else if (angle <= 240 && angle > 180)
		{
			if (EventSystem.current.currentSelectedGameObject != currentButtons[5].gameObject)
			{
				//Debug.Log(5);
				currentButtons[5].Select();
			}
		}

	}

	IEnumerator Fade(bool isFadeIn)
	{
		float t = 0f;
		if (isFadeIn)
		{
			while (t < fadeTime)
			{
				t += Time.unscaledDeltaTime;
				Time.timeScale = Mathf.Lerp(1f, timeScale, t / fadeTime);
				//we also adjust the fixed delta time to take into account the changing timescale
				Time.fixedDeltaTime = Time.timeScale * 0.02f;
				yield return null;
			}

			Time.timeScale = timeScale;
			//we start the timer here!!
			StartCoroutine(TimeLimit());
		}
		else
		{
			while (t < fadeTime)
			{
				t += Time.unscaledDeltaTime;
				Time.timeScale = Mathf.Lerp(timeScale, 1f, t / fadeTime);
				//we also adjust the fixed delta time to take into account the changing timescale
				Time.fixedDeltaTime = Time.timeScale * 0.02f;
				yield return null;
			}
			Time.timeScale = 1f;
		}
	}

	IEnumerator TimeLimit()
	{
		float t = 0f;
		while (t < maxLength)
		{
			t += Time.unscaledDeltaTime;
			yield return null;
		}

		StartCoroutine(Fade(false));
		ActivatePanel(false);
		if (AbilitySwitchDeactivated != null)
		{
			AbilitySwitchDeactivated();
		}
	}

	void ActivatePanel(bool b)
	{
		Time.timeScale = b ? timeScale : 1f;
		abilitySwitchPanel.SetActive(b);
		currentButtons.Clear();

		mainAbilityHolder.SetActive(true);

		for (int i = 0; i < mainAbilityHolder.transform.childCount; i++)
		{
			Button button = mainAbilityHolder.transform.GetChild(i).GetComponent<Button>();
			currentButtons.Add(button);

			//to avoid any potential issues
			string s = button.name;

			button.onClick.RemoveAllListeners();
			button.onClick.AddListener( () => Clicked(s));
		}

		secondaryAbilityHolder.SetActive(false);
	}
}
