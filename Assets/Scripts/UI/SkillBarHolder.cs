﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillBarHolder : MonoBehaviour
{
	public GameObject skillBarPrefab;

	[NonSerialized]
	Dictionary<Skill, GameObject> skillGamePair = new Dictionary<Skill, GameObject>();

	public void AddSkill(Skill skill)
	{

		GameObject skillBar = Instantiate(skillBarPrefab, transform);
		SkillBar sb = skillBar.GetComponent<SkillBar>();
		skillGamePair.Add(skill, skillBar);
		//set values
		SetValues(sb, skill);
		//up callbacks
		skill.LeveledUp += OnSkillLeveledUp;
		skill.XPChanged += OnSkillXPIncreased;
	}

	public void OnDisable()
	{
		foreach (var skill in skillGamePair.Keys)
		{
			skill.LeveledUp -= OnSkillLeveledUp;
			skill.XPChanged -= OnSkillXPIncreased;
		}
	}

	void SetValues(SkillBar sb, Skill skill)
	{
		//set name
		sb.skillName.text = skill.Name;
		//set numbers
		sb.skillLevel.text = skill.CurrentLevel.ToString();
		sb.xpToNextLevel.text = skill.XPToNextLevel.ToString();
		//set image fill amount
		//Debug.Log(skill.CurrentXP);
		sb.currentAmount.fillAmount = skill.CurrentXP / skill.XPToNextLevel;
		//Debug.Log(sb.currentAmount.fillAmount);
	}

	private void OnSkillLeveledUp(Skill s)
	{
		SkillBar sb = skillGamePair[s].GetComponent<SkillBar>();
		SetValues(sb, s);
	}

	private void OnSkillXPIncreased(Skill s)
	{
		SkillBar sb = skillGamePair[s].GetComponent<SkillBar>();
		SetValues(sb, s);
	}
}
