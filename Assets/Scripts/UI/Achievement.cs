﻿using System;
using UnityEngine.UI;

[Serializable]
public class Achievement
{
	public string AcheivementName; //string holding the name of the achievement
	public string description;
	public AcheivementType typeOfAcheivement;
	public bool unlocked; // whether the achievment has been achieved or not
	public int count; //the number required in order to unlock the achievement
	//public ShipPart.eShipPartType partType; // The type of part unlocked by this Achievement
	public int partIndex; //the index of the part in the player ship parts scriptable object
}


public enum AcheivementType
{
	Shot,
	WrappedShot,
	Score,
	Level,
	Fired,
	UsedJumpInLevel
}
