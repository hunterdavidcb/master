﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AbilityDataHolder : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public AbilityData abilityData;

    public delegate void EnterHandler(Vector2 pos, string message);
    public event EnterHandler Entered;
    public event EnterHandler Exited;


	public void OnPointerEnter(PointerEventData eventData)
	{
		if (Entered != null)
		{
			//Debug.Log(GetComponent<RectTransform>().position);
			//Debug.Log(GetComponent<RectTransform>().anchoredPosition);

			Vector2 pos = GetComponent<RectTransform>().anchoredPosition - new Vector2(100,50);// - new Vector2(panelSize.x / 2, -panelSize.y / 2) + padding;
			Vector2 test = RectTransformUtility.WorldToScreenPoint(Camera.main, GetComponent<RectTransform>().position);
			//Vector2 pos = new Vector2(test.x - 250 - panelSize.x / 2, test.y - panelSize.y / 2); //  new Vector2(, Input.mousePosition.y);
			Entered(pos, abilityData.Description);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (Exited != null)
		{
			Exited(Vector2.zero, null);
		}
	}

	// Start is called before the first frame update
	void Start()
    {
		//Debug.Log("calling");
		//ShowTooltip.Register(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
