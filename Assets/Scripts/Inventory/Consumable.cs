﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Consumable : IItem, IEquipable//, ISerializationCallbackReceiver
{
	[SerializeField]
	string name;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	float weight;
	[SerializeField]
	EquipmentSlot slot;
	[SerializeField]
	List<StatusEffect> statusEffects;
	[SerializeField]
	DamageType weakAgainst;
	[SerializeField]
	float protectionAmountMin;
	[SerializeField]
	float protectionAmountMax;
	[SerializeField]
	float protectionAmount;
	[SerializeField]
	uint hitpoints;

	//Dictionary<StatData,List<StatusEffect>> effects;
	[SerializeField]
	Sprite icon;
	[SerializeField]
	GameObject prefab;

	public Consumable()
	{
		id = Guid.NewGuid().ToString();
		statusEffects = new List<StatusEffect>();
		protectionAmountMin = 0f;
		protectionAmountMax = 1f;
	}


	public Consumable(string n, string d)
	{
		name = n;
		description = d;
		id = Guid.NewGuid().ToString();
		protectionAmountMin = 0f;
		protectionAmountMax = 1f;
		statusEffects = new List<StatusEffect>();
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}

	public string ID
	{
		get { return id; }
	}

	public float Weight
	{
		get { return weight; }
	}

	public uint HitPoints
	{
		get { return hitpoints; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}

	public GameObject Prefab
	{
		get { return prefab; }
	}

	public EquipmentSlot Slot => throw new NotImplementedException();

	public bool Equipped => throw new NotImplementedException();

	public List<StatusEffect> Effects
	{
		get { return statusEffects; }
	}

	public DamageType WeakAgainst
	{
		get { return weakAgainst; }
	}

	public float ProtectionAmount
	{
		get { return protectionAmount; }
	}

	public float ProtectionAmountMin
	{
		get { return protectionAmountMin; }
	}

	public float ProtectionAmountMax
	{
		get { return protectionAmountMax; }
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeSprite(Sprite s)
	{
		icon = s;
	}

	public void ChangePrefab(GameObject go)
	{
		prefab = go;
	}

	public void ChangeWeight(float w)
	{
		
	}

	public void ChangeHP(uint hp)
	{
		hitpoints = hp;
	}

	public void ChangeProtectionAmount(float a)
	{
		protectionAmount = a;
	}

	public void ChangeMin(float a)
	{
		protectionAmountMin = a;
	}

	public void ChangeMax(float a)
	{
		protectionAmountMax = a;
	}

	public void ChangeWeakness(DamageType dt)
	{
		weakAgainst = dt;
	}

	public void AddEffect(StatusEffect se)
	{
		statusEffects.Add(se);
	}

	public void RemoveEffect(StatusEffect se)
	{
		statusEffects.Remove(se);
	}

	public void RemoveEffectAt(int i)
	{
		statusEffects.RemoveAt(i);
	}

	public void SetStatData(int i, StatData sd)
	{
		statusEffects[i].SetStatData(sd);
	}

	public void Equip()
	{
		
	
	}

	public void Unequip()
	{
		
	}
}
