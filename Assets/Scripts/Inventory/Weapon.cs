﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Weapon : IEquipable
{
	[SerializeField]
	string weaponName;
	[SerializeField]
	string description;
	[SerializeField]
	string id;
	[SerializeField]
	float baseAttack;
	[SerializeField]
	List<StatusEffect> effects;
	[SerializeField]
	Sprite icon;
	[SerializeField]
	GameObject prefab;

	public Weapon(string n, string d)
	{
		weaponName = n;
		description = d;
		id = Guid.NewGuid().ToString();
		effects = new List<StatusEffect>();
	}

	public string Name
	{
		get { return weaponName; }
	}

	public string Description
	{
		get { return description; }
	}

	public string ID
	{
		get { return id; }
	}

	public float BaseAttack
	{
		get { return baseAttack; }
	}

	public List<StatusEffect> Effects
	{
		get { return effects; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}

	public GameObject Prefab
	{
		get { return prefab; }
	}

	public EquipmentSlot Slot => throw new System.NotImplementedException();

	public bool Equipped => throw new System.NotImplementedException();

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeName(string n)
	{
		weaponName = n;
	}

	public void ChangeAttack(float a)
	{
		baseAttack = a;
	}

	public void ChangeIcon(Sprite s)
	{
		icon = s;
	}

	public void ChangePrefab(GameObject pref)
	{
		prefab = pref;
	}

	public void Equip()
	{
		//throw new System.NotImplementedException();
	}
}
