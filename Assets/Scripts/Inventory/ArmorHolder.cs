﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorHolder : MonoBehaviour, IDamagable, IInteractable
{
	[SerializeField, HideInInspector]
	int hitPoints;
	[SerializeField, HideInInspector]
	DamageType weakness;
	[SerializeField, HideInInspector]
	float protectionAmount;
	[SerializeField, HideInInspector]
	Consumable consumable;
	Rigidbody rb;

	bool interactable = false;


	public delegate void DamageHandler(uint amount);
	//the npc/enemy class should subscribe to these when armor is spawned in
	//the player class should to the same when these are equipped,
	//and unsubscribe when they are knocked off, or when they are unequipped
	public event DamageHandler Damaged;

	public void SetConsumable(Consumable c)
	{
		consumable = c;
	}

	public void SetStartHP(uint hp)
	{
		hitPoints = (int)hp;
	}

	public void SetWeakness(DamageType dt)
	{
		weakness = dt;
	}

	public void SetProtectionAmount(float amt)
	{
		protectionAmount = amt;
	}

	public DamageType WeakTo
	{
		get { return weakness; }
	}

	void Awake()
	{
		rb = GetComponent<Rigidbody>();

	}

	public void ReceiveDamage(float amt, DamageType dt, bool knockBack, Vector3 dir)
	{
		//Debug.Log("from armor holder");
		if (dt == weakness)
		{
			hitPoints -= (int)amt;

			amt -= protectionAmount * amt;
		}
		else
		{
			amt -= protectionAmount * amt;
			hitPoints -= (int)amt;
		}

		//pass amt on to the body
		if (Damaged != null)
		{
			Damaged((uint)amt);
		}


		Debug.Log(hitPoints);

		if (hitPoints <= 0)
		{
			rb.isKinematic = false;
			rb.useGravity = true;
			rb.AddForce((transform.position - transform.parent.parent.position).normalized * 20f);
			GetComponent<Collider>().isTrigger = false;
			interactable = true;
		}
		//throw new System.NotImplementedException();
	}

	public void Interact()
	{
		if (interactable)
		{
			//allow to be picked up
		}
	}

	//void OnTriggerEnter(Collider collider)
	//{
	//	//if (collider.tag == "Weapon")
	//	//{
	//	//	Debug.Log("success");
	//	//	//try to get the root
	//	//	Vector3 dir = transform.root.position - collider.transform.root.position;
	//	//	dir.Normalize();
	//	//	transform.root.GetComponent<Rigidbody>().AddForce(dir * 200f);
	//	//}
	//}
}
