<Q                         POINT      SHADOWS_CUBE    �*  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct VGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    float4 hlslcc_mtx4x4objectMatrix[4];
    float3 chunkCenter;
    float chunkSize;
    float splatRadius;
    int solidHighlightId;
    float _ClayxelSize;
    float _NormalOrient;
};

struct chunkPoints_Type
{
    uint value[4];
};

struct Mtl_VertexOut
{
    float4 mtl_Position [[ position ]];
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]];
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]];
    float4 COLOR0 [[ user(COLOR0) ]];
    float2 TEXCOORD2 [[ user(TEXCOORD2) ]];
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]];
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]];
};

template <int N> vec<uint, N> bitFieldExtractU(const vec<uint, N> width, const vec<uint, N> offset, const vec<uint, N> src)
{
	vec<bool, N> isWidthZero = (width == 0);
	vec<bool, N> needsClamp = ((width + offset) < 32);
	vec<uint, N> clampVersion = src << (32-(width+offset));
	clampVersion = clampVersion >> (32 - width);
	vec<uint, N> simpleVersion = src >> offset;
	vec<uint, N> res = select(simpleVersion, clampVersion, needsClamp);
	return select(res, vec<uint, N>(0), isWidthZero);
}; 
vertex Mtl_VertexOut xlatMtlMain(
    constant VGlobals_Type& VGlobals [[ buffer(0) ]],
    const device chunkPoints_Type *chunkPoints [[ buffer(1) ]],
    uint mtl_VertexID [[ vertex_id ]])
{
    Mtl_VertexOut output;
    chunkPoints = reinterpret_cast<const device chunkPoints_Type *> (reinterpret_cast<device const atomic_uint *> (chunkPoints) + 1);
    float4 u_xlat0;
    float3 u_xlat1;
    int3 u_xlati1;
    uint4 u_xlatu1;
    bool u_xlatb1;
    uint u_xlatu2;
    float4 u_xlat3;
    float4 u_xlat4;
    float3 u_xlat5;
    float3 u_xlat6;
    float3 u_xlat7;
    bool2 u_xlatb8;
    float4 u_xlat9;
    uint4 u_xlatu9;
    float3 u_xlat10;
    float3 u_xlat12;
    float3 u_xlat13;
    bool2 u_xlatb13;
    float u_xlat23;
    uint u_xlatu23;
    float u_xlat33;
    bool u_xlatb33;
    float u_xlat34;
    float u_xlat36;
    u_xlat0.x = VGlobals.hlslcc_mtx4x4unity_MatrixV[0].y;
    u_xlat0.y = VGlobals.hlslcc_mtx4x4unity_MatrixV[1].y;
    u_xlat0.z = VGlobals.hlslcc_mtx4x4unity_MatrixV[2].y;
    u_xlat33 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat33 = rsqrt(u_xlat33);
    u_xlat0.xyz = float3(u_xlat33) * u_xlat0.xyz;
    u_xlatu1.x = mtl_VertexID / 0x3u;
    u_xlatu2 = mtl_VertexID % 0x3u;
    u_xlatu1 = uint4(chunkPoints[u_xlatu1.x].value[(0x0 >> 2) + 0], chunkPoints[u_xlatu1.x].value[(0x0 >> 2) + 1], chunkPoints[u_xlatu1.x].value[(0x0 >> 2) + 2], chunkPoints[u_xlatu1.x].value[(0x0 >> 2) + 3]);
    u_xlat12.xy = float2(int2(u_xlatu1.zy));
    u_xlat13.xy = u_xlat12.xx * float2(2048.0, 0.00048828125);
    u_xlatb33 = u_xlat13.x>=(-u_xlat13.x);
    u_xlat3.x = floor(u_xlat13.y);
    u_xlat13.xy = (bool(u_xlatb33)) ? float2(2048.0, 0.00048828125) : float2(-2048.0, -0.00048828125);
    u_xlat33 = u_xlat12.x * u_xlat13.y;
    u_xlat4.xyz = u_xlat12.yyy * float3(5.96046448e-08, 1.52587891e-05, 0.00390625);
    u_xlat4.xyz = fract(u_xlat4.xyz);
    u_xlat33 = fract(u_xlat33);
    u_xlat3.y = u_xlat33 * u_xlat13.x;
    u_xlat12.xy = fma(u_xlat3.xy, float2(0.00097703957, 0.00097703957), float2(-1.0, -1.0));
    u_xlat33 = -abs(u_xlat12.x) + 1.0;
    u_xlat3.z = -abs(u_xlat12.y) + u_xlat33;
    u_xlat33 = (-u_xlat3.z);
    u_xlat33 = clamp(u_xlat33, 0.0f, 1.0f);
    u_xlatb13.xy = (u_xlat12.xy>=float2(0.0, 0.0));
    u_xlat13.x = (u_xlatb13.x) ? (-float(u_xlat33)) : float(u_xlat33);
    u_xlat13.y = (u_xlatb13.y) ? (-float(u_xlat33)) : float(u_xlat33);
    u_xlat3.xy = u_xlat12.xy + u_xlat13.xy;
    u_xlat33 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat33 = rsqrt(u_xlat33);
    u_xlat13.xyz = float3(u_xlat33) * u_xlat3.xyz;
    u_xlat3.xyz = u_xlat13.yyy * VGlobals.hlslcc_mtx4x4objectMatrix[1].xyz;
    u_xlat3.xyz = fma(VGlobals.hlslcc_mtx4x4objectMatrix[0].xyz, u_xlat13.xxx, u_xlat3.xyz);
    u_xlat13.xyz = fma(VGlobals.hlslcc_mtx4x4objectMatrix[2].xyz, u_xlat13.zzz, u_xlat3.xyz);
    u_xlat3.xyz = u_xlat0.zxy * u_xlat13.yzx;
    u_xlat3.xyz = fma(u_xlat0.yzx, u_xlat13.zxy, (-u_xlat3.xyz));
    u_xlat33 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat33 = rsqrt(u_xlat33);
    u_xlat5.x = VGlobals.hlslcc_mtx4x4unity_MatrixV[0].x;
    u_xlat5.y = VGlobals.hlslcc_mtx4x4unity_MatrixV[1].x;
    u_xlat5.z = VGlobals.hlslcc_mtx4x4unity_MatrixV[2].x;
    u_xlat12.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat12.x = rsqrt(u_xlat12.x);
    u_xlat5.xyz = u_xlat12.xxx * u_xlat5.xyz;
    u_xlat6.xyz = fma(u_xlat3.xyz, float3(u_xlat33), (-u_xlat5.xyz));
    u_xlat3.xyz = float3(u_xlat33) * u_xlat3.xyz;
    u_xlat6.xyz = fma(float3(VGlobals._NormalOrient), u_xlat6.xyz, u_xlat5.xyz);
    u_xlat33 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat33 = rsqrt(u_xlat33);
    u_xlat6.xyz = float3(u_xlat33) * u_xlat6.xyz;
    u_xlat33 = VGlobals.splatRadius * VGlobals._ClayxelSize;
    u_xlat12.x = u_xlat33 + u_xlat33;
    u_xlat6.xyz = u_xlat12.xxx * u_xlat6.xyz;
    u_xlat7.xyz = u_xlat12.xxx * u_xlat3.xyz;
    u_xlatb8.xy = (float2(VGlobals._NormalOrient)==float2(0.0, 1.0));
    u_xlat6.xyz = (u_xlatb8.y) ? u_xlat7.xyz : u_xlat6.xyz;
    u_xlatu23 = u_xlatu1.x >> 0x18u;
    u_xlat23 = float(int(u_xlatu23));
    u_xlat36 = VGlobals.chunkSize * 0.00390625;
    u_xlat7.x = u_xlat23 * u_xlat36;
    u_xlatu9 = bitFieldExtractU(uint4(0x8u, 0x8u, 0x8u, 0x8u), uint4(0x10u, 0x8u, 0x8u, 0x10u), u_xlatu1.xxww);
    u_xlati1.xz = int2(u_xlatu1.xw & uint2(0xffu, 0xffu));
    u_xlat9 = float4(int4(u_xlatu9));
    u_xlat7.yz = float2(u_xlat36) * u_xlat9.xy;
    u_xlat9.yz = u_xlat9.zw * float2(0.00392156886, 0.00392156886);
    u_xlat7.xyz = fma(float3(VGlobals.chunkSize), float3(-0.498046875, -0.498046875, -0.498046875), u_xlat7.xyz);
    u_xlat4.xyz = fma(u_xlat4.xyz, float3(u_xlat36), u_xlat7.xyz);
    u_xlat4.xyz = u_xlat4.xyz + VGlobals.chunkCenter.xyzx.xyz;
    u_xlat7.xyz = u_xlat4.yyy * VGlobals.hlslcc_mtx4x4objectMatrix[1].xyz;
    u_xlat4.xyw = fma(VGlobals.hlslcc_mtx4x4objectMatrix[0].xyz, u_xlat4.xxx, u_xlat7.xyz);
    u_xlat4.xyz = fma(VGlobals.hlslcc_mtx4x4objectMatrix[2].xyz, u_xlat4.zzz, u_xlat4.xyw);
    u_xlat4.xyz = u_xlat4.xyz + VGlobals.hlslcc_mtx4x4objectMatrix[3].xyz;
    u_xlat7.xyz = (-u_xlat4.xyz) + VGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat34 = dot(u_xlat7.xyz, u_xlat7.xyz);
    u_xlat34 = rsqrt(u_xlat34);
    u_xlat7.xyz = float3(u_xlat34) * u_xlat7.xyz;
    u_xlat10.xyz = u_xlat5.zxy * u_xlat7.yzx;
    u_xlat5.xyz = fma(u_xlat5.yzx, u_xlat7.zxy, (-u_xlat10.xyz));
    u_xlat34 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat34 = rsqrt(u_xlat34);
    u_xlat5.xyz = float3(u_xlat34) * u_xlat5.xyz;
    u_xlat10.xyz = u_xlat5.yzx * u_xlat7.zxy;
    u_xlat10.xyz = fma(u_xlat7.yzx, u_xlat5.zxy, (-u_xlat10.xyz));
    u_xlat5.xyz = float3(u_xlat33) * u_xlat5.xyz;
    u_xlat34 = dot(u_xlat10.xyz, u_xlat10.xyz);
    u_xlat34 = rsqrt(u_xlat34);
    u_xlat10.xyz = float3(u_xlat34) * u_xlat10.xyz;
    u_xlat10.xyz = u_xlat12.xxx * u_xlat10.xyz;
    u_xlat6.xyz = (u_xlatb8.x) ? u_xlat10.xyz : u_xlat6.xyz;
    u_xlat10.xyz = u_xlat13.yzx * u_xlat3.zxy;
    u_xlat3.xyz = fma(u_xlat3.yzx, u_xlat13.zxy, (-u_xlat10.xyz));
    u_xlat12.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat12.x = rsqrt(u_xlat12.x);
    u_xlat10.xyz = fma(u_xlat3.xyz, u_xlat12.xxx, (-u_xlat0.xyz));
    u_xlat3.xyz = u_xlat12.xxx * u_xlat3.xyz;
    u_xlat3.xyz = float3(u_xlat33) * u_xlat3.xyz;
    u_xlat0.xyz = fma(float3(VGlobals._NormalOrient), u_xlat10.xyz, u_xlat0.xyz);
    u_xlat12.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12.x = rsqrt(u_xlat12.x);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat12.xxx;
    u_xlat0.xyz = float3(u_xlat33) * u_xlat0.xyz;
    u_xlat0.xyz = (u_xlatb8.y) ? u_xlat3.xyz : u_xlat0.xyz;
    u_xlat0.xyz = (u_xlatb8.x) ? u_xlat5.xyz : u_xlat0.xyz;
    u_xlat3.xyz = (-u_xlat6.xyz) + (-u_xlat0.xyz);
    u_xlat5.xyz = u_xlat6.xyz + (-u_xlat0.xyz);
    u_xlat0.xyz = fma(u_xlat0.xyz, float3(1.70000005, 1.70000005, 1.70000005), u_xlat4.xyz);
    u_xlat5.xyz = u_xlat4.xyz + u_xlat5.xyz;
    u_xlat3.xyz = u_xlat3.xyz + u_xlat4.xyz;
    u_xlatb33 = int(u_xlatu2)==0x1;
    u_xlat0.xyz = (bool(u_xlatb33)) ? u_xlat3.xyz : u_xlat0.xyz;
    u_xlat12.xz = (bool(u_xlatb33)) ? float2(1.5, 0.0) : float2(0.5, 1.35000002);
    output.TEXCOORD2.xy = (uint(u_xlatu2) != uint(0)) ? u_xlat12.xz : float2(-0.5, 0.0);
    u_xlat0.xyz = (uint(u_xlatu2) != uint(0)) ? u_xlat0.xyz : u_xlat5.xyz;
    u_xlat3.xyz = (-u_xlat4.xyz) + u_xlat0.xyz;
    u_xlat33 = dot(u_xlat7.xyz, u_xlat3.xyz);
    u_xlat3.xyz = fma((-u_xlat7.xyz), float3(u_xlat33), u_xlat0.xyz);
    u_xlatb33 = 0.0<VGlobals._NormalOrient;
    u_xlat0.xyz = (bool(u_xlatb33)) ? u_xlat3.xyz : u_xlat0.xyz;
    u_xlat3 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat3 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], u_xlat0.xxxx, u_xlat3);
    u_xlat0 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], u_xlat0.zzzz, u_xlat3);
    u_xlat0 = u_xlat0 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat3 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat3 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat0.xxxx, u_xlat3);
    u_xlat3 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat0.zzzz, u_xlat3);
    output.mtl_Position = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[3], u_xlat0.wwww, u_xlat3);
    u_xlat3.x = dot(u_xlat13.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat3.y = dot(u_xlat13.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat3.z = dot(u_xlat13.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat12.x = rsqrt(u_xlat12.x);
    output.TEXCOORD0.xyz = u_xlat12.xxx * u_xlat3.xyz;
    output.TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlati1.x = u_xlati1.x + int(0xffffffffu);
    u_xlat12.x = float(u_xlati1.z);
    u_xlat9.x = u_xlat12.x * 0.00392156886;
    u_xlatb1 = u_xlati1.x==VGlobals.solidHighlightId;
    u_xlat12.xyz = u_xlat9.xyz + float3(1.0, 1.0, 1.0);
    output.COLOR0.xyz = (bool(u_xlatb1)) ? u_xlat12.xyz : u_xlat9.xyz;
    output.COLOR0.w = 1.0;
    u_xlat1.xyz = u_xlat0.yyy * VGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = fma(VGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, u_xlat0.xxx, u_xlat1.xyz);
    u_xlat0.xyz = fma(VGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, u_xlat0.zzz, u_xlat1.xyz);
    output.TEXCOORD3.xyz = fma(VGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz, u_xlat0.www, u_xlat0.xyz);
    output.TEXCOORD4 = float4(0.0, 0.0, 0.0, 0.0);
    return output;
}
                              VGlobals�        _WorldSpaceCameraPos                         chunkCenter                   �  	   chunkSize                     �     splatRadius                   �     solidHighlightId                 �     _ClayxelSize                  �     _NormalOrient                     �     unity_ObjectToWorld                        unity_WorldToObject                  P      unity_MatrixV                    �      unity_MatrixVP                   �      unity_WorldToLight                        objectMatrix                 P            VGlobals              chunkPoints           